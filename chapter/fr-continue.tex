\section{Mappa di Gauss}
Consideriamo la \emph{mappa di Gauss} $ f \colon \R \setminus \{0\} \to [0, 1) $ data da
\begin{equation}
  f(x) = \left\{\frac{1}{x}\right\},
\end{equation}
dove con $ \{y\} = y - \lfloor y \rfloor $ indichiamo la \emph{parte frazionaria} di $ y $, ovvero $ f(x) = 1/x - n $ se $ 1/(n+1) < x \leq 1/n $. In seguito ci interesserà studiare la mappa di Gauss come sistema dinamico: se $ x \in (0, 1) $ allora $ f(x) \in [0, 1) $, e se $ x \geq 1 $ allora $ f(x) \in (0, 1) $ e quindi dopo la prima iterazione siamo in $ (0, 1) $; similmente se $ x < 0 $ allora $ f(x) \in [0, 1) $ e quindi, a meno che $ f(x) = 0 $, dopo la prima iterazione siamo in $ (0, 1) $. Dunque senza perdita di generalità basterà considerare la mappa di Gauss definita come $ f \colon (0, 1) \to [0, 1) $. In Figura \ref{fig:gauss} è riportato il grafico della mappa di Gauss. \\

\iffigureon
\begin{figure}
  \centering
  \input{img/gauss.tikz}
  \caption{mappa di Gauss ristretta a $ (0, 1) $.}
  \label{fig:gauss}
\end{figure}
\fi

Osserviamo che tale mappa conserva la misura con densità
\begin{equation}
  \dif{\mu}(x) \coloneqq \frac{1}{(\log{2})(1+x)}
\end{equation}
che è pertanto assolutamente continua rispetto a Lebesgue. Infatti, dato $ y \in (0, 1) $, $ f^{-1}(\{y\}) = \left\{\frac{1}{y+n} : n \in \N\right\} $; inoltre $ f'(x) = -\frac{1}{x^2} $, così l'operatore di Perron-Frobenius è
\begin{align*}
  \sum_{x \in f^{-1}(\{y\})} \frac{\rho(x)}{\abs{f'(x)}}
  & = \frac{1}{\log{2}} \sum_{n=0}^{+\infty} \frac{1}{1+\frac{1}{y+n}} \frac{1}{(y+n)^2}
    = \frac{1}{\log{2}} \sum_{n=0}^{+\infty} \frac{1}{(y+n+1)(y+n)} \\
  & = \frac{1}{\log{2}} \sum_{n=0}^{+\infty} \left(\frac{1}{y+n} - \frac{1}{y+n+1}\right).
\end{align*}
Quest'ultima è una serie telescopica che è quindi uguale a $ 1/(y+1)$ quindi
\[
  \sum_{x \in f^{-1}(\{y\})} \frac{\rho(x)}{\abs{f'(x)}} = \frac{1}{\log{2}} \frac{1}{y+1} = \rho(y).
\]
Quindi per quanto visto nell'Esercizio \ref{ex:invarianza-PF} la misura $ \mu $ è $ f $-invariante. In coefficiente $ \log{2} $ è la costante di normalizzazione affinché lo spazio $ ((0, 1), \mathcal{M}, \mu) $ sia uno spazio di probabilità ($ \mathcal{M} $ è l'insieme dei Lebesgue misurabili). \\

Vogliamo studiare il sistema dinamico misurabile $ ((0, 1), \mathcal{M}, \mu, f) $. Volendo considerare le orbite dei punti di $ (0, 1) $ sotto l'iterazione di $ f $ osserviamo prima di tutto che se per un qualche $ n \in \N $ si ha $ f^n(x) = 0 $, allora da quel $ n $ in poi l'orbita non è più ben definita cioè non esisteranno tutte le iterate della mappa di Gauss. Questi punti ``cattivi'' vengono caratterizzati nella seguente

\begin{proposition}
  $ f^n(x) $ è definito $ \forall n \in \N $ $ \iff $ $ x \in \R \setminus \Q \cap (0, 1) $.
\end{proposition}
\begin{proof}
  Dimostriamo la contro-nominale, ovvero che $ \exists n \in \N : f^{n}(x) = 0 $ $ \iff $ $ x \in \Q\cap(0, 1) $. Osserviamo che se $ x = p/q \in \Q\cap(0,1) $ allora $ f^{-1}\left(\{x\}\right) = \left\{\frac{q}{p + qm} : m \in \N\right\}\subseteq \Q $, cioè controimmagini di razionali tramite la mappa di Gauss sono ancora razionali. Pertanto se per qualche $ n \in \N, f^{n}(x) = 0 \in \Q $, prendendo le controimmagini di $ \{0\} $ troviamo necessariamente $ x \in \Q $. \\
  Per l'implicazione inversa sia $ x = p/q \in \Q\cap(0,1) $. Osserviamo che essendo $ x \in (0, 1) $, $ p\neq 0 $ e $ p < q $: pertanto facendo la divisione euclidea otteniamo che esistono $ a_1 \in \N $ e $ r_1 \in \N : r_1 < p $ tali che $ q = a_1 p + r_1 $. Così
  \[
    \frac{p}{q} = \cfrac{1}{\cfrac{q}{p}} = \cfrac{1}{a_1 + \cfrac{r_1}{p}}.
  \]
  Ora ci sono due possibilità: o $ r_1 = 0 $ e in tale caso ci fermiamo, oppure $ r_1 > 0 $ e quindi si più scrivere $ p = a_2 r_1 + r_2 $ con $ r_2 < r_1 $. Così nel secondo caso
  \[
    \frac{p}{q} = \cfrac{1}{a_1 + \cfrac{r_1}{p}} = \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{r_2}{r_1}}}.
  \]
  Anche questa volta abbiamo due possibilità: o $ r_2 = 0 $ e in tale caso ci fermiamo, oppure possiamo scrivere $ r_1 = a_3 r_2 + r_3 $ con $ r_3 < r_2 $. Così nel secondo caso
  \[
    \frac{p}{q} = \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{r_2}{r_1}}} = \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{1}{a_3 + \cfrac{r_3}{r_2}}}}.
  \]
  Questo procedimento non può andare avanti all'infinito, ovvero non si può avere sempre $ r_i \neq 0 $ in quanto la successione di interi $ (r_i) $ che stiamo costruendo è strettamente decrescente. Concludiamo quindi che per ogni razionale in $ (0, 1) $ esistono \emph{finiti} $ a_1, \ldots, a_n \in \N $ e $ r_1, \ldots, r_n \in \N $ con gli $ r_i $ strettamente decrescenti tali che
  \[
    \frac{p}{q} = \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{1}{\ddots + \cfrac{1}{a_n}}}}
  \]
  Applicando ora la mappa di Gauss a $ p/q $ per $ n $ volte è allora evidente che si arriverà a $ f^{n}(p/q) = 0 $:
  \begin{align*}
    f\left(\frac{p}{q}\right) & = a_1 + \cfrac{1}{a_2 + \cfrac{1}{\ddots + \cfrac{1}{a_n}}} - a_1 = \cfrac{1}{a_2 + \cfrac{1}{\ddots + \cfrac{1}{a_n}}} \\
    f^2\left(\frac{p}{q}\right) & = a_2 + \cfrac{1}{a_3 + \cfrac{1}{\ddots + \cfrac{1}{a_n}}} - a_2 = \cfrac{1}{a_3 + \cfrac{1}{\ddots + \cfrac{1}{a_n}}} \\
                              & \vdots \\
    f^{n-1}\left(\frac{p}{q}\right) & = a_{n-1} + \frac{1}{a_n} - a_{n-1} = \frac{1}{a_n} \\
    f^{n}\left(\frac{p}{q}\right) & = a_n - a_n = 0 \qedhere
  \end{align*}
\end{proof}

\section{Numeri diofantei e di Liouville}
\begin{exercise}[teorema di Dirichlet]
  Sia $ \alpha \in \R\setminus\Q $. Allora l'equazione
  \[ \abs{\alpha - \frac{p}{q}} < \frac{1}{q^2} \]
  ha infinite soluzioni $ p/q \in\Q $ distinte.
\end{exercise}

\begin{definition}[numeri diofantei]
  Dato $ \gamma > 0 $ e $ \tau \geq 0 $, definiamo l'insieme
  \[ \mathrm{CD(\gamma,\tau)} \coloneqq \left\{ \alpha\in\R\setminus\Q : \abs{\alpha - \frac{p}{q}} \geq \frac{\gamma}{q^{2+\tau}}\quad \forall p/q\in\Q \right\} \]
  Definiamo poi
  \[ \mathrm{CD(\tau)} \coloneqq \bigcup_{\gamma > 0} \mathrm{CD}(\gamma,\tau) \]
  e infine l'insieme dei numeri diofantei
  \[ \mathrm{CD} \coloneqq \bigcup_{\tau \geq 0} \mathrm{CD}(\tau) \]
\end{definition}

\begin{definition}[numeri di Liouville]
  Diciamo che $ x $ è di Liouville se $ x\in (\R\setminus\Q) \setminus \mathrm{CD} $.
\end{definition}

\begin{exercise}
  $ \sum_{n=0}^{+\infty} 10^{-n!} $ è di Liouville.
\end{exercise}
\begin{exercise}
  Gli irrazionali algebrici sono diofantei.
\end{exercise}
\begin{exercise}
  Quasi ogni reale è diofanteo.\\
  Hint: stimare la misura di Lebesgue di $ ( (0,1) \setminus \mathrm{CD}(\gamma,\tau) ),\; \tau > 0 $.
\end{exercise}

\begin{definition}[numero algebrico]
  Un numero $ \alpha $ in $ \R $ o $ \C $ si dice algebrico se è radice di un polinomio a coefficienti interi, cioè se esiste $ P \in \Z[x] $ tale che $ P(\alpha) = 0 $. \\
  Si dice che $ \alpha $ è algebrico di \emph{grado} $ d $ se $ \alpha $ è radice di un polinomio di grado $ d $ e di nessun polinomio di grado minore.
\end{definition}

\begin{thm}[Liouville]
  Se $ \alpha $ è algebrico di grado $ d \geq 2 $ allora $ \forall \epsilon > 0 $ la disequazione
  \[
    \abs{\alpha - \frac{p}{q}} < \frac{1}{q^{d + \epsilon}}
  \]
  ha solo un numero finito di soluzioni.
\end{thm}

\begin{proof}
  Sia $ P \in \Z[x] $ di grado $ d $ indivisibile tale che $ P(\alpha) = 0 $. Osserviamo che per ogni $ p/q \in \Q $ diverso da $ \alpha $ vale che $ P(p/q) = N/q^d $ per un qualche $ N \in \Z$. Se fosse $N=0$, allora il polinomio $P(x)$ avrebbe una radice razionale $p/q$, quindi sarebbe fattorizzabile come $P(x)=(x-p/q)R(x)$ e dunque non sarebbe irridcubile, assurdo. Quindi $N\in \Z \setminus \{0\} $.\\
  Lo sviluppo in serie di Taylor di $ P $ centrato in $ \alpha $ ha espressione finita e pari a
  \[
    P(x) = \sum_{k = 1}^{d} \frac{P^{(k)}(\alpha)}{k!} (x - \alpha)^k
  \]
  dove abbiamo omesso il termine di grado zero essendo $ P^{(0)}(\alpha) = P(\alpha) = 0 $. Così
  \[
    \abs{P\left(\frac{p}{q}\right)} \leq \sum_{k = 1}^{d} \frac{\abs{P^{(k)}(\alpha)}}{k!} \abs{\frac{p}{q} - \alpha}^k = \abs{\frac{p}{q} - \alpha} \sum_{k = 1}^{d} \frac{\abs{P^{(k)}(\alpha)}}{k!} \abs{\frac{p}{q} - \alpha}^{k - 1}.
  \]
  Supponiamo ora di avere infinite soluzioni $p/q$ tali che per un certo $\epsilon$ si abbia $ \abs{p/q - \alpha} \leq \frac{1}{q^{d + \epsilon}}<1$. In particolare, essendo $q$ intero positivo e $d+\epsilon >1$:
  \[
    \abs{P\left(\frac{p}{q}\right)}\leq \abs{\frac{p}{q} - \alpha} \sum_{k = 1}^{d} \frac{\abs{P^{(k)}(\alpha)}}{k!} \abs{\frac{p}{q} - \alpha}^{k - 1} \leq \abs{\frac{p}{q} - \alpha} \sum_{k = 1}^{d} \frac{\abs{P^{(k)}(\alpha)}}{k!}.
  \]
  Notiamo ora che
  \[
    A(\alpha) \coloneqq d \cdot \underset{1 \leq k \leq d}{\sup} \dfrac{1}{k!} \abs{P^{(k)}(\alpha)} \geq  \sum\limits_{k = 1}^{d} \frac{\abs{P^{(k)}(\alpha)}}{k!}
  \]
  Infatti il sup maggiora ognuno dei termini della somma. In particolar modo $A(\alpha)$ dipende dal grado del polinomio $d$ ma non dipende dalla soluzione trovata $p/q$, quindi fissato $\alpha$ è una costante.
  \[
    \frac{\abs{N}}{q^d} \leq \abs{\frac{p}{q} - \alpha} A(\alpha) \leq \frac{1}{q^{d+\epsilon}} A(\alpha)
  \]
  Da cui $\abs{N} \leq \frac{A(\alpha)}{q^{\epsilon}}$. Questa uguaglianza vale per tutte le infinite soluzioni $p/q$, in particolar modo deve valere per infiniti $q$. Dunque esiste una successione infinita crescente $\left\{q_j\right\}_{j\in \N}$ tale che $\abs{N} \leq \frac{A(\alpha)}{q_j^{\epsilon}}$. Dato che $q_j$ va a infinito, $\frac{A(\alpha)}{q_j^{\epsilon}}$ tende a 0, dunque $\abs{N}=N=0$, assurdo per l'osservazione iniziale.\qedhere
\end{proof}
