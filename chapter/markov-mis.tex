\section{Catene di Markov misurabili}
Sia $ \Gamma \subseteq \{1, \ldots, N\}^{2} $ un grafo \emph{connesso} e \emph{diretto} sui vertici $ \{1, \ldots, N\} $ con al più una sola freccia da $ i \to j $. Oltre alla matrice di adiacenza $ A $, consideriamo una \emph{matrice stocastica} $ P $, ovvero una matrice $ N \times N $ a entrate reali non negative tale che $ \sum_{j=1} P_{ij} = 1 $ per ogni riga $ i $ della matrice. L'elemento $ P_{ij} $ di tale matrice rappresenta la probabilità di transire dal vertice $ i $ al vertice $ j $ e pertanto se $ A_{ij} = 0 $ poniamo $ P_{ij} = 0 $, mentre se $ A_{ij} = 1 $ si avrà $ P_{ij} > 0 $.

Mostriamo ora che esiste un autovettore sinistro di $ P $ con autovalore 1. Sia infatti $ \lambda $ autovalore destro di $ P $ e $ v $ relativo autovettore $ Pv = \lambda v $: essendo $ P $ stocastica, $ 0 \leq P_{ij} \leq 1 $ quindi per ogni $ i \in \{1, \ldots, N\} $ si ha
\[
  \abs{\lambda} \abs{v_i} = \abs{\sum_{j} P_{ij} v_j} \leq \sum_{j} P_{ij} \abs{v_j} \leq \max_{j} \abs{v_j}
\]
da cui passando al massimo su $ i $ si ottiene $ \abs{\lambda} \leq 1 $. D'altra parte, sempre grazie al fatto che $ P $ è stocastica, $ v = (1, \ldots, 1) $ è autovettore con autovalore 1. Ma autovalori sinistri sono anche autovalori destri e viceversa, quindi per il Teorema \ref{thm:Perron-Frobenius-debole} sappiamo che esiste un autovettore sinistro di $ P $ con entrate non negative e con autovalore sinistro massimo, cioè 1. Sia $ p $ un tale autovettore con $ \sum_{i} p_i = 1 $. \\

Vogliamo ora generalizzare gli schemi di Bernoulli, come fatto per lo \emph{shift} nel caso topologico, ai percorsi permessi dalla matrice di adiacenza. Come nel caso topologico sia
\[ \Sigma_A = \{x \in \{1, \ldots, N\}^{\Z} : \forall i \in \Z, \ (x_i, x_{i+1}) \in \Gamma\} \]
e $ \sigma_A \colon \Sigma_A \to \Sigma_A $ la restrizione dello \emph{shift} sinistro. Vogliamo dotare $ \Sigma_A $ di una $ \sigma $-algebra e di una misura che sia invariante per $ \sigma_A $. Definiamo i cilindri come la restrizione dei cilindri definiti per gli schemi di Bernoulli ai percorsi permessi
\[
  \begin{pmatrix}
    i_1 & \cdots & i_m \\
    j_1 & \cdots & j_m
  \end{pmatrix}_A
  \coloneqq
  \{x \in \Sigma_A : x_{i_1} = j_1, \ldots x_{i_m} = j_m\}
\]
Sia ora $ P $ la matrice stocastica di prima e $ p = (p_1, \ldots, p_N) $ un vettore di probabilità tale che $ p P = p $, che rappresenta la probabilità di un dato simbolo in $ j \in \{1, \ldots, N\} $ e $ P_{ij} $. Definiamo allora in modo naturale la misura del cilindro come
\[
  \mu{\left(\begin{pmatrix}
        i_1 & \cdots & i_k \\
        j_1 & \cdots & j_k
      \end{pmatrix}_A\right)}
  \coloneqq
  p_{j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k}.
\]
Osserviamo che tale misura è ben definita grazie al fatto che $ p $ è autovettore sinistro con autovalore 1: infatti
\[
  \begin{pmatrix}
    i_1 & \cdots & i_m \\
    j_1 & \cdots & j_m
  \end{pmatrix}_A
  =
  \bigsqcup_{j=1}^{N}
  \begin{pmatrix}
    i_1 - 1 & i_1 & \cdots & i_k \\
    j & j_1 & \cdots & j_k
  \end{pmatrix}_A
\]
da cui vogliamo che valga
\begin{align*}
  p_{j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k} & =
                                               \mu{\left(\begin{pmatrix}
                                                     i_1 & \cdots & i_m \\
                                                     j_1 & \cdots & j_m
                                                   \end{pmatrix}_A\right)}
                                                                    =
                                                                    \sum_{j=1}^{N}
                                                                    \mu{\left(\begin{pmatrix}
                                                                          i_1 - 1 & i_1 & \cdots & i_k \\
                                                                          j & j_1 & \cdots & j_k
                                                                        \end{pmatrix}_A\right)} \\
                                             & = \sum_{j=1}^{N}
                                               p_j P_{j j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k} = \left(\sum_{j=1}^{N}
                                               p_j P_{j j_1}\right) P_{j_1 j_2} \cdots P_{j_{k-1} j_k}
\end{align*}
ovvero $ \sum_{j} p_j P_{ji} = p_i $ per ogni $ i \in \{1, \ldots, N\} $. Tale misura si estende, in modo analogo a quanto fatto con gli schemi di Bernoulli, a una misura di probabilità sulla $ \sigma $-algebra $ \mathcal{C}_A $ generata dai cilindri su $ \Sigma_A $. Con lieve abuso di notazione continueremo ad indicare con $ \mu $ tale misura. Osserviamo che la misura dei cilindri su $ \Sigma_A $ è invariante per $ \sigma_A $, in quanto tale funzione agisce su tali cilindri cambiando gli indici ma non i simboli, e pertanto invariante su ogni elemento di $ \mathcal{C}_A $. \\

La quaterna $ (\Sigma_A, \mathcal{C}_A, \mu, \sigma_A) $ è quindi un sistema dinamico misurabile detto \emph{catena di Markov misurabile} data la \emph{matrice di transizione} $ P $ e la \emph{distribuzione iniziale} $ p $.

\begin{proposition}
  Sia $ (\Sigma_A, \mathcal{C}_A, \mu, \sigma_A) $ una catena di Markov misurabile con matrice di transizione $ P $ e distribuzione iniziale $ p $. Allora
  \begin{equation}
    h_{\mu}(\sigma_A) = -\sum_{i,j} p_i P_{ij} \log{P_{ij}}.
  \end{equation}
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante.}
\end{proof}