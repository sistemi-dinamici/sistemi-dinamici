\section{Definizione e coordinate locali}

Lavoreremo nel seguito con la seguente definizione di varietà, intendendo sempre che essa è immersa nello \emph{spazio ambiente} $\R^N$.

\begin{definition}[varietà]
  Un insieme $M \subseteq \R^N$ si dice varietà di dimensione $n$ (o $n$-varietà) se esistono $N-n$ vincoli $F_{n+1}, \ldots, F_N \colon \R^N \to \R$ di classe $C^{\infty}$ tali che
  \[ M = \left\{x \in \R^N : F_j(x) = 0 \quad \forall j = n+1, \ldots, N\right\} \]
  e inoltre $\left\{\nabla F_j(x)\right\}_{j = n+1}^N$ sono linearmente indipendenti per ogni $x \in M$.
\end{definition}

In altre parole, una $n$-varietà immersa in $\R^N$ è l'intersezione di $N-n$ superfici di livello. La condizione sui gradienti assicura da un lato che le superfici di livello siano non degeneri (poiché i gradienti non possono annullarsi); dall'altro, esprime l'intuizione geometrica che esse si incontrino in modo trasversale.

Ogni $n$-varietà si può esprimere localmente come grafico di una funzione da $\R^n$ a $\R^{N-n}$, come afferma il teorema della funzione implicita per funzioni da $\R^N$ a $\R^{N-n}$. Lo dimostriamo nel seguente


\begin{thm} \label{coordinate}
  Sia $M$ una $n$-varietà, e $x_0 \in M$. Allora, a meno di una permutazione delle coordinate, esiste un intorno $U$ di $\pi(x_0)$, un intorno $V$ di $x_0$ e una funzione $f \colon U \to \R^{N-n}$ di classe $C^\infty$ tali che
  \[ M \cap V = \{(q,f(q)) : q \in U\}. \]
\end{thm}
\begin{proof} Consideriamo la matrice $(N-n) \times N$ formata dai gradienti dei vincoli nel punto $x_0$
  \[ \begin{pmatrix} \nabla F_{n+1}(x_0) \\ \vdots \\ \nabla F_N (x_0) \end{pmatrix}. \]

  A causa della condizione sui gradienti, tale matrice ha rango massimo. Esiste quindi un insieme di $N-n$ indici tali che le corrispondenti colonne formano un minore invertibile. A meno di permutare le coordinate, supponiamo che essi siano gli ultimi $N-n$. Agendo eventualmente sullo spazio ambiente $\R^N$ con un cambio di coordinate della forma
  \[ \begin{pmatrix} id_n & 0 \\ 0 & A \end{pmatrix}, \]
  dove $A$ è una matrice invertibile $(N-n)\times (N-n)$, possiamo assumere che la matrice dei gradienti sia della forma
  \[ \begin{pmatrix}
      * & \cdots & * & 1 && 0 \\
      \vdots & \ddots & \vdots & & \ddots & \\
      * & \cdots & * & 0 && 1 \\
    \end{pmatrix}. \]
  Sia per semplicità $x = (x', x_N) \in \R^N$, ove $x' \in \R^{N-1}$. Dall'equazione $F_N(x) = 0$, per il teorema della funzione implicita rispetto all'ultima variabile, otteniamo un intorno $U_N$ di $x_0'$ e una funzione $g_N$ di classe $C^\infty$ tale che $F_N(x', g_N(x')) = 0$ per ogni $x' \in U_N$.
  Sia ora $x' = (x'', x_{N-1}) \in \R^{N-1}$. Definiamo
  \[ G_{N-1}(x_1, \ldots, x_{N-1}) = F_{N-1}(x_1, \ldots, x_{N-1}, g_N(x_1, \ldots, x_{N-1})). \]
  Poiché
  \[ \frac{\partial G_{N-1}}{\partial x_{N-1}}(x_0) = \frac{\partial F_{N-1}}{\partial x_{N-1}}(x_0) + \frac{\partial F_{N-1}}{\partial x_{N}}(x_0) \frac{\partial g_{N}}{\partial x_{N-1}}(x_0') = 1 + 0 = 1, \]
  il teorema della funzione implicita ci fornisce un intorno $U_{N-1}$ di $x_0''$ e una funzione $g_{N-1}$ di classe $C^\infty$ tale che
  \begin{align*}
    F_{N-1}(x'', g_{N-1}(x''), g_N(x'', g_{N-1}(x''))) & = 0 \\
    F_N(x'', g_{N-1}(x''), g_N(x'', g_{N-1}(x''))) & = 0
  \end{align*}
  per ogni $x'' \in U_{N-1}$. Proseguendo in modo analogo, opportunamente restringendo gli intorni un numero finito di volte, otteniamo alla fine una parametrizzazione del tipo
  \[ F_j(q, f_{n+1}(q), \ldots, f_{N}(q)) = 0 \quad \forall j = n+1, \ldots, N \]
  per ogni $q$ in un opportuno intorno di $q_0 = ({x_0}_1, \ldots, {x_0}_n)$ in $\R^n$. Poiché tutte le funzioni fornite dal teorema della funzione implicita sono di classe $C^\infty$, anche $f_j$ sono di classe $C^\infty$ per ogni $j$.
\end{proof}

Quando applichiamo il Teorema \ref{coordinate}, diciamo che scegliamo delle \emph{coordinate locali} attorno al punto $x_0$. Questo equivale a scegliere $n$ indici distinti $1 \leq k_1 < \cdots < k_n \leq N$ (\emph{coordinate libere}), fissare la corrispondente proiezione $\pi \colon \R^N \to \R^n$, $\pi(x) = (x_{k_1}, \ldots, x_{k_n})$, e la mappa di ``sollevamento'' $\sigma \colon U \subseteq \R^n \to \R^N$, $\sigma(q) = \pi^{-1}(q) \cap M$ definita in un intorno di $q_0 = \pi(x_0)$. Come segue dal Teorema \ref{coordinate}, entrambe queste funzioni sono di classe $C^\infty$, e si osserva facilmente che
\begin{align*}
  \pi (\sigma (q)) & = q \quad \forall q \in U \\
  \sigma (\pi (x)) & = x \quad \forall x \in M \cap \pi^{-1}(U).
\end{align*}

\section{Spazio tangente}
Sia $M \subseteq \R^N$ una $n$-varietà, e fissiamo un suo punto $x_0$. Vogliamo definire quali sono i vettori tangenti alla varietà nel punto $x_0$. Immaginando $M$ come spazio delle configurazioni di un sistema meccanico, è naturale richiedere che, se una curva $\gamma(t)$ passa per il punto $x_0$ al tempo $t_0$, allora la sua velocità istantanea $\dot{\gamma}(t_0)$ appartenga allo spazio tangente a $M$ in $x_0$. Questo si formalizza nella seguente

\begin{definition}[spazio tangente]
  Dato $x_0 \in M$, lo spazio tangente a $M$ in $x_0$ è
  \[ T_{x_0} M \coloneqq \left\{\dot{\gamma}(0) : \gamma \colon (-\epsilon, \epsilon) \to M, \gamma(0) = x_0 \right\} \]
  ove $\epsilon$ è un qualsiasi numero positivo, e si richiede che $\gamma$ sia di classe $C^\infty$.
\end{definition}

Se $v \in T_{x_0} M$, e $\gamma \colon (-\epsilon, \epsilon) \to M$ è tale che $\gamma(0) = x_0$ e $\dot{\gamma}(0) = v$, diremo che $\gamma$ \emph{genera} $v$.

\begin{thm}
  Sia $M \subseteq \R^N$ una $n$-varietà, e $x_0 \in M$. Allora $T_{x_0} M$ è un sottospazio vettoriale di $\R^N$ di dimensione $n$.
\end{thm}

\begin{proof}
  Mostriamo intanto che $T_{x_0} M$ è uno spazio vettoriale. Siano $v_1,v_2 \in T_{x_0} M$, $\alpha_1, \alpha_2 \in \R$, e prendiamo $\gamma_1, \gamma_2 \colon (-\epsilon, \epsilon) \to M$ che generano rispettivamente $v_1$ e $v_2$. Scegliamo coordinate locali attorno ad $x_0$, e supponiamo senza perdita di generalità che le coordinate libere siano le prime $n$, e poniamo $x_0 = (q_0, f(q_0))$. In questo modo, eventualmente restringendo l'intervallo temporale di definizione, $\gamma_j(t) = (q_j(t), f(q_j(t))$. Si ha quindi che
  \[ \dot{\gamma}_j(0) = (\dot{q}_j(0), \nabla f(q_0) \cdot \dot{q}_j(0)) = v_j. \]
  Poniamo
  \[ \gamma(t) = (q_1(\alpha_1 t) + q_2(\alpha_2 t) - q_0, f(q_1(\alpha_1 t) + q_2(\alpha_2 t) - q_0)). \]
  Poiché $\gamma(0) = x_0$ e $\gamma$ è continua, restringendo opportunamente l'intervallo temporale di definizione si ha che $\gamma(t)$ è ben definita e $C^\infty$. Infine,
  \[ \dot{\gamma}(0) = \left( \alpha_1 \dot{q}_1(0) + \alpha_2 \dot{q}_2(0), \nabla f(q_0) \cdot (\alpha_1 \dot{q}_1(0) + \alpha_2 \dot{q}_2(0)) \right) = \alpha_1 v_1 + \alpha_2 v_2, \]
  da cui $\alpha_1 v_1 + \alpha_2 v_2 \in T_{x_0} M$.

  Mantenendo la scelta delle coordinate, consideriamo per $k = 1, \ldots, n$ le speciali curve (dette \emph{curve coordinate})
  \[ \gamma_k(t) = (q_0 + te_k, f(q_0 + te_k)), \]
  ove $e_k$ indica il $k$-esimo vettore della base canonica di $\R^n$. Si ha
  \[ \dot{\gamma}_k(0) = (e_k, \nabla f (q_0) \cdot e_k), \]
  e, osservando le prime componenti, si nota subito che questi sono $n$ vettori linearmente indipendenti. Se $v \in T_{x_0} M$, prendiamo $\gamma(t) = (q(t), f(q(t)))$ che lo genera. Sia $\dot{q}(0) = a_1 e_1 + \cdots + a_n e_n$. Allora
  \[v = (\dot{q}(0), \nabla f(q_0) \cdot \dot{q}(0)) = \sum_{k = 1}^n a_k (e_k, \nabla f(q_0) \cdot e_k) = \sum_{k = 1}^n a_k \dot{\gamma}_k(0), \]
  quindi $\{\dot{\gamma}(0)\}_{k = 1}^n$ generano $T_{x_0} M$.
\end{proof}

Dalla seconda parte della dimostrazione possiamo trarre la seguente osservazione: qualora si fissi un sistema di coordinate locali attorno al punto $x_0 \in M$, si ottiene una base canonica dello spazio tangente $T_{x_0} M$ data dalle velocità delle curve coordinate. Va sempre tenuto ben in mente, tuttavia, che la scelta delle coordinate \emph{non} è univoca.

Dello spazio tangente a $M$ in un punto $x_0$ può risultare molto comoda anche la seguente caratterizzazione geometrica in termini dei gradienti dei vincoli.

\begin{proposition}
  Sia $M \subseteq \R^N$ una $n$-varietà, e $x_0 \in M$. Allora
  \[ T_{x_0} M = \left\{v \in \R^N : \nabla F_j(x_0) \cdot v = 0 \quad \forall j = n+1, \ldots, N\right\} \]
\end{proposition}

\begin{proof}
  È immediato verificare che il termine a destra sia uno spazio vettoriale. Inoltre, indicando con $A$ la matrice dei gradienti nel punto $x_0$, che ha rango $N-n$, dal teorema del rango si ottiene che
  \[ \dim \ker A = N - \dim \im A = N - (N-n) = n. \]
  Per dimostrare che i due spazi coincidono, basta verificare un contenimento. Sia $v \in T_{x_0} M$, e sia $\gamma$ che genera $v$. Poiché la curva ha valori in $M$, devono essere soddisfate le $N-n$ equazioni
  \[ F_j(\gamma(t)) = 0 \quad \forall j = n+1, \ldots, N. \]
  Derivando nel tempo e calcolando in $t=0$ si ottiene
  \[ 0 = \nabla F_j(\gamma(0)) \cdot \dot{\gamma}(0) = \nabla F_j(x_0) \cdot v \quad \forall j = n+1, \ldots, N \]
  da cui $v \in \ker A$.
\end{proof}

\section{Mappe $C^\infty$ e differenziali}

Data una $n$-varietà $M$ e una funzione $\varphi \colon M \to \R$, vogliamo cercare di dare una definizione soddisfacente di ``gradiente'' della funzione $\varphi$. Per fare questo, dobbiamo intanto stabilire quali funzioni sono derivabili.

\begin{definition}
    Una funzione $\varphi \colon M \to \R$ è di classe $C^\infty$ nel punto $x_0 \in M$ se, scelto un sistema di coordinate locali $\pi, \sigma$ attorno ad $x_0$, si ha che $\varphi \circ \sigma \colon U \subseteq \R^n \to \R$ è di classe $C^\infty$.
\end{definition}

Osserviamo innanzitutto che la proprietà della funzione $\varphi$ di essere $C^\infty$ \emph{non} dipende dalle coordinate scelte, ed è quindi una proprietà intrinseca. Se $\pi', \sigma'$ è un altro sistema di coordinate, infatti, si ha che
\[ \varphi \circ \sigma' = \varphi \circ \sigma \circ \pi \circ \sigma' = (\varphi \circ \sigma) \circ (\pi \circ \sigma'). \]
Da un lato, $\varphi \circ \sigma \colon U \subseteq \R^n \to \R$ è di classe $C^\infty$ per definizione; dall'altro, $\pi \circ \sigma' \colon U' \subseteq \R^n \to \R^n$ è composizione di due mappe $C^\infty$.

Il problema nel definire il gradiente di una funzione definita solo sulla varietà $M$ è che non è possibile calcolare le derivate direzionali di $\varphi$ lungo un vettore di $\R^N$. Se infatti $x_0 \in M$ e $v \in \R^N$, gli incrementi lineari del tipo $x_0 + tv$ potrebbero non appartenere alla varietà non appena $t \neq 0$, e dunque l'espressione $\varphi(x_0 + tv)$ sarebbe priva di significato.

Questo ostacolo è superato dalla seguente

\begin{definition}[differenziale]
    Data $\varphi \colon M \to \R$ di classe $C^\infty$, dati $x_0 \in M$ e $v \in T_{x_0} M$, definiamo il differenziale di $\varphi$ in $x_0$ lungo $v$ come
    \begin{equation}
        \dif{\varphi}_{x_0} (v) = \left. \dod{}{t} \varphi(\gamma(t)) \right|_{t=0}
    \end{equation}
    ove $\gamma$ è una curva che genera $v$.
\end{definition}

Occorre innanzitutto dimostrare che la definizione non dipende dalla scelta della curva. Siano $\gamma_1, \gamma_2$ due curve che generano $v$. Scegliamo coordinate locali attorno al punto $x_0$ e, come di consueto, assumiamo per semplicità che le coordinate libere siano le prime $n$. Allora possiamo scrivere che $\gamma_j(t) = (q(t), f(q(t)))$ e $\varphi \circ \gamma_j = \varphi \circ \sigma \circ \pi \circ \gamma_j$. Dunque per $j = 1, 2$
\[ \dod{}{t} \varphi(\gamma(t)) = \nabla (\varphi \circ \sigma) (\pi(\gamma(t)) \cdot \dod{}{t} (\pi \circ \gamma_j)(t) = \nabla (\varphi \circ \sigma)(q_j(t)) \cdot \dot{q}_j (t).  \]

Calcolando in $t=0$ si ottiene per $j = 1, 2$
\[ \dif{\varphi}_{x_0} (v) = \nabla (\varphi \circ \sigma)(q_j(0)) \cdot \dot{q}_j(0) = \nabla (\varphi \circ \sigma)(q_0) \cdot \dot{q}_j(0); \]
tuttavia si può osservare che
\[ \dot{q_1}(0) = \pi(\dot{\gamma}_1(0)) = \pi(v) = \pi(\dot{\gamma}_2(0)) = \dot{q}_2(0), \]
e dunque la definizione non dipende dalla curva. Inoltre, abbiamo ottenuto la seguente espressione per il differenziale in coordinate locali:
\begin{equation} \label{diff}
\dif{\varphi}_{x_0} (v) = \nabla (\varphi \circ \sigma)(q_0) \cdot \pi(v).
\end{equation}

Come corollario, poiché $\pi$ è lineare, si ha immediatamente il seguente
\begin{corollary}
    Il differenziale $\dif{\varphi}_{x_0} \colon T_{x_0} M \to \R$ è un'applicazione lineare.
\end{corollary}

In altri termini, possiamo scrivere che $\dif{\varphi}_{x_0} \in (T_{x_0} M)^*$, ovvero che il differenziale in un punto della varietà è un elemento del duale dello spazio tangente in quel punto alla varietà.

\section{Fibrato tangente}

Sia $M \subseteq \R^N$ una $n$-varietà. Vogliamo definire un oggetto che racchiuda in sé tutte le informazioni riguardo alla varietà $M$ e ai suoi spazi tangenti in ogni punto. Questo si ottiene mediante la seguente

\begin{definition}[fibrato tangente]
    Il fibrato tangente della varietà $M$ è
    \[ TM \coloneqq \left\{(x,v) \in \R^N \times \R^N : x \in M, v \in T_x M\right\}. \]
\end{definition}

Ciò che rende lo studio del fibrato tangente possibile e interessante è il fatto che esso stesso risulti essere una varietà.

\begin{thm}
    Sia $M \subseteq \R^N$ una $n$-varietà. Allora $TM \subseteq \R^N \times \R^N$ è una $2n$-varietà.
\end{thm}

\begin{proof}
    Dobbiamo innanzitutto trovare $2(N-n)$ equazioni che siano i vincoli per $TM$. Imponiamo le seguenti condizioni su $(x,v) \in \R^N \times \R^N$:
    \begin{equation} \label{sistemone} \left\{ \begin{array}{ll} F_j(x) = 0 \quad & j = n+1, \dotsc, N \\ \nabla F_j(x) \cdot v = 0 \quad & j = n+1, \dotsc, N. \end{array} \right. \end{equation}

    Sfruttando la caratterizzazione geometrica dello spazio tangente $T_x M$, è immediato verificare che $(x,v) \in TM$ se e solo se $(x,v)$ risolve il sistema \eqref{sistemone}. Dobbiamo però controllare la condizione di lineare indipendenza dei gradienti. La matrice dei gradienti nel punto $(x,v)$ ha la seguente forma:

    \[ \begin{pmatrix}
    \nabla F_{N}(x) & \\ \vdots & 0 \\ \nabla F_{n+1}(x) & \\ & \nabla F_{n+1}(x) \\ * & \vdots \\ & \nabla F_{N}(x)
    \end{pmatrix}. \]

    Quando $x \in M$, scegliendo opportunamente $N-n$ colonne fra le prime $N$ e le corrispondenti $N-n$ colonne fra le altre $N$, si ottiene un minore invertibile, poiché il determinante può essere calcolato ``a blocchi''.
\end{proof}
