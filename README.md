[![build status](https://gitlab.com/sistemi-dinamici/sistemi-dinamici/badges/master/build.svg)](https://gitlab.com/sistemi-dinamici/sistemi-dinamici/-/jobs/artifacts/master/raw/Sistemi-Dinamici.pdf?job=compile_pdf)

Questo repository è basato sulle lezioni del corso di Introduzione ai Sistemi Dinamici I, tenuto nell'AA 2018-2019, suddivise per argomenti.

Il `.pdf` più recente si può scaricare [qui](https://gitlab.com/sistemi-dinamici/sistemi-dinamici/-/jobs/artifacts/master/raw/Sistemi-Dinamici.pdf?job=compile_pdf).

Gli appunti del corso suddivisi per lezioni sono invece disponibili, come memoria storica, [qui](https://gitlab.com/scisns2017/Sistemi-Dinamici).
