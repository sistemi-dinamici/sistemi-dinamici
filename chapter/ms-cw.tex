\section{Modello di Curie-Weiss}
Ci prefiggiamo di studiare ora il modello di Curie-Weiss nel \emph{limite termodinamico}, cioè nel limite in cui il numero $ n $ di particelle tende a infinito. In linea di principio, potremmo voler calcolare direttamente il limite della distribuzione di Boltzmann $ \lim_{n \to \infty} P_{\beta, n} $. Tuttavia tale limite conduce a problematiche assolutamente non banali: per esempio le $ P_{\beta, n} $ sono definite su spazi di misura diversi la cui cardinalità cresce con $ n $ e pertanto non è ben chiaro che cosa voglia dire fare tale limite\footnote{Una trattazione di che cosa voglia dire il limite termodinamico della misura di Boltzmann è stato fatto da Dobrushin, Lanford e Ruelle e porta alla definizione degli \emph{stati DLR}.}. Pertanto il limite termodinamico viene di solito formulato in termini di quantità termodinamiche che sono numeri reali. In particolare sarà utile calcolare il limite dell'energia libera $ f_{\beta, n} \coloneqq F_{\beta, n}/n = -\frac{\log Z_{\beta, n}}{\beta n}$\footnote{D'ora in poi ometteremo il pedice $ \beta $ per alleggerire la notazione.}.

\subsubsection{Road map}
\begin{enumerate}
    \item Calcolo di $ \lim\limits_{n\to\infty} \frac{1}{n} \log{Z_n} = \lim\limits_{n\to\infty} -\frac{\beta F_n}{n} $;
    \item Data una generica famiglia di osservabili $ \mathcal{O}_n\colon\Sigma\to\R $, calcolo del valore atteso nel limite termodinamico $ \lim\limits_{n\to\infty} \mean{\mathcal{O}_n}_{P_\beta} $. Ad esempio considereremo la \emph{magnetizzazione media} $ m_n(\sigma) \coloneqq \frac{1}{n}\sum_{i=1}^{n}\sigma_i $;
    \item Calcolo delle fluttuazioni delle osservabili, cioè di $ \mean{ \left(\mathcal{O} - \mean{\mathcal{O}}_{P_\beta} \right)^2 }_{P_\beta} $.
\end{enumerate}

Nel modello di Curie-Weiss lo spazio delle fasi è $ \Sigma = \{-1,1\}^n $. L'hamiltoniana è:
\begin{equation}
    \ham_n(\sigma) = -\frac{1}{n} \sum_{i<j} \sigma_i \sigma_j - h \sum_i \sigma_i
\end{equation}
dove il primo termine descrive l'interazione tra gli spin, che tendono ad allinearsi, e il secondo l'interazione con il campo esterno $ h $. Osserviamo che $ \sum_{i=1}^{n} \sigma_i $ scala come $ n $, $ \sum_{i<j} \sigma_i \sigma_j $ scala come $ n^2 $  e pertanto $ \ham_n $ scala come $ n $; una quantità di questo tipo di dice \emph{estensiva}. La quantità che ci prefiggiamo di calcolare, $ f_n $, è invece \emph{intensiva}.

Cominciamo riscrivendo l'hamiltoniana in termini della magnetizzazione media $ m_n(\sigma) $:
\[ \sum_{i<j} \sigma_i \sigma_j = \frac{1}{2} \sum_{i \neq j} \sigma_i \sigma_j = \frac{1}{2} \sum_{i,j} \sigma_i \sigma_j -\frac{n}{2} = \frac{n^2 m_n^2(\sigma)}{2} - \frac{n}{2} \]
da cui
\begin{equation}
    \ham(\sigma) = -\frac{n m_n^2(\sigma)}{2} - hnm_n(\sigma) + \frac{1}{2}.
\end{equation}
e quindi
\begin{equation} \label{eqn:Zn-mag}
    Z_n = \sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) - \frac{\beta}{2}\right).
\end{equation}
Di seguito trascureremo la costante $ 1/2 $ nell'hamiltoniana e conseguentemente $ -\beta/2 $ a esponente in $ Z_n $ in quanto essa, non dipendendo da $ \sigma $, porta a una costante $ e^{-\beta/2} $ che moltiplica la somma nella \eqref{eqn:Zn-mag} ovvero a un termine del tipo $ -\frac{\beta}{2n} $ in $ \frac{\log Z_n}{n} $, trascurabile nel limite termodinamico.

\subsection{Esistenza del limite}
Dimostriamo innanzi tutto che il limite $ \lim\limits_{n\to\infty} \frac{\log Z_n}{n} $ esiste.
\begin{proof}
    Procediamo facendo vedere che la successione $ \log Z_n $ è subadditiva, per concludere con il lemma di Fekete (Lemma \ref{lem:fekete}).
    Posto $ n = n_1 + n_2 $, siano $ \sigma_1 \in \{-1,1\}^{n_1} $ e $ \sigma_2 \in \{-1,1\}^{n_2} $ così che $ (\sigma_1, \sigma_2) \in \Sigma $; si ha
    \[ m_n(\sigma_1,\sigma_2) = \mean{\frac{1}{n} \sum_{i=1}^{n_1} \sigma_i} + \mean{\frac{1}{n} \sum_{i=n_1 + 1}^{n_2} \sigma_i} = \frac{n_1}{n} m_{n_1}(\sigma_1) + \frac{n_2}{n} m_{n_2}(\sigma_2) \]
    dove abbiamo definito $ m_1(\sigma) = \frac{1}{n_1} \sum_{i=1}^{n_1}\sigma_{1, i} $ e similmente $ m_2(\sigma) $. Si verifica con facili calcoli che
    \[ n m_n^2 = n_1 m_{n_1}^2 + n_2 m_{n_2}^2 - \frac{n_1 n_2}{n} (m_{n_1}-m_{n_2})^2 \leq n_1 m_{n_1}^2 + n_2 m_{n_2}^2 \]
    e quindi
    \begin{align*}
        Z_{n_1} Z_{n_2} & = \sum_{\sigma_1\in\Sigma_1} \sum_{\sigma_2\in\Sigma_2} \exp\left(\frac{\beta}{2} \left(n_1 m_{n_1}^2(\sigma_1) + n_2 m_{n_2}^2(\sigma_2) \right) \right) \exp\left(\beta h\left(n_1 m_{n_1}(\sigma_1) + n_2 m_{n_2}(\sigma_2) \right) \right) \\
                        & \geq \sum_{\sigma\in\Sigma} \exp\left(\frac{\beta n m_n^2(\sigma)}{2} \right) \exp\left(\beta h n m_n(\sigma) \right) = Z_{n_1+n_2}.
    \end{align*}
    Prendendo il logaritmo si ottiene $ \log Z_{n_1+n_2} \leq \log Z_{n_1} + \log Z_{n_2} $.
\end{proof}

Possiamo ora dare una stima grossolana del limite in analisi. Osservando che dalla definizione $ m_n(\sigma) \leq 1 $, in un verso abbiamo:
\begin{align*}
    Z_n & = \sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) \right) \leq \sum_{\sigma \in \Sigma} \exp\left(  \frac{\beta n}{2} + \beta n h \right) \\
    & = 2^n \exp\left( \frac{\beta n}{2} + \beta n h\right) = \exp\left( n \left( \log 2 + \frac{\beta}{2} + \beta h \right) \right)
\end{align*}
da cui
\[ \frac{\log Z_n}{n} \leq \log 2 + \frac{\beta}{2} + \beta h \]
Nell'altro verso, detta $ \Omega_0 $ la distribuzione uniforme sulle $ 2^n $ configurazioni possibili, e usando la diseguaglianza di Jensen\footnote{Sia $ (X, \mathcal{F}, \mu) $ uno spazio di misura. Se $ f \colon X \to \R $ è una variabile aleatoria e $ \varphi \colon \R \to \R $ è una funzione convessa, allora \[\varphi\left(\mathbb{E}\left[f\right]\right) \leq \mathbb{E}\left[\varphi(f)\right].\]}, si ha
\[
    Z_n = 2^n \mean{\exp\left(-\beta\ham_n\right)}_{\Omega_0} \geq 2^n \exp\left( -\beta \mean{\ham}_{\Omega_0} \right) = 2^n
\]
in quanto $ \mean{\sigma_i}_{\Omega_0} = 0 $ e $ \mean{\sigma_i\sigma_j}_{\Omega_0} = \mean{\sigma_i}_{\Omega_0}\mean{\sigma_j}_{\Omega_0} = 0 $. Prendendo il logaritmo abbiamo infine:
\[ \frac{\log Z_n}{n} \geq \log 2. \]
La stessa stima dal basso poteva essere ottenuta usando il principio variazionale di Gibbs (che d'altra parte fa uso della disuguaglianza di Jensen). Infatti sapendo che $ F_\beta \leq G[\Omega_0] $ si ottiene
\[ \frac{\log Z_n}{n} = -\frac{\beta}{n} F_\beta \geq -\frac{\beta}{n} \left[\mean{H}_{\Omega_0} + \frac{1}{\beta} \sum_{\sigma \in \Sigma} \Omega_0(\sigma) \log{\Omega_0(\sigma)}\right]. \]
Ora come detto in precedenza $ \mean{H}_{\Omega_0} = 0 $ mentre $ \sum_{\sigma \in \Sigma} \Omega_0(\sigma) \log{\Omega_0(\sigma)} = 2^{n} \frac{1}{2^{n}} (- n \log{2}) $ da cui $ \frac{\log Z_n}{n} \geq -\frac{n}{\beta}\left(-\frac{\beta}{n} \log{2}\right) = \log{2}. $

\subsection{Calcolo dell'energia libera}
Calcoliamo ora in modo esatto l'energia libera nel limite termodinamico.
\subsubsection{Primo bound}
Sia $ M \in [-1,1] $. Essendo $ -1 \leq m_n(\sigma) \leq 1 $ si ha $ \exp\left( -\frac{\beta n (m_n(\sigma) - M)^2}{2} \right) \leq 1 $. Quindi
\begin{align*}
    Z_n & \geq \sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) - \frac{\beta n (m_n(\sigma)-M)^2}{2}\right) \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) \sum_{\sigma \in \Sigma} \exp\left( (M+h) \beta n m_n(\sigma) \right)                              \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) \sum_{\sigma \in \Sigma} \prod_{i=1}^{n} \exp( \beta(M+h) \sigma_i )                               \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) \sum_{\sigma_1 = \pm 1} \cdots \sum_{\sigma_n = \pm 1} \prod_{i=1}^{n} \exp( \beta(M+h) \sigma_i ) \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) \left(\sum_{\sigma_1 = \pm 1} \exp(\beta(M+h)\sigma_1)\right) \cdots \left(\sum_{\sigma_n = \pm 1} \exp(\beta(M+h)\sigma_n)\right) \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) \prod_{i=1}^{n} \sum_{\sigma_i = \pm 1} \exp(\beta(M+h)\sigma_i) \\
        & = \exp\left( -\frac{\beta n M^2}{2} \right) 2^n \cosh^n(\beta(M+h)). \\
\end{align*}
Abbiamo dunque:
\[ \frac{\log Z_n}{n} \geq \log 2 + \log\cosh(\beta(M+h)) - \frac{\beta M^2}{2} \eqqcolon \alpha(M, \beta, h) \]
Prendendo il limite per $ n\to\infty $ e il $ \sup $ al variare di $ M $ in $ [-1,1] $ otteniamo
\[ \lim_{n \to +\infty}\frac{\log Z_n}{n} \geq \sup_{M\in[-1,1]} \alpha(M, \beta, h) \eqqcolon \bar{\alpha}(\beta, h). \]

\subsubsection{Secondo bound}
Osserviamo che $ m_n(\sigma) $ può assumere solamente $ n+1 $ valori: $ \im m_n = \left\{ -1, -1 + \frac{2}{n}, \ldots, 1-\frac{2}{n}, 1 \right\} $. Sia ora $ M \in \im m_n $; abbiamo che
\[ \sum_{M\in \im m_n} \exp\left( -\frac{\beta(m_n(\sigma)-M)^2}{2} \right) = 1 + \sum_{\substack{M \in \im m_n \\ M \neq m_n(\sigma)}} \exp\left( -\frac{\beta(m_n(\sigma)-M)^2}{2} \right) \geq 1 \]
e quindi
\begin{align*}
    Z_n & \leq \sum_{M\in \im m_n} \sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) - \frac{\beta n (m_n(\sigma)-M)^2}{2}\right)     \\
        & = \sum_{M\in \im m_n} \exp\left( n \alpha(M, \beta, h) \right)
         \leq \sum_{M\in \im m_n} \exp(n \bar{\alpha}(\beta, h)) = (n+1) \exp(n \bar{\alpha}(\beta, h)).
\end{align*}
Prendendo il logaritmo e facendo il limite si ottiene
\[  \lim_{n \to +\infty} \frac{\log Z_n}{n} \leq \bar{\alpha}(\beta,h). \]
Dunque abbiamo dimostrato che
\[ \lim_{n \to +\infty}\frac{\log Z_n}{n} = \bar{\alpha}(\beta,h) = \sup_{M\in[-1,1]} \alpha(M, \beta, h). \]

Studiamo ora i punti stazionari di $ \alpha(M, \beta, h) $ a fissati $ \beta $ e $ h $:
\begin{equation}\label{eq:mbar}
    \pd{\alpha}{M} = \beta \left( \tanh(\beta (M+h)) - M \right) = 0.
\end{equation}
Nel caso particolare $ h = 0 $, cioè in assenza di campo esterno, l'equazione si riduce a $ \tanh{\beta M} = M $. Questa ha sempre la soluzione $ M = 0 $, mentre solo nel caso $ \beta > 1 $ si aggiungono due soluzioni opposte non nulle. Inoltre in un intorno destro di $ M = 0 $ la derivata $ \pd{\alpha}{M} $ è negativa per $ \beta \leq 1 $, positiva per $ \beta > 1 $. Dunque $ \alpha $ presenta in $ M = 0 $ un massimo per $ \beta \leq 1 $, mentre un minimo per $ \beta > 1 $. Si verifica infine che le soluzioni aggiuntive presenti nel caso $ \beta > 1 $ corrispondono a due massimi (si veda anche la Figura \ref{fig:alpha}).

Nel seguito porremo $ \overline{M}(\beta,h) \coloneqq \argmax_{M\in[-1,1]} \alpha(M,\beta,h) $ e dunque sarà $ \bar{\alpha}(\beta, h) = \alpha(\overline{M}, \beta, h) $\footnote{In caso di ambiguità si scelga, ad esempio, il più grande $ M $ che realizza il massimo.}. Nella Figura \ref{fig:transizione} è riportato l'andamento di $ \pm \overline{M}(\beta, 0) $ in funzione di $ T=\frac{1}{\beta} $. Si osserva che in corrispondenza di $ \beta = 1 $ il sistema presenza una transizione di fase \textcolor{red}{di seconda specie}.

\iffigureon
\begin{figure}[p]
    \centering
    \subfloat{\input{img/cw/h0b>1.tikz}}
    \subfloat{\input{img/cw/h0b<1.tikz}} \\
    \subfloat{\input{img/cw/h>0b>1.tikz}}
    \subfloat{\input{img/cw/h<0b>1.tikz}}
    \caption{$ \alpha(M,\beta,h) $ per diversi valori di $ \beta $ e $ h $ fissati.}
    \label{fig:alpha}
\end{figure}
\begin{figure}[p]
    \centering
    \includegraphics[scale=0.8]{img/cw/transizione.pdf}
    \caption{$ \overline{M} $ in funzione di $ T = \frac{1}{\beta}$.}
    \label{fig:transizione}
\end{figure}
\fi

\subsection{Valore atteso della magnetizzazione media}
Sfruttiamo ora la conoscenza del limite termodinamico dell'energia libera per calcolare il valore atteso della magnetizzazione media e del suo quadrato. Dimostriamo innanzi tutto la seguente
\begin{proposition}
    Siano $ f_n\colon I\subseteq\R \to \R $ funzioni convesse e derivabili in $ x_0 \in I $ e sia $ f\colon I\to\R $ derivabile in $ x_0 $ tale che $ f_n \to f $ puntualmente. Allora
    \[ \lim_{n \to +\infty} f_n'(x_0) = f'(x_0) = \od{}{x} \left( \lim_{n \to +\infty} f'(x_0) \right). \]
\end{proposition}
\begin{proof}\label{prop:convessascambio}
    Sia $ \Delta x \geq 0 $.
    \[ f_n'(x_0) \leq \frac{f_n(x_0+\Delta x)-f_n(x_0)}{\Delta x}. \]
    Prendendo il $ \limsup $ si ha
    \[ \limsup_{n\to +\infty} f'_n(x_0) \leq \frac{f(x_0+\Delta x)-f(x_0)}{\Delta x} \]
    che nel limite $ \Delta x \to 0 $ diventa
    \[ \limsup_{n\to +\infty} f'_n(x_0) \leq f'(x_0). \]
    Analogamente si mostra che vale
    \[ \liminf_{n\to +\infty} f_n'(x_0) \geq f'(x_0) \]
    da cui la tesi.
\end{proof}
Si ha ora\footnote{Si noti che $ \alpha(M,\beta,h) $ è discontinua sulla semiretta $ h=0 $, $ \beta \geq 1 $ e dunque non è possibile derivare rispetto ad $ h $ in tale regione.}
\[ \pd{}{h}\left(\frac{\log Z_n}{n}\right) = \frac{1}{n Z_n} \pd{Z_n}{h} = \frac{1}{nZ_n}\sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) \right) \beta n m_n(\sigma) = \beta \mean{m_n}_{P_\beta}. \]
Abbiamo quindi
\begin{align}
	\label{eq:limmagnmedia}
    \lim_{n \to +\infty}\mean{m_n}_{P_\beta} & = \frac{1}{\beta} \lim_{n \to +\infty} \dpd{}{h}\left(\frac{\log Z_n}{n}\right) = \frac{1}{\beta} \dpd{}{h} \left( \lim_{n \to +\infty} \frac{\log Z_n}{n} \right) \nonumber \\
	                                         & = \frac{1}{\beta} \dpd{\overline{\alpha}}{h}(\beta, h) = \frac{1}{\beta} \dod{}{h} \left( \alpha(\overline{M}(\beta,h), \beta, h)\right) \nonumber \\
                                             & = \frac{1}{\beta} \left[ \dpd{\alpha}{M}(\overline{M}(\beta,h),\beta,h) \dpd{\overline{M}}{h}(\beta,h) + \dpd{\alpha}{h}(\overline{M}(\beta,h),\beta,h) \right] \nonumber \\
                                             & = \tanh(\beta(\overline{M}(\beta,h)+h))  \nonumber \\
                                             & = \overline{M}(\beta, h)
\end{align}
dove si è sostituita la condizione \eqref{eq:mbar} e si sono scambiati limite e derivata per la Proposizione \ref{prop:convessascambio}. Se invece deriviamo rispetto a $ \beta $ otteniamo
\begin{align*}
    \dpd{}{\beta} \left(\frac{\log Z_n}{n}\right) &= \frac{1}{n Z_n} \dpd{Z_n}{\beta} = \frac{1}{n Z_n} \sum_{\sigma \in \Sigma} \exp\left( \frac{\beta n m_n^2(\sigma)}{2} + \beta h n m_n(\sigma) \right) \left( \frac{n m_n^2(\sigma)}{2} + hnm_n(\sigma) \right) \\ &= \frac{1}{2} \mean{m_n^2}_{P_\beta} + h \mean{m_n}_{P_\beta}
\end{align*}
da cui
\begin{align*}
	\frac{1}{2}\lim_{n \to +\infty}\mean{m_n^2}_{P_\beta} + h\lim_{n \to +\infty}\mean{m_n}_{P_\beta}& = \lim_{n \to +\infty} \dpd{}{\beta} \left(\frac{\log Z_n}{n}\right) = \dpd{}{\beta} \left( \lim_{n \to +\infty}\frac{\log Z_n}{n} \right) \\
                                               & = \dpd{\overline{\alpha}}{\beta}(\beta,h) = \dod{}{\beta}\left(\alpha(\overline{M}(\beta,h),\beta,h)\right) \\
                                               & = \dpd{\alpha}{M}(\overline{M}(\beta,h),\beta,h) \dpd{\overline{M}}{\beta}(\beta,h) + \dpd{\alpha}{\beta}(\overline{M}(\beta,h),\beta,h) \\
                                               & = \left(\overline{M}(\beta,h) + h\right) \tanh\left(\beta(\overline{M}(\beta,h)+ h)\right) - \frac{1}{2}\overline{M}^2(\beta,h)\\
                                               & = \frac{1}{2}\overline{M}^2(\beta,h) + h \overline{M}(\beta,h).
\end{align*}
Usando la \ref{eq:limmagnmedia} segue che
\[ \lim_{n \to +\infty} \mean{m_n^2}_{P_\beta} = \overline{M}^2(\beta,h). \]

Infine, anche nel caso $ h=0 $ e $ \beta \geq 1 $ vale $ \mean{m_n}_{P_\beta} = \bar{M}(\beta,0) = 0$; infatti stanti queste ipotesi l'hamiltoniana è invariante sotto la trasformazione $ \sigma_i \to -\sigma_i \ \forall i = 1,\ldots,n$ e in particolare anche la misura di Boltzmann dovrà rimanere invariata, dunque $ \mean{m_n}_{P_\beta} \to \mean{m_n}_{P_\beta} $; d'altra parte $ m_n $ cambia di segno sotto tale trasformazione, per cui dev'essere anche $ \mean{m_n}_{P_\beta} \to -\mean{m_n}_{P_\beta} $, da cui si conclude $ \mean{m_n}_{P_\beta} = 0 $.

\textcolor{red}{Qui iniziava una nuova lezione, ci sono delle ripetizioni}

Abbiamo ottenuto che, tranne sulla semiretta $ h=0 $, $ \beta \geq 1 $, vale
\[ \lim_{n \to +\infty}\mean{m_n}_{P_\beta} = \overline{M}(\beta, h) \]
\[ \lim_{n \to +\infty}\mean{m_n^2}_{P_\beta} = \overline{M}^2(\beta,h) \]
e quindi
\[ \lim_{n \to +\infty}\mean{\left(m_n-\mean{m_n}_{P_\beta}\right)^2}_{P_\beta} = \lim_{n \to +\infty} \left( \mean{m_n^2}_{P_\beta} - \mean{m_n}^2_{P_\beta} \right) = 0. \]
In generale, una famiglia di osservabili $ \mathcal{O}_n $ la cui varianza tenda a zero per $ n\to +\infty $ si dice \emph{automediante}; ciò significa che nel limite termodinamico il valore dell'osservabile diventa deterministico.\\
Se invece $ h=0 $ e $ \beta \geq 1 $ non possiamo derivare l'energia libera in $ h $, ma abbiamo visto che per simmetria
\[ \lim_{n \to +\infty}\mean{m_n}_{P_\beta} = 0 , \]
mentre possiamo ancora derivare in $ \beta $ e ottenere
\[ \lim_{n \to +\infty}\mean{m_n^2}_{P_\beta} = \overline{M}^2(\beta,h). \]
In tale caso si parla di \emph{magnetizzazione bimodale} in cui la distribuzione della magnetizzazione è piccata in modo simmetrico in $ +1 $ e $ -1 $. \\

Se invece $ h=0 $ e $ \beta \geq 1 $ non possiamo derivare l'energia libera in $ h $, ma abbiamo visto che per simmetria
\[ \lim_{n \to +\infty}\mean{m_n}_{P_\beta} = 0 , \]
mentre possiamo ancora derivare in $ \beta $ e ottenere
\[ \lim_{n \to +\infty}\mean{m_n^2}_{P_\beta} = \overline{M}^2(\beta,h). \]
In tale caso si parla di \emph{magnetizzazione bimodale} in cui la distribuzione della magnetizzazione è \emph{peak}-ata in modo simmetrico in $ +1 $ e $ -1 $. \\

Un altro modo per calcolare la varianza di un'osservabile è il seguente: supponiamo di poter riscrivere l'hamiltoniana esplicitando l'osservabile di interesse:
\[ \ham(\sigma) = \mathcal{K}(\sigma) + \lambda\mathcal{O}(\sigma). \]
Allora la varianza di $ \mathcal{O} $ si ottiene calcolando
\begin{align*}
    \dpd[2]{}{\lambda} \left( \log Z_n \right) & = \dpd{}{\lambda} \left[ \frac{1}{Z_n} \sum_{\sigma \in \Sigma} \exp\left( K(\sigma) + \lambda \mathcal{O}(\sigma) \right) \mathcal{O}(\sigma) \right] \\
    & = -\frac{1}{Z_n^2} \left( \sum_{\sigma \in \Sigma} \exp\left( K(\sigma) + \lambda \mathcal{O}(\sigma)\right) \mathcal{O}(\sigma) \right)^2 + \frac{1}{Z_n} \sum_{\sigma \in \Sigma} \exp\left( K(\sigma) + \lambda \mathcal{O}(\sigma) \right) \mathcal{O}^2(\sigma) \\
    & = \mean{\mathcal{O}^2}_{P_\beta} - \mean{\mathcal{O}}^2_{P_\beta} = \mean{ \left(\mathcal{O} - \mean{\mathcal{O}}_{P_\beta} \right)^2 }_{P_\beta}.
\end{align*}
Tornando al caso specifico della magnetizzazione media, prendendo $ \mathcal{O} = \beta n m_n $  e $ \lambda = h $ si ha:
\[ \dpd[2]{}{h}\left( \frac{\log Z_n}{n} \right) = \beta^2 n \mean{\left(m_n-\mean{m_n}_{P_\beta}\right)^2}_{P_\beta} \]
e quindi:
\[ \lim_{n \to +\infty} \mean{\left(m_n-\mean{m_n}_{P_\beta}\right)^2}_{P_\beta} = \frac{1}{\beta^2} \lim_{n \to +\infty} \left[ \frac{1}{n} \dpd[2]{}{h}\left( \frac{\log Z_n}{n} \right) \right] = 0. \]

Da ultimo possiamo calcolare esplicitamente la distribuzione di $ m_n $, cioè $ f(x) = P_\beta \left( m_n^{-1} (\{x\}) \right) $. Detto $ k $ il numero di spin uguali a 1, notiamo che $ m_n $ può assumere solo i valori della forma $ (-1 + 2k/n) $ con $ k=0,\ldots,n $; se poniamo $ m_n = x $, allora $ k = (1+x)n/2 $ e quindi
\begin{align}\label{eq:distrMagn}
    f(x) & = P_\beta \left( m_n^{-1} (\{x\}) \right) = \sum_{\sigma\in m_n^{-1}(\{x\})} \frac{1}{Z_n} \exp\left( \beta\left( \frac{n}{2}m_n^2(\sigma) + nhm_n(\sigma) -\frac{1}{2}\right) \right) \nonumber \\
    & = \frac{1}{Z_n} \binom{n}{\frac{1}{2}(1+x) n} \exp\left( \beta\left( \frac{n}{2}x^2 + nhx - \frac{1}{2} \right) \right).
\end{align}
Infatti tutti i $ \sigma $ tali che $ m_n(\sigma) = x $ hanno la stessa probabilità, e sono $ \binom{n}{k} $ (il numero di modi in cui si possono scegliere gli spin uguali a 1). In Figura \ref{fig:distrMagn} sono riportati alcuni grafici della \eqref{eq:distrMagn} al variare di $ h $ e $ \beta $.

\iffigureon
\begin{figure}[p]
    \centering
    \subfloat[$ h=0, \beta=1 $.]{\input{img/cw/magnh0b1.tikz}}
    \subfloat[$ h=0, \beta<1 $.]{\input{img/cw/magnh0b<1.tikz}} \\
    \subfloat[$ h=0, \beta>1 $: si ha magnetizzazione spontanea.]{\input{img/cw/magnh0b>1.tikz}}
    \subfloat[$ h>0 $: indipendentemente da $ \beta $ il picco è spostato nel verso del campo esterno.]{\input{img/cw/magnh>0b>1.tikz}}
    \caption{distribuzione di $ m_n $ data dalla \eqref{eq:distrMagn} per $ n = 165 $ al variare di $ h $ e $ \beta $.}
    \label{fig:distrMagn}
\end{figure}
\fi

\subsection{Valore atteso di un'osservabile generica}
Poiché lo spazio degli stati è finito, una generica osservabile $ \mathcal{O}\colon \Sigma\to\R $ è completamente caratterizzata da $ \card{\Sigma} = 2^n $ valori. Possiamo quindi vedere $ \mathcal{O} $ come una funzione di $ n $ variabili appartenenti $ \{+1,-1\} $. Estendiamo ora tale osservabile ad una funzione $ \tilde{\mathcal{O}}\colon\R^n\to\R $, ad esempio facendo passare un polinomio in $ n $ variabili per i $ 2^n $ punti assegnati. Vogliamo cioè trovare una funzione del tipo
\[ \tilde{\mathcal{O}} (x_1,\ldots,x_n) = \sum_{\abs{i}=0}^{N(n)} \alpha_{i} x^i \]
dove $ i $ è un multi indice e $ x = (x_1, \ldots, x_n) $, in modo tale che
\[ \tilde{\mathcal{O}}(\sigma_1, \ldots, \sigma_n) = \mathcal{O}((\sigma_1,\ldots,\sigma_n)). \]
Volendo studiare $ \lim_{n \to +\infty} \mean{\mathcal{O}}_{P_\beta} $, possiamo quindi ridurci allo studio di generici prodotti della forma $ \sigma_1 \cdots \sigma_k $.

\textcolor{red}{
    Non è chiaro il senso di tutto ciò. Innanzi tutto non è ben definito cosa voglia dire considerare la stessa osservabile in sistemi a diversa taglia, poiché non c'è sempre un modo ovvio (come invece nel caso di $ m_n $) per esprimere l'osservabile come funzione di $ n $.
    Inoltre, sia il grado $ N $ che i coefficienti del polinomio interpolante dipendono, in generale, da $ n $, per cui non è sufficiente conoscere $ \mean{x}_{P_\beta} $ per calcolare $ \lim_{n \to +\infty} \mean{\mathcal{O}}_{P_\beta} $.
}

\begin{lemma}\label{lemma:cusumano}
    Siano $ f_n\colon \Sigma_n \to \R $ equilimitate e sia $ \mean{f} \coloneqq \lim\limits_{n \to +\infty}\mean{f_n}_{P_{\beta, n}} $. Allora
    \[ \mean{f_n m_n}_{P_{\beta,n}} \to \overline{M}(\beta,h) \mean{f}. \]
\end{lemma}
\begin{proof}
    Per la diseguaglianza di Cauchy-Schwartz si ha
    \begin{align*}
        \abs{ \mean{f_n\cdot \left(m_n-\overline{M}(\beta,h) \right) }_{P_{\beta,n}} }^2  & \leq \mean{f_n^2}_{P_{\beta,n}} \cdot \mean{\left (m_n-\overline{M}(\beta,h)\right )^2}_{P_{\beta,n}} \\
        & \leq K \cdot \left( \mean{m_n^2}_{P_{\beta,n}} + \overline{M}^2(\beta,h) - 2\overline{M}(\beta,h)\mean{m_n}_{P_{\beta,n}} \right) \\
        & \to K \left( \overline{M}^2(\beta,h) + \overline{M}^2(\beta,h) - 2\overline{M}^2(\beta,h) \right) = 0
    \end{align*}
    e quindi
    \begin{align*}
        \lim_{n \to +\infty} \mean{f_n m_n}_{P_{\beta,n}} & = \lim_{n \to +\infty} \mean{f_n\cdot \left(m_n-\overline{M}(\beta,h) \right) }_{P_{\beta,n}} + \lim_{n \to +\infty}\mean{f_n}_{P_{\beta,n}} \overline{M}(\beta,h) \\
        & = \overline{M}(\beta,h)\mean{f}. \qedhere
    \end{align*}
\end{proof}

\begin{thm}
    Se $ \beta < 1 $ oppure $ \beta \geq 1 \wedge h \neq 0 $ vale
    \[ \lim_{n \to +\infty} \mean{\sigma_1, \ldots, \sigma_k}_{P_\beta} = \overline{M}^k(\beta, h). \]
\end{thm}
\begin{proof}
    Si procede per induzione. Osserviamo che
    \[ \mean{m_n}_{P_\beta} = \frac{1}{n} \sum_{i=1}^{n}\mean{\sigma_i}_{P_\beta} = \mean{\sigma_1}_{P_\beta} \]
    ma $ \mean{m_n}_{P_\beta} \to_n \overline{M}(\beta,h) $, da cui la tesi per $ k=1 $. Per il passo induttivo osserviamo che
    \begin{align*}
        \mean{\sigma_1 \cdots \sigma_k m_n}_{P_{\beta,n}} & = \frac{1}{n} \sum_{i=1}^{n} \mean{\sigma_1 \cdots \sigma_k \sigma_i}_{P_{\beta,n}} = \frac{1}{n} \sum_{i=1}^k\mean{\sigma_1\cdots\sigma_k\sigma_i}_{P_{\beta,n}} + \frac{1}{n} \sum_{i=k+1}^n \mean{\sigma_1\cdots\sigma_k\sigma_i}_{P_{\beta,n}} \\
        & = \frac{1}{n} \sum_{i=1}^k {\mean{\sigma_1 \cdots \sigma_{i-1} \sigma_{i+1} \cdots \sigma_k}}_{P_{\beta,n}} + \frac{1}{n} \sum_{i=k+1}^n \mean{\sigma_1 \cdots \sigma_k \sigma_{k+1}}_{P_{\beta,n}} \\
        & = \frac{k}{n} \mean{\sigma_1 \cdots \sigma_{k-1}}_{P_{\beta,n}} + \frac{n-k}{n} \mean{\sigma_1 \cdots \sigma_k \sigma_{k+1}}_{P_{\beta,n}}.
    \end{align*}
    Passando al limite, usando il Lemma \ref{lemma:cusumano} con $ f(\sigma_1, \ldots, \sigma_n) = \sigma_1 \cdots \sigma_k $ e l'ipotesi induttiva si ottiene la tesi.
\end{proof}

\subsection{Altri metodi per il calcolo dell'energia libera}
\textcolor{red}{Mancante}