\section{Catene di Markov topologiche}
Sia $ \Gamma \subseteq \{1, \ldots, N\}^{2} $ un grafo \emph{connesso} e \emph{diretto} sui vertici $ \{1, \ldots, N\} $ con al più una sola freccia da $ i \to j $. A tale grafo possiamo associare la \emph{matrice di adiacenza} $ A $ che ci dice se esiste o meno un cammino da $ i \to j $:
\[
  A_{ij} = \begin{cases}
    1 & (i, j) \in \Gamma \\
    0 & (i, j) \notin \Gamma
  \end{cases}
\]

\begin{example} \label{ex:markov-top}
  \textcolor{red}{Immagine e matrice di adiacenza dell'esempio fatto a lezione, molto utile per avere un'idea concreta della definizione.}
\end{example}

Vogliamo modellizzare come sistema dinamico una ``passeggiata'' infinita nel futuro e nel passato su tale grafo. A tale scopo consideriamo l'insieme dei possibili cammini sul grafo
\[
  \Sigma_A \coloneqq \{x = (x_i)_{i\in\Z} \in \{1, \ldots, N\}^{\Z} : \forall i \in \Z, \ (x_i, x_{i+1}) \in \Gamma\}
\]
e lo \emph{shift} sinistro su $ N $ simboli $ \sigma \colon \Sigma_N \to \Sigma_N $ dove $ \Sigma_N \coloneqq \{1, \ldots, N\}^{\Z} $. Sappiamo già che se dotiamo $ \Sigma_N $ della distanza $ d(x, y) = N^{-a(x, y)} $ dove $ a(x, y) = \inf\{\abs{i} : x_i \neq y_i\} $, $ \Sigma_N $ diventa uno spazio metrico compatto e $ \sigma $ un omeomorfismo. Osserviamo ora che
\begin{itemize}
\item $ \Sigma_A $ è invariante per lo \emph{shift} ovvero $ \sigma(\Sigma_A) \subseteq \Sigma_A $;
\item $ \Sigma_A $ è un chiuso di $ \Sigma_N $ rispetto alla topologia indotta da $ d $ e quindi compatto.
\end{itemize}
Pertanto è ben definita la mappa $ \sigma_A \colon \Sigma_A \to \Sigma_A $ data dalla restrizione $ \sigma_A \coloneqq \sigma\lvert_{\Sigma_A} $ e la terna $ (\Sigma_A, d, \sigma_A) $ risulta essere un sistema dinamico topologico. Chiamiamo tale sistema dinamico \emph{catena di Markov topologica}.

\begin{exercise}
  Trovare una funzione $ f \colon [0, 1] \to [0, 1] $ lineare a tratti e una partizione di $ [0, 1] $ la cui dinamica simbolica sia la catena di Markov dell'esempio \ref{ex:markov-top}. \\
  \emph{Hint}: se fossero tutti collegati la mappa $ 3x \pmod{1} $ andrebbe bene.
\end{exercise}

\begin{definition}
  Sia $ A $ una matrice reale $ N \times N $ a coefficienti non negativi. Diciamo che $ A $ è:
  \begin{itemize}
  \item \emph{irriducibile} se $ \forall i, j, \; \exists k \in \N : (A^k)_{ij} > 0 $;
  \item \emph{primitiva} se $ \exists k \in \N : \forall i, j, \ (A^k)_{ij} > 0 $;
  \end{itemize}
\end{definition}

Ricordiamo inoltre i seguenti risultati
\begin{itemize}
\item $ A $ irriducibile $ \Rightarrow $ $ \Gamma $ fortemente connesso, ovvero $ \forall i, j $ esiste un cammino finito $ i \to j $;
\item $ A $ irriducibile $ \Rightarrow $ $ A + \Id $ primitiva;
\item $ A $ primitiva con $ k \in \N $ tale che $ \forall i, j, \ (A^k)_{ij} > 0 $ $ \Rightarrow $ $ \forall h \geq k, \ \forall i, j, \ (A^{h})_{ij} > 0 $.
\end{itemize}

Al fine di calcolare l'entropia topologica di una catena di Markov (con qualche ipotesi aggiuntiva) enunciamo il seguente teorema ``sublime''.
\begin{thm}[Perron-Frobenius]
  Sia $ A $ una matrice primitiva. Allora esiste $ \lambda_A > 0 $ reale autovalore destro (o sinistro) di $ A $ che gode inoltre delle seguenti proprietà    \begin{enumerate}[label=(\roman*)]
  \item $ \forall \lambda \neq \lambda_A $ autovalore di $ A $ si ha $ \abs{\lambda} < \lambda_A $;
  \item autovettori destri (o sinistri) relativi a $ \lambda_A $ hanno componenti reali tutte strettamente positive;
  \item l'autospazio relativo a $ \lambda_A $ ha dimensione 1;
  \item $ \lambda_A $ è una radice semplice del polinomio caratteristico $ P_A(\lambda) $.
  \end{enumerate}
\end{thm}

Grazie al teorema di Perron-Frobenius possiamo in realtà mostrare il seguente enunciato che richiede ipotesi più deboli su $ A $ ma che ci fornisce disuguaglianze larghe.
\begin{thm}
  Sia $ A $ una matrice reale a coefficienti non negativi. Allora esiste un autovalore destro (o sinistro) $ \lambda_A \geq 0 $ tale che $ \forall \lambda $ autovalore destro (o sinistro) vale $ \abs{\lambda} \geq \lambda_A $ e autovettori destro (o sinistri) di $ \lambda_A $ hanno componenti reali non negative.
\end{thm}
\begin{proof}
  \textcolor{red}{Fissato $ \epsilon > 0 $ sia $ A(\epsilon) $ una matrice $ N\times N $ data da $ A_{ij}(\epsilon) = A_{ij} + \epsilon $. Essendo $ A $ a coefficienti non negatici, $ A(\epsilon) $ è a coefficienti strettamente positivi e dunque primitiva. Per il teorema di Perron-Frobenius esiste allora un autovalore destro reale $ \lambda_A(\epsilon) > 0 $ con le proprietà date dal teorema. Da qui si conclude passando le varie disuguaglianze al limite in $ \epsilon \to 0 $.}
\end{proof}

\begin{proposition} \label{thm:Perron-Frobenius-debole}
  Sia $ (\Sigma_A, d, \sigma_A) $ una catena di Markov topologica. Supponiamo che $ A $ matrice di adiacenza di $ \Gamma $ primitiva e sia $ \lambda_A $ l'autovalore dato dal teorema di Perron-Frobenius. Allora
  \begin{equation}
    h_{\mathrm{top}}(\sigma_A) = \log{(\lambda_A)}.
  \end{equation}
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante, serve un lemma di analisi numerica}
\end{proof}