\section{Trasformazioni canoniche}
\subsection{Cambio di coordinate in una ODE}
Per semplicità di notazione lavoriamo su $ \R^m \times \R $ invece che su un aperto $ \mathcal{O} $ in esso contenuto. Consideriamo una generica equazione differenziale alle derivate ordinarie
\begin{equation} \label{eqn:ode}
  \begin{cases}
    \dot{\vec{x}} = \vec{v}(\vec{x}, t) \\
    \vec{x}(0) = \vec{x}_0
  \end{cases}
\end{equation}
dove $ \vec{v} \colon \R^{m} \times \R \to \R^m $ è un campo vettoriale di classe $ \mathcal{C}^{\infty} $. Vogliamo studiare in che modo trasforma l'equazione \eqref{eqn:ode} sotto cambio di coordinate. Ricordiamo che dire che $ \vec{x}(t) $ è soluzione dell'equazione \eqref{eqn:ode} vuol dire che $ \vec{x}(0) = \vec{x}_0 $ e per ogni $ t \in (a, b) \subseteq \R $ si ha $ \od{}{t} \vec{x}(t) = \vec{v}(\vec{x}(t), t) $. \\

Vogliamo trovare una $ \vec{w} \colon \R^m \times \R \to \R $ che ci permetta di scrivere scrivere il sistema \eqref{eqn:ode} come
\begin{equation} \label{eqn:ode-trasf}
  \begin{cases}
    \dot{\vec{y}} = \vec{w}(\vec{y}, t) \\
    \vec{y}(0) = \vec{y}_0
  \end{cases}
\end{equation}
dove $ \vec{y} $ è l'esito di un cambio di coordinate. Dire che $ \vec{y}(t) $ è soluzione dell'equazione \eqref{eqn:ode-trasf} vuol dire che $ \vec{y}(0) = \vec{y}_0 $ e per ogni $ t \in (a, b) \subseteq \R $ si ha $ \od{}{t} \vec{y}(t) = \vec{w}(\vec{y}(t), t) $. Sia quindi
\begin{align*}
  \vec{f} \colon \R^m \times \R & \to \R^m \times \R \\
  (\vec{x}, t) & \mapsto (\boldsymbol{\varphi}(\vec{x}, t), t) \eqqcolon (\vec{y}, t)
\end{align*}
un diffeomorfismo $ \mathcal{C}^\infty $  di $ \R^m \times \R $ che agisce sul tempo come l'identità e sulla variabile $ \vec{x} $ con una funzione $ \boldsymbol{\varphi} \colon \R^m \times \R \to \R^m $. Essendo un diffeomorfismo sia
\begin{align*}
  \vec{g} \colon \R^m \times \R & \to \R^m \times \R \\
  (\vec{y}, t) & \mapsto (\boldsymbol{\psi}(\vec{y}, t), t) \eqqcolon (\vec{x}, t)
\end{align*}
la trasformazione inversa dove $ \boldsymbol{\psi} \colon \R^m \times \R \to \R^m $. Si ha per ogni componente
\begin{align*}
  \dod{}{t} y_i(t) & = \dod{}{t} \left( \varphi_i(\vec{x}(t), t)\right) = \sum_{j=1}^{2n} \dpd{\varphi_i}{x_j}(\vec{x}(t), t) \; \dod{}{t} x_j(t)  + \dpd{\varphi_i}{t}(\vec{x}(t), t) \\
                   & = \sum_{j=1}^{2n} \dpd{\varphi_i}{x_j}(\vec{x}(t), t) \; v_j(\vec{x}(t), t)  + \dpd{\varphi_i}{t}(\vec{x}(t), t) \\
                   & = \left[\sum_{j=1}^{2n} \dpd{\varphi_i}{x_j} v_j + \dpd{\varphi_i}{t}(\vec{x}(t), t)\right] (\vec{x}(t), t) \\
                   & = \left[\sum_{j=1}^{2n} \dpd{\varphi_i}{x_j} v_j + \dpd{\varphi_i}{t}(\vec{x}(t), t)\right] (\boldsymbol{\psi}(\vec{y}(t), t), t)
\end{align*}
Indicando con $ J = \left(\dpd{\varphi_i}{x_j}\right) $ lo jacobiano rispetto alla variabile $ \vec{x} $ del cambio di coordinate abbiamo che il campo trasformato è
\begin{equation}
  \vec{w}(\vec{y}, t) \coloneqq \left[ J \vec{v} + \dpd{\boldsymbol{\varphi}}{t}\right] (\boldsymbol{\psi}(\vec{y}, t), t).
\end{equation}
Mentre chiaramente $ \vec{y}_0 = \boldsymbol{\varphi}(\vec{x}_0, 0) $.

\subsection{Trasformazioni canoniche}
Vogliamo applicare quanto detto nella sezione precedente alle equazioni di Hamilton
\[
  \dot{\vec{x}} = \Gamma \, \nabla_\vec{x} \ham(\vec{x}, t).
\]
In tale caso il campo vettoriale è $ \vec{v}(\vec{x}, t) \coloneqq \Gamma \, \nabla_\vec{x} \ham(\vec{x}, t) $. Consideriamo la trasformazione delle coordinate $ \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \vec{x} = \boldsymbol{\psi}(\vec{y}, t) $ la trasformazione inversa. Le equazioni di Hamilton diventano
\[
  \dot{\vec{y}} = \vec{w}(\vec{y}, t) = \left[ J \, \Gamma \, \nabla_\vec{x} \ham + \dpd{\boldsymbol{\varphi}}{t}\right] (\boldsymbol{\psi}(\vec{y}, t), t).
\]
In generale non è detto che per ogni hamiltoniana $ \ham $ la trasformazione delle coordinate lasci le equazioni di Hamilton invarianti in forma. In altre parole non è garantito che sotto la trasformazione $ \vec{x} \to \vec{y} $ per ogni hamiltoniana $ \ham(\vec{x}, t) $ esista un'altra hamiltoniana $ \mathcal{K}(\vec{y}, t) $ tale che
\[
  \vec{w}(\vec{y}, t) = \left[ J \, \Gamma \, \nabla_\vec{x} \ham + \dpd{\boldsymbol{\varphi}}{t}\right] (\boldsymbol{\psi}(\vec{y}, t), t) = \Gamma \, \nabla_\vec{y} \mathcal{K}(\vec{y}, t).
\]
È ora nostro interesse riuscire a caratterizzare le trasformazioni per cui questo accade.

\begin{example} \label{es:ham-quadratica-canoniche}
  Restringiamoci per ora al caso dei sistemi hamiltoniani lineari autonomi. Sia quindi $ \ham(\vec{x}, t) = \frac{1}{2} \vec{x}^T S \vec{x} $ con $ S \in \gl(2n, \R) $ simmetrica. Vogliamo caratterizzare l'insieme delle trasformazioni lineari dello spazio delle fasi $ \boldsymbol{\varphi}(\vec{x}, t) \coloneqq A \vec{x} $ con $ A \in \GL(2n, \R) $ che lasciano le equazioni di Hamilton invarianti in forma. Le equazioni di Hamilton sono $ \dot{\vec{x}} = \Gamma S \vec{x} $ che sotto la trasformazione diventano
  \[
    \dot{\vec{y}} = A \Gamma S A^{-1} \vec{y}.
  \]
  Affinché la trasformazione sia canonica deve esistere una matrice $ C \in \gl(2n, \R) $ simmetrica tale che
  \[
    A \Gamma S A^{-1} = \Gamma C \quad \iff \quad - A^T \Gamma A \Gamma S = A^T C A
  \]
  Ora essendo $ C $ simmetrica, anche $ A^T C A $ è simmetrica, per cui detta $ \Lambda \coloneqq A^T \Gamma A $ che è antisimmetrica si ottiene che $ \Lambda \Gamma S $ è una matrice simmetrica ovvero usando che $ S^T = S $
  \[
    \Lambda \Gamma S = - S^T \Gamma \Lambda^T = S \Gamma \Lambda
  \]
  Dunque $ \boldsymbol{\varphi} $ è una trasformazione che lascia le equazioni di Hamilton invarianti in forma per le hamiltoniane lineari indipendenti dal tempo se e solo se per ogni $ S $ simmetrica vale $ \Lambda \Gamma S = S \Gamma \Lambda $. Mostriamo che tale condizione è equivalente a chiedere che $ \exists \mu \neq 0  : \Lambda = \mu \Gamma $, ovvero $ A $ è $ \mu $-simplettica: $ A^T \Gamma A = \mu \Gamma $. \\
  Scriviamo le matrici come matrici a blocchi
  \[
    \Lambda =
    \begin{pmatrix}
      a & b \\
      -b^T & d
    \end{pmatrix}
    \qquad
    S =
    \begin{pmatrix}
      e & f \\
      f^T & g
    \end{pmatrix}
    \qquad
    \Gamma =
    \begin{pmatrix}
      0 & \Id \\
      -\Id & 0
    \end{pmatrix}
  \]
  con $ a, b, c, d, e, f, g \in \gl(n, \R) $ tali che $ a^T = -a $, $ d^T = -d $, $ e^T = e $ e $ g^T = g $. Dobbiamo imporre
  \[
    \Lambda \Gamma S =
    \begin{pmatrix}
      a f^T - b e & a g - b f \\
      - b^T f^T - d e & - b^T g - d f
    \end{pmatrix}
    =
    S \Gamma \Lambda
    =
    \begin{pmatrix}
      -e b^T - f a & e d - f b \\
      -f^T b^T - g a & f^T d - g b
    \end{pmatrix}
  \]
  ovvero
  \begin{equation}
    \begin{cases}
      a f^T - b e = -e b^T - f a \\
      a g - b f = e d - f b \\
      - b^T f^T - d e = -f^T b^T - g a \\
      - b^T g - d f  = f^T d - g b
    \end{cases}
  \end{equation}
  Dal momento che tale equazione deve essere soddisfatta per ogni matrice $ S $ simmetrica possiamo porre $ e = f = 0 $ da cui ricaviamo $ a g = g a = 0 $. Scegliendo $ g = \Id $ otteniamo $ a = 0 $. Scegliendo ora $ e = 0 $ troviamo che per ogni $ f \in \gl(n, \R) $ deve essere $ f b = b f $. Ma una matrice commuta con ogni matrice se e solo se è multiplo dell'identità, quindi $ \exists \mu \neq 0 : b = \mu \Id $. Ricaviamo quindi che $ e d =  d e = 0 $ da cui scegliendo analogamente a prima $ e = \Id $ otteniamo $ d = 0 $. In conclusione
  \[
    \Lambda =
    \begin{pmatrix}
      0 & \mu \Id \\
      - \mu \Id & 0
    \end{pmatrix}
    = \mu \Gamma.
  \]
  Abbiamo quindi trovato che restringendosi ai sistemi lineari autonomi, le trasformazioni lineari delle coordinate che lasciano le equazioni di Hamilton invarianti in forma sono tutte e sole quelle che hanno matrice $ \mu $-simplettica.
\end{example}

Precisiamo che nell'esempio \ref{es:ham-quadratica-canoniche} \emph{non} abbiamo risposto alla domanda iniziale: le trasformazioni a cui siamo interessati devono lasciare le equazioni di Hamilton invarianti in forma per \emph{tutte} le hamiltoniane, mentre nell'esempio ci siamo ristetti alle forme quadratiche indipendenti dal tempo. Tuttavia vedremo che la condizione trovata si trasferisce al caso generale a livello infinitesimo.

\begin{definition}[trasformazione canonica]
  Una trasformazione $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ si dice canonica se la sua matrice jacobiana $ J(\vec{x}, t) \coloneqq J_\vec{x} \boldsymbol{\varphi}(\vec{x}, t) $ rispetto alla variabile $ \vec{x} $ è una matrice simplettica $ \forall (\vec{x}, t) \in \mathcal{O} $. Nel caso in cui $ \boldsymbol{\varphi}(\vec{x}, t) $ non dipenda esplicitamente dal tempo si parla di trasformazioni \emph{completamente} canoniche.
\end{definition}

\begin{lemma} \label{lem:jacob-simplettica}
  Se $ J(\vec{x}, t) $ è una matrice simplettica $ \forall (\vec{x}, t) \in \mathcal{O} $, allora $ A \coloneqq \dpd{J}{t} J^{-1} $ è una matrice hamiltoniana.
\end{lemma}
\begin{proof}
  Essendo $ J $ simplettica si ha
  \[
    \pd{J^T}{t} \Gamma J + J^T \Gamma \pd{J}{t} = \pd{}{t} \left(J^T \Gamma J\right) = \pd{}{t} \Gamma = 0
  \]
  da cui otteniamo che
  \[
    J^T \Gamma \pd{J}{t} = - \pd{J^T}{t} \Gamma J \quad \Rightarrow \quad - (J^T)^{-1} \pd{J^T}{t} \Gamma = \Gamma \pd{J}{t} J^{-1}.
  \]
  Ci basta mostrare che la matrice $ \Gamma A $ è simmetrica
  \[
    \left(\Gamma \dpd{J}{t} J^{-1} \right)^T = - (J^{-1})^T \pd{J^T}{t} \Gamma = - (J^T)^{-1} \pd{J^T}{t} \Gamma = \Gamma \dpd{J}{t} J^{-1}. \qedhere
  \]
\end{proof}

\begin{thm}
  Le trasformazioni canoniche lasciano le equazioni di Hamilton invarianti in forma. Inoltre, se la trasformazione è completamente canonica, la nuova hamiltoniana è uguale alla vecchia espressa in termini delle nuove coordinate.
\end{thm}
\begin{proof}
  Come abbiamo visto il nuovo campo hamiltoniano dopo il cambio di coordinate è
  \[
    \vec{w}(\vec{y}, t) \coloneqq J(\boldsymbol{\psi}(\vec{y}, t), t) \ \Gamma \, (\nabla_\vec{x} \ham)(\boldsymbol{\psi}(\vec{y}, t), t) + \dpd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t).
  \]
  Definiamo $ \hat{\ham}(\vec{y}, t) \coloneqq \ham(\boldsymbol{\psi}(\vec{y}, t), t) $ che è l'hamiltoniana scritta nelle nuove coordinate. Essendo la trasformazione un diffeomorfismo si ha anche $ \ham(\vec{x}, t) = \hat{\ham}(\boldsymbol{\varphi}(\vec{x}, t), t) $ così per la \emph{chain rule}
  \[ (\nabla_{\vec{x}} \ham) (\boldsymbol{\psi}(\vec{y},t),t) = \left( \nabla_{\vec{x}} \hat{\ham}(\boldsymbol{\varphi}(\vec{x},t),t) \right)(\boldsymbol{\psi}(\vec{y},t),t) \]
  e quindi
  \begin{align*}
    (\nabla_{\vec{x}} \ham)_i (\boldsymbol{\psi}(\vec{y},t),t) &= \dpd{}{x_i}\left( \hat{\ham}(\boldsymbol{\varphi}(\vec{x},t),t) \right)(\boldsymbol{\psi}(\vec{y},t),t)
    \\ &=  \sum_{j=1}^{2n} \dpd{\hat{H}}{y_j} (\vec{y},t) \cdot \dpd{\varphi_j}{x_i} (\boldsymbol{\psi}(\vec{y},t),t) =
         \sum_{j=1}^{2n} (\nabla_{\vec{y}}\hat{\ham})_j(\vec{y},t) \cdot \jac_{ji}(\boldsymbol{\psi}(\vec{y},t),t)
  \end{align*}
  da cui
  \[ (\nabla_{\vec{x}} \ham) (\boldsymbol{\psi}(\vec{y},t),t) = \jac^T(\boldsymbol{\psi}(\vec{y},t),t) \; (\nabla_{\vec{y}}\hat{\ham})(\vec{y},t). \]
  Pertanto il campo hamiltoniano si scrive in termini di $ \hat{\ham} $ come
  \begin{align*}
    \vec{w}(\vec{y}, t) & = J(\boldsymbol{\psi}(\vec{y}, t), t) \ \Gamma \, J^{T}(\boldsymbol{\psi}(\vec{y}, t), t) \ (\nabla_\vec{y} \hat{\ham})(\vec{y}, t) + \dpd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t) \\
                        & = \Gamma \nabla_\vec{y} \hat{\ham}(\vec{y}, t) +  \dpd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t)
  \end{align*}
  dove abbiamo usato la simpletticità di $ J^T $ che discende dalla simpletticità di $ J $. Ora se la trasformazione è completamente canonica $ \pd{\boldsymbol{\varphi}}{t} = 0 $ e si ha la tesi. \\
  Se la trasformazione, invece, non è completamente canonica ci basta mostrare che $ \pd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t) $ è un campo hamiltoniano. Grazie al Teorema \ref{thm:campo-hamiltoniano} è sufficiente provare che matrice $ A(\vec{y}, t) \coloneqq J_\vec{y} \left(\pd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t)\right) $ è una matrice hamiltoniana per ogni $ (\vec{x}, t) $. Infatti
  \[
    A_{ij} = \pd{}{y_j} \left(\dpd{\hat{y}_i}{t}(\boldsymbol{\psi}(\vec{y}, t), t)\right) = \sum_{k=1}^{2n} \md{\hat{y}_i}{2}{t}{}{x_k}{} (\hat{\vec{x}}(\vec{y}, t), t) \ \pd{\hat{x}_k}{y_j} (\vec{y}, t) = \left(\dpd{J}{t} \ J^{-1}\right)_{ij}
  \]
  che è una matrice hamiltoniana grazie al Lemma \ref{lem:jacob-simplettica}. Dunque esiste una hamiltoniana $ \mathcal{K}_0 $ tale che $ \dpd{\boldsymbol{\varphi}}{t}(\boldsymbol{\psi}(\vec{y}, t), t) = \Gamma \nabla_\vec{y} \mathcal{K}_0(\vec{y}, t) $. Così
  \[
    \vec{w}(\vec{y}, t) = \Gamma \hat{\ham}(\vec{y}, t) + \Gamma \nabla_\vec{y} \mathcal{K}_0(\vec{y}, t).
  \]
  Definendo quindi la nuova hamiltoniana $ \mathcal{K}(\vec{y}, t) \coloneqq \hat{\ham}(\vec{y}, t) + \mathcal{K}_0(\vec{y}, t) $ abbiamo che le equazioni di Hamilton trasformate sono
  \[
    \dot{\vec{y}} = \vec{w}(\vec{y}, t) = \Gamma \nabla_\vec{y} \mathcal{K}(\vec{y}, t)
  \]
  ovvero sono invarianti in forma sotto la trasformazione $ \vec{x} \to \vec{y} $.
\end{proof}

\begin{example}[trasformazioni di scala]
  Consideriamo la trasformazione per $ i = 1, \ldots, n $
  \[
    \begin{cases}
      Q_i = \mu_i q_i \\
      P_i = \nu_i p_i
    \end{cases}
  \]
  Nella notazione usata in precedenza, $ \vec{x} = (\vec{q}, \vec{p}) $ e $ \vec{y} = (\vec{Q}, \vec{P}) $. \\
  Mostriamo che tale trasformazione è $ \rho $-simplettica se e solo se $ \forall i = 1, \ldots, n, \ \mu_i \nu_i = \rho $ e che in tale caso la nuova hamiltoniana è
  \[
    \mathcal{K}(\vec{Q}, \vec{P}, t) = \rho \ham\left(
      \begin{pmatrix}
        \mu_1^{-1} & & \\
        & \ddots & \\
        & & \mu_n^{-1}
      \end{pmatrix}
      \vec{Q},
      \begin{pmatrix}
        \nu_1^{-1} & & \\
        & \ddots & \\
        & & \nu_n^{-1}
      \end{pmatrix}
      \vec{P},
      t
    \right)
  \]
  \textcolor{red}{Mancante}
\end{example}

\begin{example}[scambio di coordinate]
  Consideriamo la trasformazione per $ i = 1, \ldots, n $
  \[
    \begin{cases}
      Q_i = p_i \\
      P_i = -q_i
    \end{cases}
  \]
  Mostriamo che è canonica e quindi conserva la struttura delle equazioni di Hamilton. Tale risultato evidenzia il fatto che nel formalismo hamiltoniano le $ q $ e le $ p $ sono allo stesso livello e interscambiabili. \\
  \textcolor{red}{Mancante}
\end{example}

\begin{example}[trasformazioni puntuali]
  Consideriamo una trasformazione generica delle posizioni $ \vec{Q} = \hat{\vec{Q}}(\vec{q}) $. Nel formalismo lagrangiano, tale trasformazione induce una trasformazione dei momenti coniugati data da $ \vec{P} = ((J^T)^{-1}(\vec{q}))\vec{p} $ dove $ J(\vec{q}) \coloneqq J \hat{\vec{Q}} $. Come è noto nella teoria lagrangiana, le equazioni di Eulero-Lagrange sono invarianti in forma sotto una trasformazioni delle posizioni. Mostriamo che tale trasformazione è simplettica e che quindi lascia invariata anche la struttura delle equazioni di Hamilton. \\
  \textcolor{red}{Mancante}
\end{example}

Il seguente risultato stabilisce il legame tra la canonicità di una trasformazione e le parentesi di Poisson.
\begin{thm}
  Sia $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ una trasformazione di coordinate $ \vec{x} \to \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \boldsymbol{\psi} $ l'inversa. Le seguenti proprietà sono equivalenti:
  \begin{enumerate}[label=(\roman*)]
  \item $ \boldsymbol{\varphi} $ è una trasformazione canonica;
  \item $ \forall f, g \in \mathcal{C}^{\infty}(\mathcal{O}, \R) $, dette $ F(\vec{y}, t) = f(\boldsymbol{\psi}(\vec{y}, t), t) $ e $ G(\vec{y}, t) = g(\boldsymbol{\psi}(\vec{y}, t), t) $ le osservabili trasformate, si ha \[ \{f, g\}_{\vec{x}} = \{F, G\}_{\vec{y}}; \]
  \item posto $ \vec{x} = (q, p) $ e $ \vec{y} = (Q, P) $ si ha
    \begin{equation}
      \{P_i, P_j\}_{\vec{x}} = \{Q_i, Q_j\}_{\vec{x}} = 0 \qquad \{Q_i, P_j\}_{\vec{x}} = \delta_{ij}.
    \end{equation}
  \end{enumerate}
\end{thm}


\subsection{Forma di Poincaré-Cartan}
\emph{Setting}: Lo spazio delle fasi $ \mathcal{O} \subseteq \R^{2n} \times \R $ è un aperto semplicemente connesso. Un punto dello spazio delle fasi è $ (\vec{x}, t) = (\vec{q}, \vec{p}, t) \in \mathcal{O} $.

\begin{definition}[forma non singolare, direzioni caratteristiche]
  Una 1-forma $ \omega \colon \mathcal{O} \to (\R^{2n+1})^{*} $ data da $ \omega(\vec{x}) = \sum_{i=1}^{2n+1} \omega_i(\vec{x}) \dif{x_i} $ su $ \mathcal{O} $ si dice non singolare se $ \forall \vec{x} \in \mathcal{O} $ la matrice antisimmetrica
  \[
    A_{ij}(\vec{x}) = \pd{\omega_i}{x_j}(\vec{x}) - \pd{\omega_j}{x_i}(\vec{x})
  \]
  ha rango massimo ($ \operatorname{rank}A(\vec{x}) = 2n $). In questo caso, per ogni $ \vec{x} \in \mathcal{O} $ il nucleo della matrice $ \ker A(\vec{x}) $ è unidimensionale e definisce quindi un campo di direzioni nello spazio delle fasi, dette direzioni caratteristiche. Le curve integrali associate al campo di direzioni caratteristiche sono dette caratteristiche di $ \omega $.
\end{definition}

\begin{proposition}
  Se $ \omega $ e $ \xi $ sono forme non singolari e la loro differenza è esatta $ \omega - \xi = \dif{F} $, allora le due forme hanno le stesse caratteristiche.
\end{proposition}

\begin{definition}[forma di Poincaré-Cartan]
  Data un'hamiltoniana $ \ham \colon \mathcal{O} \to \R $ si chiama forma di Poincaré-Cartan la 1-forma su $ \mathcal{O} $
  \begin{equation}
    \omega(\vec{q}, \vec{p}, t) \coloneqq \sum_{i=1}^{n} p_i \dif{q_i} - \ham(\vec{q}, \vec{p}, t) \dif{t}.
  \end{equation}
\end{definition}

\begin{oss}
  La forma di Poincaré-Cartan è non singolare.
\end{oss}

\begin{proposition}
  Le caratteristiche della forma di Poincaré-Cartan sono le soluzioni delle equazioni di Hamilton associate ad $ \ham $.
\end{proposition}
\begin{proof}
  \textcolor{red}{Sketch} La matrice associata alla forma di Poincaré-Cartan è
  \[
    A =
    \begin{pmatrix}
      0                            & \Id_{n}                      & \nabla_{\vec{q}} \ham \\
      -\Id_{n}                     & 0                            & \nabla_{\vec{p}} \ham \\
      -(\nabla_{\vec{q}} \ham)^{T} & -(\nabla_{\vec{p}} \ham)^{T} & 0
    \end{pmatrix}
  \]
  quindi $ \vec{v} = (\nabla_{\vec{p}}\ham, -\nabla_{\vec{q}} \ham, 1) \in \ker{A} $ è tangente alle linee integrali delle equazioni di Hamilton, essendo $ \od{}{t}(\vec{q}, \vec{p}, 1) = \vec{v} $.
\end{proof}

\begin{thm}\label{thm:canonica-poincare-cartan}
  Sia $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ una trasformazione di coordinate $ \vec{x} \to \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \boldsymbol{\psi} $ la sua inversa. Siano inoltre $ \mathcal{K}(\vec{y}, t) = \ham(\boldsymbol{\psi}(\vec{y}, t), t) $ l'hamiltoniana scritta nelle nuove coordinate e $ \omega $ e $ \xi $ le forme di Poincaré-Cartan associate rispettivamente a $ \ham $ e a $ \mathcal{K} $. Allora $ \boldsymbol{\varphi} $ è una trasformazione canonica se e solo se la forma $ \omega - \xi = \dif{F} $ è esatta.
\end{thm}

\begin{example}
  Vogliamo capire per quali valori di $ \alpha, \beta, \gamma \in \R $ la seguente trasformazione
  \[
    \begin{cases}
      p = \alpha \sqrt{P} \cos{(\gamma Q)} \\
      q = \beta \sqrt{P} \sin{(\gamma Q)}
    \end{cases}
  \]
  è canonica. Osserviamo che, nelle variabili $ P $ e $ Q $ si ha
  \begin{align*}
    p\dif{q} & = \alpha \sqrt{P} \cos{(\gamma Q)} \frac{\beta}{2\sqrt{P}} \sin{(\gamma Q)} \dif{P} + \alpha \sqrt{P} \cos{(\gamma Q)} \beta \gamma \sqrt{P} \cos{(\gamma Q)} \dif{Q} \\
             & = \frac{\alpha \beta}{4} \sin{(2\gamma Q)} \dif{P} + \alpha \beta \gamma P \cos^{2}{(\gamma P)} \dif{Q} \\
             & = \frac{\alpha \beta \gamma}{2} \left(\frac{1}{2\gamma} \sin{(2\gamma Q)} \dif{P} + P \cos{(2 \gamma Q)} \dif{Q}\right) + \frac{\alpha \beta \gamma}{2} P \dif{Q} \\
             & = \frac{\alpha \beta \gamma}{2} \dif{\left(\frac{1}{2\gamma} \sin{(2\gamma Q)} P\right)} + \frac{\alpha \beta \gamma}{2} P \dif{Q}.
  \end{align*}
  Ma allora, detta $ \omega $ la forma di Poincaré-Cartan nelle variabili $ (q, p) $ e $ \xi $ quella relativa alle variabili $ (Q, P) $, si ha
  \[
    \omega - \xi = p \dif{q} - H(q, p, t) \dif{t} - P \dif{Q} + K(Q, P, t) = \dif{\left(\frac{\alpha \beta \gamma}{2} \frac{1}{2\gamma} \sin{(2\gamma Q)} P\right)} + \left(\frac{\alpha \beta \gamma}{2} - 1\right)P \dif{Q}
  \]
  essendo $ K(Q, P, t) = H(q(Q, P), p(Q, P), t) $. Grazie al teorema appena enunciato concludiamo che la trasformazione è canonica se e solo se $ \alpha \beta \gamma = 2 $.
\end{example}

\subsection{Condizioni di Lie e canonicità del flusso hamiltoniano}
Mostriamo ora come, in realtà, la canonicità di una trasformazione può essere verificata a ``tempo fissato''. A tale scopo diamo la seguente
\begin{definition}[differenziale virtuale]
  Sia $ f \colon \mathcal{O} \to \R $ una funzione di classe $ \mathcal{C}^{\infty} $. Definiamo il differenziale virtuale di $ f(\vec{x}, t) $ come il differenziale fatto solo ``rispetto alla variabile $ \vec{x} $'':
  \begin{equation}
    \vdif{f} \coloneqq \sum_{i=1}^{2n} \dpd{f}{x_i} \dif{x_i} = \dif{f} - \dpd{f}{t} \dif{t}.
  \end{equation}
\end{definition}
Definiamo inoltre la forma di Poincaré-Cartan a ``tempo fissato'' associata all'hamiltoniana $ \ham $ come
\begin{equation}
  \tilde{\omega}(\vec{q}, \vec{p}, t) \coloneqq \sum_{i=1}^{n} p_i \dif{q_i} = \omega(\vec{q}, \vec{p}, t) + \ham(\vec{q}, \vec{p}, t) \dif{t}.
\end{equation}
Possiamo allora togliere la ``dipendenza temporale'' nel teorema~\ref{thm:canonica-poincare-cartan} grazie al seguente
\begin{thm}\label{thm:condizioni-lie}
  Sia $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ una trasformazione di coordinate $ \vec{x} \to \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \boldsymbol{\psi} $ la sua inversa. Siano inoltre $ \mathcal{K}(\vec{y}, t) = \ham(\boldsymbol{\psi}(\vec{y}, t), t) $ l'hamiltoniana scritta nelle nuove coordinate e $ \tilde{\omega} $ e $ \tilde{\xi} $ le forme di Poincaré-Cartan a ``tempo fissato'' associate rispettivamente a $ \ham $ e a $ \mathcal{K} $. Allora $ \boldsymbol{\varphi} $ è una trasformazione canonica se e solo se la forma $ \tilde{\omega} - \tilde{\xi} = \vdif{F} $ è esatta nel senso del differenziale virtuale.
\end{thm}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

Data un'hamiltoniana $ \ham $, consideriamo ora le equazioni di Hamilton
\[
  \begin{cases}
    \dot{q}_i = \dpd{\ham}{p_i}, & \dot{p}_i = -\dpd{\ham}{q_i} \\
    q_i(0) = Q_i, & p_i(0) = P_i \\
  \end{cases}
\]
Osserviamo che il flusso hamiltoniano è una trasformazione dello spazio delle fasi dipendente dal tempo che al tempo $ t $ porta il punto iniziale $ (\vec{Q}, \vec{P}) $ nel punto $ (\vec{q}, \vec{p}) = \Phi^{t}_\ham(\vec{Q}, \vec{P}) $. Possiamo allora vedere il flusso hamiltoniano come un cambio di coordinate $ \boldsymbol{\psi}(\vec{Q}, \vec{P}, t) = (\Phi^{t}_\ham(\vec{Q}, \vec{P}), t) $. Ciò che è sorprendente è il seguente
\begin{thm}[canonicità del flusso hamiltoniano]
  L'evoluzione temporale, ovvero la trasformazione di coordinate $ \boldsymbol{\psi} $ indotta dal flusso hamiltoniano, è una trasformazione canonica.
\end{thm}
\begin{proof}
  \textcolor{red}{Road map:} Definita l'azione hamiltoniana
  \begin{align*}
    S(\vec{Q}, \vec{P}, t) & \coloneqq \int_{0}^{t} \left\{\sum_{i=1}^{n} p_i(Q, P, \tau) \dpd{q_i}{\tau}(Q, P, \tau) - \ham_i(q(Q, P, \tau), p(Q, P, \tau), \tau)\right\} \dif{\tau} \\
                           & = \int_{\gamma(\vec{Q}, \vec{P}, t)} \omega
  \end{align*}
  dove $ \gamma(\vec{Q}, \vec{P}, t) $ è la curva che segue l'evoluzione temporale del punto $ (\vec{Q}, \vec{P}) $ dal tempo $ \tau=0 $ al tempo $ \tau=t $ data dalle equazioni di Hamilton. Si verifica che questa soddisfa le condizioni di Lie
  \[
    \tilde{\omega} - \tilde{\xi} = \vdif{S}. \qedhere
  \]
\end{proof}

\subsection{Funzioni generatrici}
Con il teorema~\ref{thm:canonica-poincare-cartan} abbiamo mostrato che la canonicità di una trasformazione dello spazio delle fasi è equivalente all'esistenza di una funzione $ F $ tale che
\[
  \omega - \xi = \sum_{i=1}^{n} \left(p_i \dif{q_i} - P_i\dif{Q_i}\right) + (\mathcal{K} - \ham) \dif{t} = \dif{F}.
\]
Fino ad ora abbiamo inteso che in questa scrittura occorre esplicitare le variabili $ Q_i, P_i $ rispetto alle variabili $ q_i, p_i $ o viceversa; in altri termini la funzione $ F $ è definita sullo spazio delle fasi della variabili ``maiuscole'' o di quelle ``minuscole''. Tuttavia tale scrittura suggerisce di poter esprimere la funzione $ F $ in termini delle variabili ``miste'' $ q_i $ e $ Q_i $: quando questo è possibile possiamo leggere la relazione precedente nel seguente modo
\[
  \begin{dcases}
    \dpd{F}{q_i} = p_i, & \dpd{F}{Q_i} = -P_i \\
    \mathcal{K} = \ham + \dpd{F}{t}
  \end{dcases}
\]
In altri termini, data un cambio di variabili, se troviamo una funzione $ F $ che soddisfa le equazioni $ p_i = \dpd{F}{q_i}, P_i = \dpd{F}{Q_i} $ allora la trasformazione è canonica e la nuova hamiltoniana è $ \mathcal{K} = \ham + \dpd{F}{t} $. $ F $ si dice \emph{funzione generatrice} della trasformazione. \\

Chiaramente, questo risultato ha come presupposto la possibilità di poter invertire parzialmente la trasformazione di coordinate: di solito una trasformazione di coordinate esprime le nuove coordinate in funzione di quelle vecchie $ (\vec{Q}, \vec{P}) = \boldsymbol{\varphi}(\vec{q}, \vec{p}, t) $ o viceversa; ora invece stiamo chiedendo che esista una funzione tale che $ (\vec{p}, \vec{P}) = \boldsymbol{\varphi'}(\vec{q}, \vec{Q}, t) $. Tuttavia non è detto che questa trasformazione sia possibile; potrebbe invece essere possibile esprimere altri set di coordinate miste in funzione delle restanti. Ad esempio potrebbe essere possibile esprimere il set $ (\vec{q}, \vec{P}) $ in funzione di $ (\vec{p}, \vec{Q}) $; questa scelta di coordnate darà origine a una diversa funzione generatrice. Possiamo passare da una funzione generatrice all'altra mediante un'opportuna \emph{trasformata di Legendre}; le quattro funzioni generatrici più frequenti sono le seguenti:
\begin{itemize}
\item $ F_1 = F_1(\vec{q}, \vec{Q}, t) $ che soddisfa
  \[
    \dpd{F_1}{q_i} = p_i \qquad \dpd{F_1}{Q_i} = -P_i;
  \]
\item $ F_2 = F_2(\vec{q}, \vec{P}, t) = F_1 + \sum_{i} Q_i P_i $ che soddisfa
  \[
    \dpd{F_2}{q_i} = p_i \qquad \dpd{F_2}{P_i} = Q_i;
  \]
\item $ F_3 = F_3(\vec{p}, \vec{Q}, t) = F_1 - \sum_{i} p_i q_i $ che soddisfa
  \[
    \dpd{F_3}{p_i} = -q_i \qquad \dpd{F_3}{Q_i} = -P_i;
  \]
\item $ F_4 = F_4(\vec{p}, \vec{P}, t) = F_1 + \sum_{i} Q_i P_i - \sum_{i} p_i q_i $ che soddisfa
  \[
    \dpd{F_4}{p_i} = -q_i \qquad \dpd{F_{4}}{P_i} = Q_i.
  \]
\end{itemize}

\begin{example}\label{ex:identita}
  La funzione $ F_{2}(\vec{q},\vec{P}) = \sum_{i} q_{i}P_{i} $ è la generatrice dell'identità, che è ovviamente una trasformazione canonica. Infatti per le formule sopra si ha
  \[ p_{i} = \dpd{F_{2}}{q_{i}} = P_{i} \qquad Q_{i} = \dpd{F_{2}}{P_{i}} = q_{i}. \]
  Osserviamo che non è ovviamente possibile esprimere la generatrice dell'identità solo rispetto alle posizioni o solo rispetto agli impulsi coniugati (casi $ F_{1} $ e $ F_{4} $).
\end{example}
