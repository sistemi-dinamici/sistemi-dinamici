\section{Spazi di Hilbert}
\emph{Setting}: $ V $ è uno spazio vettoriale su $ \K = \R \text{ o } \C $.

\begin{definition}[prodotto scalare]
  Una funzione $ {\langle \; , \, \rangle} \colon V \times V \to \K $ è un prodotto scalare se
  \begin{enumerate}[label = (\roman*)]
  \item $ \forall v \in V, \ {\langle v, v \rangle} \geq 0 $ e $ {\langle v, v \rangle} \iff v = 0 $;
  \item $ \forall a_1, a_2 \in \K, \forall v_1, v_2, w \in V, \ {\langle a_1v_1 + a_2v_2, w \rangle} = a_1{\langle v_1, w\rangle} + a_2{\langle v_2, w\rangle} $;
  \item $ \forall b_1, b_2 \in \K, \forall w_1, w_2, v \in V, \ {\langle v, b_1w_1 + b_2w_2 \rangle} = \bar{b}_1{\langle v, w_1\rangle} + \bar{b}_2{\langle v, w_2\rangle} $
  \end{enumerate}
\end{definition}

Un prodotto scalare su $ V $ induce una funzione $ \norm{\;} \colon V \to \K  $ detta \emph{norma} definita come $ {\norm{v} \coloneqq \sqrt{{\langle v, v \rangle}}} $. Una norma a sua volta indice una distanza $ d \colon V \times V \to \K $ data da \linebreak $ d(v, w) \coloneqq \norm{v - w} $ che definisce una struttura di spazio metrico e di topologia.

\begin{proposition}[Cauchy-Schwarz]
  Per ogni $ v, w \in V $ vale $ \abs{{\langle v, w \rangle}} \leq \norm{v} \cdot \norm{w} $.
\end{proposition}

\begin{definition}[sottospazio ortogonale]
  Se $ W \subseteq V $ è un sottospazio definiamo l'ortogonale di $ W $ come
  \[
    W^{\perp} \coloneqq \{v \in V : \forall w \in W, \ {\langle v, w \rangle} = 0\}.
  \]
\end{definition}

\begin{lemma}
  Il prodotto scalare è una funzione continua rispetto alla topologia indotta dalla norma (e a quella euclidea in arrivo).
\end{lemma}
\begin{proof}
  Fissati $ (v_0, w_0) \in V \times V $, usando la triangolare e Cauchy-Schwarz
  \[
    \abs{{\langle v, w \rangle} - {\langle v_0, w_0 \rangle}} = \abs{{\langle v - v_0, w\rangle} + {\langle v_0, w - w_0 \rangle}} \leq \norm{v - v_0} \, \norm{w} + \norm{v_0} \, \norm{w - w_0}.
  \]
  Quindi se $ v \in B_\delta(v_0) $ e $ w \in B_\delta(w_0) $ si ha
  \[
    \abs{{\langle v, w \rangle} - {\langle v_0, w_0 \rangle}} \leq \delta(\norm{w} + \norm{v_0}) \leq \delta(\norm{w_0} + \norm{v_0} + \delta). \qedhere
  \]
\end{proof}

\begin{proposition}
  $ W^\perp $ è un sottospazio chiuso di $ V $.
\end{proposition}
\begin{proof}
  Il fatto che sia un sottospazio è ovvio per la linearità del prodotto scalare nella prima componente. Fissiamo ora $ w \in W $. Allora $ \{w\}^\perp = \{v \in V : {\langle v, w \rangle} = 0\} $ è un chiuso (controimmagine continua di un chiuso) da cui $ W^\perp = \bigcap_{w \in W} \{w\}^\perp $ è chiuso essendo intersezione di chiusi.
\end{proof}

\begin{proposition}
  Vale che $ \overline{W} \subseteq (W^\perp)^\perp $.
\end{proposition}
\begin{proof}
  Se $ w \in W $ allora $ {\langle v, w \rangle} = 0 $ per ogni $ v \in W^\perp $ da cui $ W \subseteq (W^\perp)^\perp \Rightarrow \overline{W} \subseteq (W^\perp)^\perp $ essendo l'ortogonale di $ W^\perp $ un chiuso.
\end{proof}

\begin{definition}[base ortonormale]
  Una base ortonormale di $ V $ è un insieme $ \{e_j\}_{j \in J} $ con $ J $ al più numerabile tale che
  \begin{enumerate}[label=(\roman*)]
  \item $ \forall j, k \in J, \ {\langle e_j, e_k \rangle} = \delta_{jk} $;
  \item\label{def:coin:base} $ \forall v \in V, \ \exists \{v_j\}_{j \in J} \subseteq \K : v = \displaystyle{\sum_{j \in J} v_j e_j} $.
  \end{enumerate}
\end{definition}

Nel caso in cui $ J $ sia numerabile la condizione \ref{def:coin:base} è da intendersi come convergenza rispetto alla norma, cioè
\[
  \norm{v - \sum_{j = 1}^{N} v_j e_j} \to 0 \quad \text{ per } N \to +\infty.
\]

\begin{proposition}
  Uno spazio vettoriale $ V $ è separabile\footnote{Uno spazio topologico si dice separabile se contiene un sottoinsieme denso numerabile.} se e solo se ammette una base al più numerabile.
\end{proposition}

\begin{definition}[spazio di Hilbert]
  $ V $ si dice spazio di Hilbert se è separabile e completo rispetto alla norma indotta dal prodotto scalare.
\end{definition}

\begin{thm}[criterio di completezza]
  Sia $ (V, \norm{\;}) $ uno spazio normato. Allora le seguenti proprietà sono equivalenti.
  \begin{enumerate}[label=(\roman*)]
  \item $ V $ è completo.
  \item $ \forall (v_k)_{k \in \N} \subseteq V, \ \displaystyle{\sum_{k \in \N} \norm{v_k} < +\infty \Rightarrow \sum_{k \in \N} v_k} \text{ converge} $.
  \end{enumerate}
\end{thm}

\begin{example}[$ \R^n, \C^n $]
  Lo spazio $ \C^n $ ($ \R^n $) con il prodotto hermitiano standard (scalare standard) $ {\langle v, w \rangle} = v \cdot w \coloneqq v_1\bar{w}_1 + \ldots v_n\bar{w}_n $ è uno spazio di Hilbert. Più in generale su $ \R^n $ possiamo definire il prodotto scalare $ {\langle v, w \rangle} = v \cdot Qw $ con $ Q $ matrice simmetrica definita positiva che dota $ \R^n $ di una struttura di spazio di Hilbert, di cui una base ortonormale è data dagli autovettori di $ Q $.
\end{example}

\begin{example}[spazio $ L^2 $]
  Consideriamo lo spazio $ L^2(X, \mathcal{F}, \mu) \coloneqq \faktor{\mathscr{L}^2(X,\mathcal{F},\mu)}{\sim} $ dove
  \[
    \mathscr{L}^2(X,\mathcal{F},\mu) \coloneqq \left\{f \colon X \to \K \ \text{ misurabili tali che } \int_{X} \abs{f}^2 \dif{\mu} < +\infty\right\}
  \]
  e $ f \sim g \iff \mu\text{-q.o.} \ f = g $. Su $ L^2(X, \mathcal{F}, \mu) $ definiamo il prodotto scalare
  \[
    {\langle f, g \rangle} \coloneqq \int_{X} f(x) \overline{g(x)} \dif{\mu(x)}
  \]
  che induce la norma
  \[
    \norm{f}_2 \coloneqq \left(\int_{X} \abs{f(x)}^2 \dif{\mu(x)}\right)^{1/2}.
  \]
  $ L^2(X, \mathcal{F}, \mu) $ con questo prodotto scalare è uno spazio di Hilbert.
\end{example}

\begin{example}[spazio $ l^2 $]
  Consideriamo lo spazio
  \[
    \ell^2(\N) \coloneqq \left\{(x_n)_{n \in \N} \subseteq \K : \sum_{n \in \N} \abs{x_n}^2 < +\infty\right\}.
  \]
  Tale spazio può essere visto come un esempio di $ \mathscr{L}^2(X, \mathcal{F}, \mu) $ in cui $ X = \N $, $ \mathcal{F} = \mathscr{P}(\N) $ e $ \mu(E) = \card{E} $ (misura conta punti). Infatti in tale caso le funzioni $ f \colon \N \to \K $ sono successioni a valori nel campo e l'integrale si riduce a una sommatoria. Osserviamo inoltre che, poiché l'unico insieme di misura nulla è il vuoto, la relazione di equivalenza è banale, cioè le classi di equivalenza sono i singoletti di $ \ell^2(\N) $: $ l^2(\N) \simeq \ell^2(\N) = \mathscr{L}^2(\N, \mathscr{P}(\N), \mu) $. Il prodotto scalare su $ l^2(\N) $ è
  \[
    {\langle (x_n), (y_n) \rangle} = \sum_{n \in \N} x_n \bar{y}_n.
  \]
  Tale prodotto scalare è ben definito essendo la serie assolutamente convergente (infatti $ \abs{x_n \bar{y}_n} \leq \abs{x_n}^2 /2 + \abs{y_n}^2 /2 $). Una base di $ l^2(\N) $ è $ \{e^j\}_{j \in \N} $ dove $ e^j_n \coloneqq \delta_{jn} $.
\end{example}

\begin{proposition}
  Lo spazio $ l^2(\N) $ è completo.
\end{proposition}
\begin{proof}
  {
    \newcommand{\eps}{\varepsilon}
    Si consideri una successione $ (x^{(k)})_{k\in\N} \subseteq l^2 $ di Cauchy, con $ x^{(k)} = (x_n^{(k)})_{n\in\N} $. Fissato $ \eps > 0 $ ciò significa che:
    \[ \exists K_{\eps} > 0 : \forall k,h \geq K_{\eps},\ \norm{x^{(k)} - x^{(h)}}_2 < \sqrt{\eps}. \]
    Si ha quindi definitivamente in $ k $ ed $ h $:
    \[ \abs{x_n^{(k)} - x_n^{(h)}}^2 \leq \sum_{n\in\N} \abs{x_n^{(k)} - x_n^{(h)}}^2 = \norm{x^{(k)} - x^{(h)}}^2_2 < \eps \Rightarrow \abs{x_n^{(k)} - x_n^{(h)}} < \sqrt{\eps} \]
    e a fisso $ n $ la successione $ (x_n^{(k)})_{k\in\N} $ è di Cauchy in $ \R $, ed ha quindi un limite $ \tilde{x}_n $. Mostreremo adesso che $ \tilde{x} = (\tilde{x}_n)_{n\in\N} $ è in $ l^2 $ ed è il limite $ l^2 $ della successione  $ (x^{(k)})_{k\in\N} $. Abbiamo che, sempre per $ k,h \geq K_{\eps} $:
    \[
      \sum_{n=0}^{M} \abs{x_n^{(k)} - x_n^{(h)}}^2 \leq
      \sum_{n=0}^{+\infty} \abs{x_n^{(k)} - x_n^{(h)}}^2 =
      \norm{x^{(k)} - x^{(h)}}^2_2 < \eps
    \]
    da cui:
    \[
      \sum_{n=0}^{M} \abs{x_n^{(k)} - \tilde{x}_n}^2 =
      \sum_{n=0}^{M} \abs{x_n^{(k)} - \lim\limits_{h\to +\infty} x_n^{(h)}}^2 =
      \lim\limits_{h\to +\infty} \sum_{n=0}^{M} \abs{x_n^{(k)} - x_n^{(h)}}^2 < \eps
    \]
    e, passando al limite in $ M $:
    \[
      \norm{x_n^{(k)} - \tilde{x}_n}_2^2 =
      \sum_{n=0}^{+\infty} \abs{x_n^{(k)} - \tilde{x}_n}^2 < \eps.
    \]
    Ciò mostra che $ x^{(k)} - \tilde{x} \in l^2 $ e quindi che $ \tilde{x}\in l^2 $, ed anche che $ \lim\limits_{k\to +\infty} \norm{x_n^{(k)} - \tilde{x}_n}_2^2 = 0 $.
  }
\end{proof}

\begin{exercise}
  Dimostrare che lo spazio $ L^2([0, 1]) $ è completo.
\end{exercise}



\section{Convergenza debole}
\emph{Setting}: $ V $ spazio di Hilbert su $ \K = \R \text{ o } \C $.

\begin{definition}[convergenza debole]
  Diciamo che $ (v_n)_{n \in \N} \subseteq V $ converge debolmente a $ v \in V $, e scriviamo $ v_n \todeb v $, se $ \forall w \in V, \ {\langle v_n, w \rangle} \to {\langle v, w \rangle} $ (o analogamente $ {\langle v_n - v, w \rangle} \to 0 $) dove la convergenza del prodotto scalare è intesa rispetto alla distanza euclidea su $ \K $.
\end{definition}

\begin{oss}
  Su $ \C^n $ o più in generale spazi di dimensione finita la convergenza debole è equivalente a quella rispetto alla norma. Infatti per avere la convergenza in norma è sufficiente guardare la convergenza sugli elementi di una base $ \{e^j\} $ e $ v_n^j = {\langle v_n, e^j\rangle} \to {\langle v, e^j\rangle} = v^j $. Il viceversa è invece ovvio.
\end{oss}

\begin{lemma}
  Sia $ (v_n)_{n \in \N} \subseteq V $. Se $ v_n \to v $ allora $ v_n \todeb v $.
\end{lemma}

\begin{lemma}
  Sia $ (v_n)_{n \in \N} \subseteq V $. Se $ v_n \todeb v $ e $ \norm{v_n} \to \norm{v} $ allora $ v_n \to v $.
\end{lemma}
\begin{proof}
  Infatti $ \norm{v_n - v}^2 = \norm{v_n}^2 + \norm{v}^2 - 2 {\langle v_n, v \rangle} \to 2\norm{v}^2 - 2\norm{v}^2 = 0 $.
\end{proof}

\begin{thm} \label{thm:Banach-Alaoglu-facile}
  La palla $ B_1(0) \subseteq V $ è relativamente compatta rispetto alla convergenza debole, cioè per ogni $ (v_n)_{n \in \N} \subseteq V $ con $ \norm{v_n} \leq 1 $ esiste una sottosuccessione $ (v_{n_k})_{k \in \N} \subseteq V $ e un $ v \in V $ tali che $ v_{n_k} \todeb v $.
\end{thm}
\begin{proof}
  (Idea)
  Per ogni $ v \in V $, consideriamo $ D_v \coloneqq \overline{B_{\norm{v}}(0)} \subseteq \C $ e l'insieme $ K \coloneqq \prod_{v \in V} D_v $. Essendo $ D_v $ compatto per ogni $ v $, $ K $ è compatto per il teorema di Tychonoff. Sia ora
  \begin{align*}
    \varphi \colon B_1(0) \subseteq V & \to K \\
    w & \mapsto ({\langle w, v \rangle})_{v \in V}.
  \end{align*}
  Tale mappa è ben definita essendo $ \abs{{\langle w, v \rangle}} \leq \norm{w} \norm{v} \leq \norm{v} $. Basterà allora mostrare che la ``restrizione di $ \varphi $ nell'immagine'' $ \psi \colon B_1(0) \to \varphi(B_1(0)) $ è continua con inversa continua (prendendo su $ K $ al topologia indotta e su $ B_1(0) $ quella indotta della convergenza debole) e che $ \varphi(B_1(0)) $ è chiusa. Essendo allora $ \varphi(B_1(0)) $ chiusa in un compatto otteniamo che è compatta e dato che $ \psi $ è un omeomorfismo otteniamo che $ B_1(0) $ è compatta.
\end{proof}

\begin{proposition}\label{prop:completezza}
  Sia $ \{e_n\}_{n \in \N} $ un insieme ortonormale in $ V $. Le seguenti proprietà sono equivalenti.
  \begin{enumerate}[label=(\roman*)]
  \item $ \forall v \in V, \ v = \sum_{n = 0}^{+\infty} {\langle v, e_n \rangle} e_n $;
  \item $ \forall v \in V, \ \exists \{a_n\}_{n \in \N} \subseteq \K : v = \sum_{n = 0}^{+\infty} a_n e_n $;
  \item $ \operatorname{Span}{(\{e_n\})} $\footnote{Con $ \operatorname{Span}{(\{w_n\})} $ si intende l'insieme delle combinazioni lineari \emph{finite} di elementi $ w_n $.} è denso in $ V $;
  \item $ {\langle v, e_n \rangle} = 0 \ \forall n \in \N \Rightarrow v = 0 $.
  \end{enumerate}
  Se è verificata una di queste proprietà si dice che $ \{e_n\} $ è un \emph{insieme completo}.
\end{proposition}
\begin{proof}
  Mostriamo le varie implicazioni.
  \begin{description}
  \item[$ (i) \Rightarrow (ii) $] Ovvio.
  \item[$ (ii) \Rightarrow (iii) $] Prendo $ v \in V $ che scrivo come $ \sum_{n = 0}^{+\infty} a_n v_n $. Ora se $ w_{N} \coloneqq \sum_{n = 0}^{N} a_ne_n $ ho per ipotesi che $ w_N \to v $.
  \item[$ (iii) \Rightarrow (iv) $] Sia $ v \in V $ tale che $ \forall n \in \N, \ {\langle v, e_n \rangle} = 0 $. Siano $ (w_k)_{k \in \N} \subseteq \operatorname{Span}{(\{e_n\})} $ tali che $ w_k \to v $. Ma allora $ w_k \todeb v $, quindi $ {\langle v, v \rangle} = \lim_{k} {\langle v, w_k \rangle} = 0 $ essendo $ {\langle v, w_k \rangle} = \sum_{n = 0}^{N} a_n^k {\langle v, e_n \rangle} = 0 $. Così $ \norm{v} = 0 \Rightarrow v = 0 $.
  \item[$ (iv) \Rightarrow (i) $] Fissato $ v \in V $ poniamo $ v_N \coloneqq \sum_{n = 0}^{N} {\langle v, e_n \rangle} e_n $. Essendo $ \{e_n\} $ un insieme ortonormale
    \[
      {\langle v - v_N, v_N \rangle} = {\langle v, v_N \rangle} - {\langle v_N, v_N \rangle} = \sum_{n = 0}^{N} \overline{{\langle v, e_n \rangle}}{\langle v, e_n \rangle} - \norm{v_N}^2 = \sum_{n=0}^{N} \abs{\langle v, e_n \rangle}^2 - \sum_{n=0}^{N} \abs{\langle v, e_n \rangle}^2 = 0.
    \]
    Pertanto per il teorema di Pitagora
    \[
      \norm{v}^2 = \norm{v - v_N}^2 + \norm{v_N}^2 \geq \norm{v_N}^2 = \sum_{n = 0}^{N} \abs{{\langle v, e_n \rangle}}^2.
    \]
    Ora $ \norm{v_N} \leq \norm{v} $ quindi per il Teorema \ref{thm:Banach-Alaoglu-facile} esiste $ (v_{N_k}) $ e $ \tilde{v} $ con $ \norm{\tilde{v}} \leq \norm{v} $ tale che $ v_{N_k} \todeb \tilde{v} $. Osservando quindi che $ \forall n \in \N $ si ha
    \[
      {\langle v - \tilde{v}, e_n \rangle} = {\langle v, e_n \rangle} - {\langle \tilde{v}, e_n \rangle} = {\langle v, e_n \rangle} - \lim_{k \to + \infty} {\langle v_{N_k}, e_n \rangle} = {\langle v, e_n \rangle} - {\langle v, e_n \rangle} = 0
    \]
    da cui per ipotesi $ \tilde{v} = v $. Mostriamo che $ (v_{N_k}) $ tende a $ v $ in norma. Infatti
    \[
      \norm{v - v_{N_k}}^2 = {\langle v - v_{N_k}, v - v_{N_k}\rangle} = {\langle v - v_{N_k}, v \rangle} - {\langle v - v_{N_k}, v_{N_k} \rangle} = {\langle v - v_{N_k}, v \rangle} \to 0.
    \]
    Finora abbiamo dunque estratto una sottosuccessione di $ v_N $ che tende a $ v $. Possiamo applicare lo stesso procedimento su un'arbitraria sottosuccessione di $ v_N $, estraendo da ciascuna una sotto-sottosuccessione convergente a $ v $. Per un noto lemma, quindi, l'intera successione $ v_N $ deve convergere a $ v $. \qedhere
  \end{description}
\end{proof}

\section{Spazio $ L^2([0, 2\pi]) $}
Consideriamo lo spazio $ L^2([0, 2\pi]) $.
Una sua base ortonormale è $ e_n(\theta) = \frac{1}{\sqrt{2\pi}}e^{i n \theta} $ con $ n \in \Z $ e $ \theta \in [0, 2\pi] $ ed è detta \emph{base di Fourier}.

\begin{oss}
  Al contrario delle funzioni di $ L^2(\T^1) $, le funzioni di $ L^2([0, 2\pi]) $ possono assumere valori diversi in $ 0 $ e $ 2\pi $. Prima di passare $ [0,2\pi] $ al quoziente, quindi, i due spazi non sono del tutto analoghi.
\end{oss}

\begin{definition}[polinomi trigonometrici]
  Chiamiamo polinomi trigonometrici di grado minore o uguale a $ N $ gli elementi di
  $ T_N \coloneqq \left\{\sum_{n = -N}^{N} a_n e^{i n \theta}, \ a_n \in \C\right\} $.
\end{definition}

\begin{thm}
  La base di Fourier è un insieme completo di $ L^2([0,2\pi]) $.
\end{thm}
\begin{proof}
  Road map
  \begin{enumerate}
  \item Se $ f \in L^2([0, 2\pi]) $, $ \hat{f}(n) \coloneqq {\langle f, e_n \rangle} = \frac{1}{2\pi} \int_{0}^{2\pi} f(\theta) e^{-i n\theta} \dif{\theta} $ sono i candidati coefficienti;
  \item Mostro che per ogni $ p \in T_N $ vale $ \norm{f - \sum_{n = -N}^{N} \hat{f}(n) e_n}_2 \leq \norm{f - p}_2 $;
  \item Considero la famiglia di mollificatori $ \{Q_k\} $, con $ Q_k(t) \coloneqq \lambda_k (\frac{1 + \cos{t}}{2})^k $, dove $ \lambda_k $ è il fattore di normalizzazione. Questo è un polinomio trigonometrico di grado $ k $ e che ``tende alla delta di Dirac''. Mostro che
    \[
      (f * Q_k)(x) \coloneqq \int_{-\pi}^{\pi} f(y) Q_k(x - y) \dif{y} \overset{k}{\to} f(x)
    \]
    e uso il punto precedente per concludere. \textcolor{red}{Non chiaro}.
  \end{enumerate}
\end{proof}

\begin{exercise}
  Si calcolino i coefficienti di Fourier per la funzione
  \[
    f(\theta) \coloneqq
    \begin{cases}
      1  & \text{se } 0 \leq \theta \leq \pi \\
      -1 & \text{se } \pi < \theta \leq 2\pi
    \end{cases}
  \]
\end{exercise}

\section{Identità di Parseval}
\emph{Setting}: $ H $ spazio di Hilbert su $ \K = \R \text{ o } \C $. \\

Vedremo adesso un'ulteriore condizione equivalente alla completezza per un sistema ortonormale in uno spazio di Hilbert.
\begin{proposition}[identità di Parseval]\label{prop:Parseval}
  Sia $ \{e_n\} $ un sistema ortonormale in $ H $. Allora $ \{e_n\} $ è completo se e solo se vale l'identità di Parseval:
  \[ \forall v \in H,\ \norm{v}^2 = \sum_{n = 0}^{+\infty}\abs{\langle v, e_n \rangle}^2. \]
\end{proposition}
\begin{proof}
  Mostriamo che se $ \{e_n\} $ è completo vale l'identità di Parseval, utilizzando la prima caratterizzazione di completezza fornita dalla Proposizione \ref{prop:completezza}. Abbiamo che $\forall v \in H$, posto
  \[ S_N = \sum_{n=0}^{N}\langle v, e_n \rangle e_n, \]
  $ S_n \to v $ in norma $L^2$. Si ottiene quindi che:
  \[ \norm{S_N - v} \to 0 \quad \Rightarrow \quad \norm{S_N}^2 \to \norm{v}^2. \]
  Per Pitagora, però, essendo gli $ \{e_n\} $ ortonormali:
  \[ \norm{S_N}^2 = \sum_{n=0}^{N}\abs{\langle v, e_n \rangle}^2 \]
  da cui la tesi.\\
  Per l'implicazione opposta useremo invece la quarta caratterizzazione di completezza fornita dalla Proposizione \ref{prop:completezza}, e supponiamo quindi che per un qualche $v \in H$, $ \forall n \in \N,\ \langle v, e_n \rangle = 0 $. Parseval diventa:
  \[ \norm{v}^2 = \sum_{n=0}^{+\infty}\abs{\langle v, e_n \rangle}^2 = 0 \]
  da cui, per non degenerazione del prodotto scalare, $ v = 0 $, e quindi $ \{e_n\} $ è completo.
\end{proof}

\begin{exercise}
  Si consideri $ H = L^2([0,2\pi]) $ con la base di Fourier, ed il seguente elemento di $ H $:
  \[
    f(\theta) \coloneqq
    \begin{cases}
      1  & \text{se } 0 \leq \theta \leq \pi \\
      -1 & \text{se } \pi < \theta \leq 2\pi
    \end{cases}
  \]
  e si sfrutti l'identità di Parseval per calcolare $ \sum_{n=0}^{+\infty}\frac{1}{(2n+1)^2} $
\end{exercise}
\begin{solution}
  \textcolor{red}{Mancante}
\end{solution}

\section{Convergenza puntuale della serie di Fourier}
\emph{Setting}: $ H = L^2(\T^1) $ con $ \{e_n\}\ped{n\in\N} $ la base di Fourier.\\

Dimostreremo adesso alcuni criteri che forniscono la convergenza puntuale (o uniforme) della serie di Fourier di una funzione $ f \in H $, i cui coefficienti saranno indicati con $ a_n $ e le cui somme parziali saranno indicate con $ S_N = \sum_{n=0}^{N} a_n e_n $

\begin{proposition}\label{prop:fourier_puntuale_1}
  Sia $ f \in \mathcal{C}^k(\T^1) $\footnote{Ricordiamo che la continuità e differenziabilità sul toro $\T^1$ sono più forti delle rispettive condizioni su $[0,2\pi]$, in quanto richiedono anche la continuità o differenziabilità in $ 0\equiv 2\pi $.} per qualche $ k \geq 0 $. Vale:
  \[ \forall n \in \N,\ \abs{a_n} \leq \frac{c}{n^k} \]
  dove $ c $ dipende da $ f $ ma non da $ n $.
\end{proposition}
\begin{proof}
  Osserviamo innanzitutto che $ f $ appartiene sicuramente ad $ L^2(\T^1) $, in quanto funzione continua su un compatto e quindi limitata. Procediamo adesso per induzione, osservando che il caso $ k = 0 $ è banale, in quanto essendo i coefficienti $ (a_n)_{n\in\N} $ in $\ell^2$, essi sono limitati. Supponiamo adesso vera la tesi per funzioni in $ \mathcal{C}^k $ e consideriamo $ f \in \mathcal{C}^{k+1} $. Integrando per parti:
  \begin{align*}
    a_n &= \frac{1}{\sqrt{2\pi}} \int_{0}^{2\pi} f(x) e^{-inx}\dif{x} = \left[ \frac{1}{-in\sqrt{2\pi}} f(x) e^{-inx} \right]_0^{2\pi} + \frac{1}{in\sqrt{2\pi}} \int_0^{2\pi} f'(x)e^{-inx}\dif{x}\\
        &= \frac{1}{in\sqrt{2\pi}} \int_0^{2\pi} f'(x)e^{-inx}\dif{x} = \frac{1}{in}b_n
  \end{align*}
  dove $ b_n $ sono i coefficienti di Fourier della derivata di $ f $, che è una funzione di $ \mathcal{C}^k $; per ipotesi induttiva vale quindi:
  \[ \abs{a_n} = \frac{\abs{b_n}}{n} \leq \frac{c}{n^k\cdot n} = \frac{c}{n^{k+1}} \]
  che dimostra il passo induttivo.
\end{proof}

\begin{corollary}
  Se $ f \in \mathcal{C}^2(\T^1) $ la serie di Fourier converge uniformemente:
  \[ \frac{1}{\sqrt{2\pi}}\sum_{n=-N}^{N}\langle f, e_n \rangle e^{inx}\  \touf \ f(x). \]
\end{corollary}
\begin{proof}
  Per $ k = 2 $ la Proposizione \ref{prop:fourier_puntuale_1} fornisce:
  \[ \abs{a_n} \leq \frac{c}{n^2}  \]
  ma, essendo $ \norm{e_n} = 1\ \forall n $ ed essendo la serie $ \sum_{n=1}^{+\infty}\frac{1}{n^2} $ assolutamente convergente, si ha convergenza totale della serie di Fourier e quindi convergenza uniforme.
\end{proof}

Diamo adesso un risultato più forte:
\begin{proposition}\label{prop:fourier_puntuale_2}
  Se $ f \in \mathcal{C}(\T^1) $ è $ \mathcal{C}^1 $ a tratti con derivata limitata, si ha convergenza uniforme della serie di Fourier:
  \[ \frac{1}{\sqrt{2\pi}}\sum_{n=0}^{N}\langle f, e_n \rangle e^{inx}\  \touf \ f(x). \]
\end{proposition}
\begin{proof}
  Analogamente a quanto fatto nella dimostrazione della Proposizione \ref{prop:fourier_puntuale_1} integrando per parti la definizione degli $ a_n $ si ottiene che:
  \[ a_n = -\frac{1}{in}b_n \]
  dove i $ b_n $ sono i coefficienti di Fourier della derivata, che sono ben definiti in quanto essa è limitata e quindi $ L^2 $. Da Parseval si ottiene:
  \[ \sum_{n\in\Z} \abs{b_n}^2 = \norm{f'} < +\infty \]
  da cui:
  \[ \sum_{n\in\Z} n^2\abs{a_n}^2 = \sum_{n\in\Z} \abs{b_n}^2 < +\infty. \]
  Per un noto lemma di analisi\footnote{Utilizzando la diseguaglianza di H\"older si ha: \[ \sum_{n=0}^{N} \abs{c_n} = \sum_{n=0}^{N} \frac{1}{n} \cdot n \abs{c_n} \leq \left(\sum_{n=0}^{N}\frac{1}{n^2}\right)^{1/2} \left(\sum_{n=0}^{N}n^2\abs{c_n}^2\right)^{1/2} \] e passando al limite in $ N $ si conclude.} ciò implica:
  \[ \sum_{n\in\Z} \abs{a_n} < +\infty \]
  e quindi la serie di Fourier converge totalmente e quindi uniformemente.
\end{proof}

Introduciamo un lemma che sarà utile nella dimostrazione del prossimo criterio:
\begin{lemma}[Riemann - Lebesgue]
  Sia $ f \in L^1(\R) $, $ \zeta \in \R $. Allora:
  \[ \lim\limits_{\abs{\zeta} \to +\infty} \int_\R f(x)e^{i\zeta x}\dif{x} = 0. \]
\end{lemma}
\begin{proof}
  Sia intanto $ f = \chi_{[a,b]} $. In tal caso:
  \[ \int_{\R} \chi_{[a,b]}(x)e^{i\zeta x} \dif{x} = \frac{1}{i\zeta}\left(e^{i\zeta b} - e^{i\zeta a}\right) \to 0. \]
  Procedendo per linearità e per densità delle caratteristiche in $ L^1 $ si conclude.\\ \textcolor{red}{Esplicitare meglio la conclusione.}
\end{proof}

Dimostriamo adesso un risultato che fornisce la convergenza puntuale della serie di Fourier in un singolo punto:

\begin{proposition}\label{prop:fourier_puntuale_3}
  Sia $ f\colon \T^1 \to \T^1 $ limitata e differenziabile in $ x_0 $. Allora la serie di Fourier converge puntualmente in $ x_0 $:
  \[ \lim\limits_{N \to +\infty} \frac{1}{\sqrt{2\pi}} \sum_{n=-N}^{N} \langle f, e_n \rangle e^{inx_0} = f(x_0). \]
\end{proposition}
\begin{proof}
  Sia:
  \[ S_N(x) = \frac{1}{\sqrt{2\pi}} \sum_{n=-N}^{N} \langle f, e_n \rangle e^{inx}. \]
  Allora:
  \begin{align*}
    S_N(x_0) &= \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(y) e^{-iny} e^{inx_0} \dif{y} = \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(y) e^{-in(y-x_0)} \dif{y}\\
             &= \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{-x_0}^{2\pi-x_0} f(z + x_0) e^{-inz} \dif{z} = \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(z + x_0) e^{-inz} \dif{z}\\
             &= \frac{1}{2\pi} \int_{0}^{2\pi} f(z + x_0) \left(\sum_{n=-N}^{N} e^{-inz} \right) \dif{z}
  \end{align*}
  dove si è operato il cambio di variabile $ z = y-x_0 $ e nella penultima uguaglianza si è usato il fatto che sia $ f $ che $ e^{inz} $ sono $ 2\pi $-periodiche. Riconosciamo adesso il nucleo di Dirichlet\footnote{o, più semplicemente, la somma parziale di una serie geometrica.}:
  \[ D_N(z) \coloneqq \sum_{n=-N}^{N} e^{-inz} = \frac{\sin\left[(2N+1)\frac{z}{2}\right]}{\sin\left(\frac{z}{2}\right)} \]
  e, osservando che $ \int_{0}^{2\pi} D_N(z) \dif{z} = 1 $ otteniamo:
  \begin{align*}
    S_N(x_0) - f(x_0) &=  \frac{1}{2\pi} \int_{0}^{2\pi} f(z + x_0) \left(\sum_{n=-N}^{N} e^{-inz} \right) \dif{z} - f(x_0)\\
                      &= \int_{0}^{2\pi} D_N(z) f(z+x_0) \dif{z} - \int_{0}^{2\pi} D_N(z) f(x_0) \dif{z} \\
                      &= \frac{1}{2\pi} \int_{0}^{2\pi} \frac{f(z + x_0) - f(x_0)}{2\sin\left(\frac{z}{2}\right)}\cdot 2\sin\left[(2N+1)\frac{z}{2}\right] \dif{z}.
  \end{align*}
  Osserviamo che il primo fattore è $ L^1 $ in quanto limitato, poiché per $ z\to 0 $ si ha $ 2\sin(\frac{z}{2}) \sim z $ e quindi esso tende a $ f'(x_0) $, mentre il secondo fattore si può scrivere come $ e^{i\frac{2N+1}{2}z} - e^{-i\frac{2N+1}{2}z} $; si può quindi applicare il lemma di Riemman - Lebesgue, ottenendo:
  \[ \lim\limits_{N\to +\infty}\left[S_N(x_0) - f(x_0)\right] = 0. \qedhere\]
\end{proof}

\begin{exercise}
  Si consideri $ f = \frac{x}{2} $. Si sfruttino l'identità di Parseval e la Proposizione \ref{prop:fourier_puntuale_3} con $ x_0 = \frac{\pi}{2} $ per calcolare $ \sum_{n=1}^{+\infty}\frac{1}{n^2} $ e $ \sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1} $.
\end{exercise}
\begin{solution}
  \textcolor{red}{Mancante}
\end{solution}
