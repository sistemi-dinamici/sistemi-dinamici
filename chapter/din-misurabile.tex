\section{Introduzione}

\begin{definition}[sistema dinamico misurabile]
  Un sistema dinamico misurabile è una quaterna $ (X, \mathcal{A}, \mu, f) $ dove
  \begin{enumerate}[label=(\roman*)]
  \item $ (X, \mathcal{A}, \mu) $ è uno spazio di probabilità, cioè un insieme $ X $ (spazio delle fasi) con una $ \sigma $-algebra $ \mathcal{A} $ e una misura $ \mu $ tale che $ \mu(X) = 1 $;
  \item $ f \colon X \to X $ è una funzione misurabile e tale che $ \mu $ sia $ f $-invariante, cioè tale che $ \forall A \in \mathcal{A}, \ f_{\sharp}\mu (A) = \mu(f^{-1}(A)) = \mu(A) $;
  \item la dinamica è data dall'iterazione di $ f $.
  \end{enumerate}
\end{definition}

\begin{definition}[omomorfismo di sistemi dinamici misurabili]
  Un sistema dinamico misurabile $ (Y, \mathcal{B}, \nu, g) $ si dice omomorfo a $ (X, \mathcal{A}, \mu, f) $ se esiste una mappa $ h \colon X \to Y $ tale che
  \begin{enumerate}[label=(\roman*)]
  \item $ h $ sia misurabile;
  \item $ \nu $ sia la misura immagine si $ \mu $ secondo $ h $, cioè $ \forall B \in \mathcal{B}, \ h_\sharp \mu(B) = \mu(h^{-1}(B)) = \nu(B) $;
  \item faccia quasi commutare il diagramma \eqref{cd:coniugazione}, cioè sia tale che $ (h \circ f)(x) = (g \circ h)(x) $ per $ \mu $-q.o. $ x \in X $.
  \end{enumerate}
  In tale caso $ (Y, \mathcal{B}, \nu, g) $ è un \emph{fattore} di $ (X, \mathcal{A}, \mu, f) $.
\end{definition}

\begin{definition}[isomorfismo di sistemi dinamici misurabili]
  Due sistemi dinamici misurabili $ (X, \mathcal{A}, \mu, f) $ e $ (Y, \mathcal{B}, \nu, g) $ si dicono isomorfi se esistono una funzione $ h \colon X \to Y $ e una funzione $ k \colon Y \to X $ che siano una l'inversa dell'altra a meno di un insieme di misura nulla e facciano commutare il seguente diagramma:
  \begin{center}
    \begin{tikzcd}
      X \arrow[r, "f"] \arrow[d, shift right, "h" left] & X \arrow[d, shift left, "h" right]\\
      Y \arrow[r, "g" below] \arrow[u, shift right, "k" right] & Y \arrow[u, shift left, "k" left]
    \end{tikzcd}
  \end{center}
  Più precisamente chiediamo che $ (Y, \mathcal{B}, \nu, g) $ sia omeomorfo a $ (X, \mathcal{A}, \mu, f) $ tramite $ h $, che $ (X, \mathcal{A}, \mu, f) $ sia omeomorfo a $ (Y, \mathcal{B}, \nu, g) $ tramite $ k $ e che
  \begin{enumerate}[label=(\roman*)]
  \item per $ \mu $-q.o. $ x \in X $ si abbia $ k(h(x)) = x $;
  \item per $ \nu $-q.o. $ y \in Y $ si abbia $ h(k(y)) = y $.
  \end{enumerate}
\end{definition}

\begin{oss}
  Le nozioni di dinamica misurabile e teoria ergodica che andremo ad enunciare sono invarianti per isomorfismo di sistemi dinamici.
\end{oss}

\section{Misure invarianti}
L'enunciato contenuto nell'esercizio seguente, è molto utile in quanto ci permette di verificare l'invarianza di una misura data o trovare una misura invariante per mappe dall'intervallo $ [0, 1] $ in sé.

\begin{exercise}[operatore di Perron-Frobenius] \label{ex:invarianza-PF}
  Sia $ f \colon [0, 1] \to [0, 1] $ monotona e $ C^1 $ a tratti (anche numerabili). Su $ [0, 1] $ è posta una misura con densità $ \rho(x) $, cioè tale che $ \mu(A) \coloneqq \int_A \rho(x) \dif{x} $ dove $ \dif{x} $ è la misura di Lebesgue. Mostrare che $ \mu $ è $ f $-invariante se e solo se
  \begin{equation} \label{eqn:invarianza-PF}
    \sum_{x \in f^{-1}(\{y\})} \frac{\rho(x)}{\abs{f'(x)}} = \rho(y).
  \end{equation}
\end{exercise}
\begin{solution}
  Per semplicità, supponiamo inizialmente che $ f $ sia crescente e $ C^1 $ su tutto $ [0, 1] $. Fissato $ y \in f([0, 1]) $, consideriamo l'intervallo $ [0, y] $. Detto $ c = \inf f $, si ha $ f^{-1}([0, y]) = [f^{-1}(c), f^{-1}(y)] $ essendo $ f $ crescente. Allora, se $ \mu $ è $ f $-invariante, abbiamo
  \[
    \mu([0, y]) = \int_0^y \rho(t) \dif{t} = \mu(f^{-1}([0, y])) = \mu([f^{-1}(c), f^{-1}(y)]) = \int_{f^{-1}(c)}^{f^{-1}(y)} \rho(t) \dif{t}.
  \]
  Facendo il cambio di variabile $ t = f^{-1}(s) $ da cui $ \dif{t} = \dif{s}/f'(f^{-1}(s)) $ (che è ben definito essendo $ f $ monotona) si ottiene
  \[
    \int_0^y \rho(t) \dif{t} = \int_{c}^{y} \rho(f^{-1}(s)) \frac{\dif{s}}{f'(f^{-1}(s))}
  \]
  da cui differenziando rispetto a $ y $ abbiamo la tesi
  \[
    \rho(y) = \frac{\rho(f^{-1}(y))}{f'(f^{-1}(y))} = \frac{\rho(f^{-1}(y))}{\abs{f'(f^{-1}(y))}}
  \]
  essendo $ f' $ positiva. Se invece $ f $ è decrescente, $ f^{-1}([0, y]) = [f^{-1}(y), f^{-1}(c)] $ da cui, supponendo $ \mu $ invariante e facendo lo stesso cambio di variabile,
  \[
    \int_0^y \rho(t) \dif{t} =  \int_{f^{-1}(y)}^{f^{-1}(c)} \rho(t) \dif{t} = \int_{y}^{c} \rho(f^{-1}(s)) \frac{\dif{s}}{f'(f^{-1}(s))}.
  \]
  Differenziando rispetto a $ y $ si ottiene
  \[
    \rho(y) = - \frac{\rho(f^{-1}(y))}{f'(f^{-1}(y))} = \frac{\rho(f^{-1}(y))}{\abs{f'(f^{-1}(y))}}
  \]
  essendo $ f' $ negativa. \\
  Veniamo ora al caso generale di $ f $ monotona e $ C^1 $ a tratti. Dividiamo il dominio in sotto-intervalli $ [0, 1] = \bigcup_{i \in I} [a_i, a_{i+1}] $ con parti interne disgiunte e tali che $ f_i \coloneqq f \lvert_{[a_i, a_{i+1}]} $ è monotona e $ C^1 $. Come nel caso più semplice sia $ y \in f([0, 1]) $, consideriamo l'intervallo $ [0, y] $ e sia $ c_i \coloneqq \inf f_i $. Sia
  \[ I(y) \coloneqq \{i \in I : y \in f_i([a_i, a_{i+1}])\} \]
  l'insieme degli indici dei sotto-intervalli a cui appartengono gli $ x \in f^{-1}(\{y\}) $. Allora $ f^{-1}([0, y]) = \bigcup_{i \in I(y)} f_i^{-1}([0, y]) $ dove per ogni $ i \in I(y) $ si ha $ f_i^{-1}([0, y]) = [f_i^{-1}(c_i), f_i^{-1}(y)] $ se $ f_i $ è crescente ovvero $ f_i^{-1}([0, y]) = [f_i^{-1}(y), f_i^{-1}(c_i)] $ se $ f_i $ è decrescente (se ci restringiamo agli intervalli in cui $ f $ è monotona, le controimmagini di $ y $ tramite le $ f_i $ sono uniche). Pertanto se $ \mu $ è $ f $-invariante abbiamo che
  \[
    \mu([0, y]) = \int_0^y \rho(t) \dif{t} = \mu(f^{-1}([0, y])) = \sum_{i \in I(y)} \mu(f_i^{-1}([0, y])).
  \]
  Ora, come fatto in precedenza per le funzioni monotone su tutto il loro dominio, facendo il solito cambio di variabile si ottiene
  \[
    \int_0^y \rho(t) \dif{t} = \sum_{i \in I(y)} \int_{c_i}^{y} \rho(f_i^{-1}(s)) \frac{\dif{s}}{\abs{f_i'(f_i^{-1}(s))}}
  \]
  da cui differenziando rispetto a $ y $ abbiamo la tesi
  \[
    \rho(y) = \sum_{i \in I(y)}  \frac{\rho(f_i^{-1}(y))}{\abs{f_i'(f_i^{-1}(y))}} = \sum_{x \in f^{-1}(\{y\})} \frac{\rho(x)}{\abs{f'(x)}}.
  \]
  Per dimostrare che se vale la formula \eqref{eqn:invarianza-PF}, allora $ \mu $ è $ f $-invariante basta integrare la formula e fare il cambio di variabile inverso.
\end{solution}

\begin{example}
  I sistemi dinamici nell'esempio \ref{ex:Ulam_tenda} sono isomorfi tramite la mappa $ h $ ivi definita.
\end{example}

\begin{exercise}[mappa di Gauss]
  Sia $ G\colon [0,1]\to [0,1] $ definita come $ G(x) \coloneqq \left\{ \frac{1}{x} \right\} $. Verificare che $ G $ conserva la misura con densità:
  \[ \dif{\mu}(x) = \frac{\dif x}{(\log 2)(1+x)} \, . \]
\end{exercise}

\begin{exercise}[mappa di Farey]
  Sia $ F\colon (0,1)\to (0,1) $ la mappa
  \[
    F(x) \coloneqq
    \begin{cases}
      \frac{x}{1-x}   & \text{se } 0 < x \leq \frac{1}{2} \\
      \frac{1}{x} - 1 & \text{se } \frac{1}{2} < x < 1
    \end{cases}
  \]
  Essa conserva la misura infinita $ \dif{\mu}(x) = \frac{\dif{x}}{x} $.
\end{exercise}

\begin{exercise}
  Sia $ f\colon \R\to\R $ definita come $ f(x) \coloneqq \frac{1}{2} \left( x - \frac{1}{x} \right) $. Questa conserva la misura $ \dif{\mu}(x) = \frac{\dif{x}}{\pi(1+x^2)} $.
\end{exercise}

Il seguente teorema definisce un collegamento tra un sistema dinamico topologico e un sistema dinamico misurabile e stabilisce che il sistema dinamico dato dall'iterazione di $ f $ su uno spazio metrico compatto ammette anche una descrizione in termini di sistema dinamico misurabile.

\begin{thm}[Krylov-Bogolyubov]
  Sia $ (X, d, f) $ un sistema dinamico topologico. Allora esiste almeno una misura $ \mu $ di probabilità sui boreliani di $ X $ che sia $ f $-invariante.
\end{thm}