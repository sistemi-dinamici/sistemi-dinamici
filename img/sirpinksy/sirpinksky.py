import numpy as np
import matplotlib.pyplot as plt
import sys


def w1(r):
    return r/2


def w2(r):
    return r/2 + [1/4, np.sqrt(3)/4]


def w3(r):
    return r/2 + [1/2, 0]


N = 50
S = np.array([[x, y] for y in np.linspace(0.25, 0.75, N)
              for x in np.linspace(0.25, 0.75, N)])

for i in range(int(sys.argv[1])):
    s1 = w1(S)[::2]
    s2 = w2(S)[::2]
    s3 = w3(S)[::2]
    S = np.vstack((s1, s2, s3))

x = np.array([S[i][0] for i in range(len(S[:, 0]))])
y = np.array([S[i][1] for i in range(len(S[:, 1]))])

plt.plot(x, y, linestyle='', marker='.')
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.show()
