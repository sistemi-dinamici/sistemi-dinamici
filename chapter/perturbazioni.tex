\section{Sistemi integrabili e teoria delle perturbazioni}
\emph{Setting}: ci concentriamo esclusivamente su sistemi hamiltoniani indipendenti dal tempo.

\subsection{Variabili angolo-azione e sistemi integrabili}
\begin{definition}[sistema completamente canonicamente integrabile]
  Si dice che un sistema hamiltoniano descritto con un'hamiltoniana $ \ham(\vec{q}, \vec{p}) $ è completamente canonicamente integrabile (c.c.i.) se esistono un aperto $ U \subseteq \R^n $ e una trasformazione canonica dalle variabili $ (\vec{q}, \vec{p}) $ alle variabili $ (\boldsymbol{\chi}, \vec{J}) \in \T^{n} \times U $, dette rispettivamente variabili \emph{angolo} e variabili \emph{azione}, tale che la nuova hamiltoniana risulti indipendente dalle variabili angolo $ \mathcal{K} = \mathcal{K}_0(\vec{J}) $.
\end{definition}

Se un sistema risulta c.c.i. allora le equazioni del moto assumono una forma molto semplice: infatti dalle equazioni di Hamilton abbiamo che $ \dot{J}_i = 0 \Rightarrow J_i(t) = J_i(0) $, da cui
\[
  \dot{\chi}_i(t) = \dpd{\mathcal{K}_0}{J_i}(\vec{J}(t)) = \dpd{\mathcal{K}_0}{J_i}(\vec{J}(0)) \eqqcolon \omega_i \quad \Rightarrow \quad \chi_i(t) = \omega_i t + \chi_i(0)
\]
dove l'ultima equazione è intesa a valori nel toro. Abbiamo quindi ottenuto che le variabili azione si ``congelano'' mentre le variabili angolo $ \chi_i $ eseguono un moto periodico sul toro di frequenza $ \omega_i $. Chiaramente, però, il moto globale sarà in generale non periodico in quanto gli angoli hanno un moto di ``frequenza'' diversa e quindi la periodicità dipenderà dalle relazioni di commensurabilità tra le $ \omega_i $. Più precisamente vale la seguente
\begin{proposition}
  Posto $ \dot{\boldsymbol{\chi}} = \boldsymbol{\omega} $ e definito $ M \coloneqq \{\vec{k}\in\Z^{n} : \vec{k}\cdot\boldsymbol{\omega} = k_1\omega_1 +\cdots+ k_n\omega_n = 0\} $ il \emph{modulo di risonanza} si hanno due casi
  \begin{enumerate}[label=(\roman*)]
  \item $ \dim M = 0 $ $ \iff $ il flusso hamiltoniano è denso su $ \T^{n} $ ed è unicamente ergodico ($ \boldsymbol{\omega} $ si dice \emph{non risonante});
  \item $ 1 \leq \dim M \leq n $ $ \Rightarrow $ il flusso hamiltoniano è denso su $ \T^{n-d} $ con $ d \coloneqq \dim M $.
  \end{enumerate}
\end{proposition}

Una condizione sufficiente per la completa integrabilità del moto è data dal seguente
\begin{thm}[Arnold-Louville]
  Sia $ \ham \colon \R^{2n} \to \R $ un'hamiltoniana indipendente dal tempo. Supponiamo che
  \begin{enumerate}[label=(\roman{*})]
  \item $ \exists F_1, \ldots, F_n \colon \R^{2n} \to \R $ di classe $ \mathcal{C^{\infty}} $ tali che
    \begin{enumerate}[label=\arabic{*}.]
    \item siano integrali primi del moto;
    \item siano in \emph{involuzione}, cioè tali che $ \{F_i, F_j\} = 0 $ per ogni $ i, j \in \{1, \ldots, n\} $;
    \item abbiano i gradienti linearmente indipendenti, cioè \[ \dim{\mathrm{Span}\{\nabla F_1(\vec{q}, \vec{p}), \ldots, \nabla F_n(\vec{q}, \vec{p})\}} = n \qquad \forall (\vec{q}, \vec{p}) \in\R^{2n}; \]
    \end{enumerate}
  \item $ \exists a \in \R^{n} $ tale che $ M(\vec{a}) = \{(\vec{q}, \vec{p})\in\R^{2n} : F_i(\vec{q}, \vec{p}) = a_i, \; \forall i = 1, \ldots, n\} \neq \emptyset $ sia connesso e compatto.
  \end{enumerate}
  Allora il sistema hamiltoniano è completamente canonicamente integrabile in un intorno di $ M(\vec{a}) $ e $ M(\vec{a}) $ risulta diffeomorfo a $ \T^n $.
\end{thm}

\subsection{Introduzione alla teoria delle perturbazioni classica}
\textcolor{red}{\emph{Disclaimer}: in questa sezione non ci siamo preoccupati di dare delle ipotesi precise per quanto fatto, sarebbe bello riuscire a trasformare questi conti in dei teoremi con delle dimostrazioni.}

\begin{definition}[sistema quasi integrabile]
  Un sistema hamiltoniano si dice quasi integrabile se l'hamiltoniana è della forma \[ h_ {\epsilon}(\vec{q}, \vec{p}) = k_0(\vec{q}, \vec{p}) + \epsilon f(\vec{q}, \vec{p}) \] dove $ k_0 $ è l'hamiltoniana di un sistema c.c.i e $ \epsilon \in \R $. Il termine $ f $ è detto perturbazione \textcolor{red}{ed è una funzione definita su un aperto di $ \R^{2n} $ su cui è limitata}.
\end{definition}

Essendo il sistema descritto da $ k_{0} $ c.c.i, è naturale voler passare nelle coordinate angolo-azione $ (\boldsymbol{\chi}, \vec{J}) $ per $ h_0 $. Così l'hamiltoniana diventa
\[ \ham_{\epsilon}(\boldsymbol{\chi}, \vec{J}) = \mathcal{K}_0(\vec{J}) + \epsilon F(\boldsymbol{\chi}, \vec{J}). \]
Essendo il passaggio in coordinate angolo-azione una trasformazione canonica, valgono ancora le equazioni di Hamilton
\begin{equation}
  \begin{cases}
    \dot{\chi}_i = \omega_i(\vec{J}) + \epsilon \dpd{F}{J_i}(\boldsymbol{\chi}, \vec{J}) \\
    \dot{J}_i = -\epsilon \dpd{F}{\chi_i}(\boldsymbol{\chi}, \vec{J})
  \end{cases}
\end{equation}
dove abbiamo posto $ \omega_i(\vec{J}) \coloneqq \dpd{\mathcal{K}_0}{J_i}(\vec{J}) $. \\

Ora l'idea è quella di riuscire a mostrare in quali casi possiamo concludere che anche il sistema perturbato risulta completamente canonicamente integrabile: l'obiettivo è quello di esprimere la trasformazione come una serie di potenze di $ \epsilon $ che, avendo in mente di avere un a perturbazione ``piccola'', sarà tanto più vicina all'identità (che rende c.c.i la $ k_0 $) tanto più $ \epsilon $ è ``piccolo''.

\subsubsection{Caso $ n = 1 $}
In uno spazio delle fasi $ 2 $--dimensionale possiamo ottenere la completa integrabilità del moto richiedendo che $ \omega(J) $ sia non nullo e limitato. Vogliamo mostrare che in tale caso una possibile funzione generatrice di tipo $ F_2 $ è della forma
\begin{equation}
  W_\epsilon(\chi, J') = \chi J' + \sum_{n=1}^{+\infty} \epsilon^{n} W_n(\chi, J').
\end{equation}
Questa è ottenuta aggiungendo all'identità (si veda l'esempio~\ref{ex:identita}) una termine perturbativo sotto forma di serie di potenze di $ \varepsilon $. La nuova hamiltoniana assume invece la forma
\begin{equation}\label{eq:perturbazioni-claim-ham}
  \ham_{\epsilon}'(J') = \mathcal{K}_0(J') + \sum_{n=1}^{+\infty} \epsilon^{n} \mathcal{K}_n(J').
\end{equation}
\textcolor{red}{Chiaramente servono delle ipotesi in più per assicurare la buona convergenza di queste serie.} Di seguito procederemo nel modo tipico della teoria delle perturbazioni che consiste nel determinare i $ W_n $ e i $ \mathcal{K}_n $ richiedendo che queste soddisfino le equazioni esattamente all'ordine $ n $ nel parametro dello sviluppo $ \epsilon $. \\

Essendo la $ W_\epsilon $ una funzione generatrice di tipo $ F_2 $ abbiamo che
\begin{equation}\label{eq:perturbazioni-cambio-variabili}
  \begin{cases}
    J = \dpd{W_\epsilon}{\chi} = J' + \displaystyle\sum_{n=1}^{+\infty} \epsilon^{n} \dpd{W_n}{\chi}(\chi, J') \\
    \chi' = \dpd{W_\epsilon}{J} = \chi + \displaystyle\sum_{n=1}^{+\infty} \epsilon^{n} \dpd{W_n}{J}(\chi, J')
  \end{cases}
\end{equation}
Dato che l'hamiltoniana indipendente dal tempo, l'hamiltoniana nelle variabili primate sarà l'hamiltoniana vecchia, espressa nelle nuove variabili $ \ham_{\epsilon}'(\chi', J') = \ham_{\epsilon}(\chi(\chi', J'), J(\chi', J')) $, usando le relazioni fornite dalla~\eqref{eq:perturbazioni-cambio-variabili}.
\begin{enumerate}
\item All'\emph{oridne zero} la~\eqref{eq:perturbazioni-claim-ham} è già l'hamiltoniana nelle nuove variabili essendo la trasformazione l'identità e a tale ordine $ \ham_{\epsilon}'(J') = \mathcal{K}_0(J') = \mathcal{K}_0(J) = \ham_{\epsilon}(J) $.
\item Al \emph{primo ordine} la trasformazione è
  \[
    \begin{cases}
      J = J' + \epsilon \dpd{W_1}{\chi}(\chi, J') \\
      \chi = \chi' - \epsilon \dpd{W_1}{J'}(\chi, J')
    \end{cases}
  \]
  dove come argomento delle derivate parziali possiamo utilizzare indipendentemente le variabili primate o quelle non primate essendo le correzioni di ordine superiore al primo; usando tali relazioni abbiamo al primo ordine
  \[
    \mathcal{K}_0(J') + \epsilon\mathcal{K}_1(J') = \ham_{\epsilon}\left(J' + \epsilon \dpd{W_1}{\chi}(\chi, J')\right) = \mathcal{K}_0\left(J' + \epsilon \dpd{W_1}{\chi}(\chi, J')\right) + \epsilon F(\chi, J')
  \]
  dove anche qui abbiamo considerato la trasformazione come l'identità nella composizione con $ F $, essendo le correzioni di ordine superiore al primo in $ \epsilon $. Ponendo $ \omega(J) \coloneqq \pd{\mathcal{K}_0}{J}(J) $ e sviluppando in serie di Taylor al primo ordine $ \mathcal{K}_0 $ si ha
  \[
    \mathcal{K}_0(J') + \epsilon\mathcal{K}_1(J') = \mathcal{K}_0(J') + \omega(J') \cdot \epsilon \dpd{W_1}{\chi}(\chi, J') + \epsilon F(\chi, J')
  \]
  da cui si ottiene
  \begin{equation}\label{eq:perturbazioni-fondamentale}
    \mathcal{K}_1(J') = \omega(J') \dpd{W_1}{\chi}(\chi, J') + F(\chi, J')
  \end{equation}
  che viene detta \emph{equazione fondamentale della teoria delle perturbazioni}. In questa equazione le incognite sono la $ W_1 $ e la $ \mathcal{K}_1 $, mentre $ F $ è nota; per risolvere l'equazione con un metodo generale ed essendo la $ \chi $ definita sul toro $ \T^{1} $ è conveniente scomporre in serie di Fourier $ W_1 $ e $ F $:
  \[
    W_1(\chi, J') = \sum_{k \in \Z} \hat{W}_1(k, J') e^{2\pi i k \chi}, \qquad F(\chi, J') = \sum_{k \in \Z} \hat{F}(k, J') e^{2\pi i k \chi}.
  \]
  Sostituendo nell'equazione~\eqref{eq:perturbazioni-fondamentale} si ha
  \begin{align*}
    \mathcal{K}_1(J') & = \sum_{k \in \Z} \left[2\pi i k \omega(J') \hat{W}_1(k, J') + \hat{F}(k, J')\right]e^{2\pi i k \chi} \\
                      & = \hat{F}(0, J') + \sum_{k \in \Z\setminus\{0\}} \left[2\pi i k \omega(J') \hat{W}_1(k, J') + \hat{F}(k, J')\right]e^{2\pi i k \chi}.
  \end{align*}
  Eguagliando termine a termine la serie di Fourier otteniamo per $ k=0 $
  \begin{equation}
    \mathcal{K}_1(J') = \hat{F}(0, J') = \int_{0}^{1} F(\chi, J') \dif{\chi}
  \end{equation}
  mentre per $ k>0 $, essendo $ \omega(J')\neq 0 $, abbiamo
  \begin{equation}
    \hat{W}_1(k, J') = - \frac{\hat{F}(k, J')}{2\pi i k \omega(J')}
  \end{equation}
  che determinano il primo ordine dello sviluppo della $ W_\epsilon $ e della $ \mathcal{\ham}_{\epsilon}' $ nota l'hamiltoniana di partenza.
\item Al \emph{secondo ordine} la trasformazione è
  \[
    \begin{dcases}
      J = J' + \epsilon \dpd{W_1}{\chi}(\chi, J') + \epsilon^2 \dpd{W_2}{\chi}(\chi, J') \\
      \chi = \chi' - \epsilon \dpd{W_1}{J'}(\chi, J') - \epsilon^2 \dpd{W_2}{J'}(\chi, J')
    \end{dcases}
  \]
  \textcolor{red}{Un ottimo esercizio, ma sarebbe bello scriverla per bene. Di fatto usando che la $ W_1 $ soddisfa l'equazione al primo ordine si arriva ad un'equazione simile alla~\eqref{eq:perturbazioni-fondamentale} per la $ W_2 $ con come ``forzante'' la $ W_1 $ che ora è nota.}
\end{enumerate}

\subsubsection{Caso $ n \geq 2 $}

Per uno spazio delle fasi a più di 2 dimensioni la questione diventa molto più complessa e servono delle ipotesi in più:
\begin{itemize}
\item \emph{ipotesi di non degenerazione}: $ \det\left(\dmd{\mathcal{K}_0}{2}{J_i}{}{J_j}{}\right) \neq 0 $ da cui si ottiene che $ \boldsymbol{\omega}(\vec{J}) \coloneqq \nabla\mathcal{K}_0(\vec{J}) $ è un diffeomorfismo locale \textcolor{red}{(tra cosa e cosa?)};
\item \emph{ipotesi di ``genericità'' della perturbazione}: se $ F(\boldsymbol{\chi}, \vec{J'}) = \sum_{\vec{k} \in \Z^{n}} \hat{F}(\vec{k}, \vec{J'}) e^{2\pi i \vec{k} \cdot \boldsymbol{\chi}} $ richiediamo che $ \forall\vec{J}\in\R^{n} $ e che per ogni $ \vec{k}\in\Z $ o si abbia $ \hat{F}(\vec{k}, \vec{J'})\neq0 $, oppure che esista un $ \vec{k'}\neq0 $ parallelo a $ \vec{k} $ tale che $ \hat{F}(\vec{k}, \vec{J'})\neq0 $.
\end{itemize}
Sotto queste due ipotesi vale il seguente
\begin{thm}[Poincaré]
  Non esistono soluzioni regolari all'equazione fondamentale della teoria delle perturbazioni
  \begin{equation}
    \mathcal{K}_1(\vec{J}) = \boldsymbol{\omega}(\vec{J'}) \cdot \nabla_{\boldsymbol{\chi}} W_1(\boldsymbol{\chi}, \vec{J'}) + F(\boldsymbol{\chi}, \vec{J'}).
  \end{equation}
\end{thm}

\begin{thm}[di non esistenza di Poincaré]
  Se $ \ham_{\epsilon}(\boldsymbol{\chi}, \vec{J}) $ è analitica, non degenere, con perturbazione generica, allora non esistono integrali primi del moto analitici indipendenti dall'hamiltoniana stessa.
\end{thm}

\begin{thm}[Fermi]
  Se $ \ham_{\epsilon}(\boldsymbol{\chi}, \vec{J}) $ è analitica, non degenere, con perturbazione generica, allora il sistema non ha una sotto-varietà invariante per il flusso hamiltoniano di dimensione $ 2n-1 $ che dipende in modo regolare da $ \epsilon $ ad eccezione della linea di livello dell'energia $ \ham_{\epsilon}(\boldsymbol{\chi}, \vec{J}) = E \in \R $.
\end{thm}