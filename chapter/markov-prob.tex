\section{Catene di Markov probabilistiche}
\subsection{Introduzione}
Sia $ I $ un insieme (che nel prosieguo supporremo essere di cardinalità finita, salvo esplicita menzione), che chiameremo \emph{spazio degli stati}.
Consideriamo uno spazio di probabilità ambiente $ (\Omega, \mathcal{F}, \PP) $ e una successione di variabili aleatorie $ {(X_{n})}_{n\in\N} $ a valori nello spazio degli stati
\[ X_{n}\colon \Omega\to I. \]
Possiamo considerare il push-forward della misura su $ \Omega $ nello spazio degli stati, ovvero, per ogni $ i\in I $ definiamo
\[ (X_{\sharp}\PP)(i) \coloneqq \PP\left(\left\{ \omega\in \Omega : X(\omega) = i \right\}\right) \equiv \PP(X=i). \]
Definiamo inoltre un \emph{vettore di probabilità} come un vettore di $ \R^{n} $ a entrate non negative i cui elementi sommino a 1, e una \emph{matrice stocastica} come una matrice $ n\times n $ a entrate non negative le cui righe sommino a 1.
Possiamo allora dare in questo contesto la seguente
\begin{definition}[Catena di Markov]
  Sia $ {(X_{n})}_{n\in\N} $ una successione definita come sopra; siano inoltre $ \lambda $ un vettore di probabilità e $ P $ una matrice stocastica aventi come dimensione la cardinalità di $ I $. Diciamo che $ {(X_{n})}_{n\in\N} $ è Markov $ (\lambda, P) $ se
  \begin{enumerate}[label=(\roman*)]
  \item $ {(X_{0})}_{\sharp}\PP = \lambda $, ovvero più esplicitamente: $ \forall i\in I\quad \lambda_{i} = \PP(X_{0} = i) $;
  \item $ \forall n\in\N, \forall i_{0}, \ldots, i_{n+1} \in I$ vale
    \[ \PP \left( X_{n+1} = i_{n+1} | X_{0} = i_{0}, \ldots, X_{n} = i_{n} \right) = \PP \left( X_{n+1} = i_{n+1} | X_{n} = i_{n} \right) = P_{i_{n}i_{n+1}}. \]
  \end{enumerate}
\end{definition}
\textcolor{red}{Grafo orientato e pesato}

\begin{thm}
  $ {(X_{n})}_{n\in\N} $ è Markov $ (\lambda, P) $ se e solo se $ \forall n\in\N, \forall i_{0}, \ldots, i_{n} \in I $
  \begin{equation}\label{eq:markov-prodotto}
    \PP\left( X_{0} = i_{0}, \ldots, X_{n} = i_{n} \right) = \lambda_{i_{0}} P_{i_{0}i_{1}} \cdots P_{i_{n-1}i_{n}}
  \end{equation}
  \begin{proof}
    \textcolor{red}{Mancante}
  \end{proof}

  \begin{thm}[Proprietà di Markov]
    Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora, se condiziono a $ X_{m} = i $ per un certo $ i\in I $, $ {(X_{m+n})}_{n\in\N} $ è Markov $ (\delta_{i}, P) $\footnote{Ricordiamo che $ \delta_{i}(j) \coloneqq \delta_{ij}\ \forall j\in I $.}  ed è indipendente da $ X_{0}, \ldots, X_{m} $. In simboli
    \begin{multline*}
      \PP\left( X_{0} = i_{0}, \ldots, X_{m} = i_{m}, \ldots, X_{m+n} = i_{m+n} | X_{m} = i \right) = \\
      = \PP\left( X_{0} = i_{0}, \ldots, X_{m-1} = i_{m-1} | X_{m} = i \right) \PP\left(X_{m} = i_{m}, \ldots, X_{m+n} = i_{m+n} | X_{m} = i \right)
    \end{multline*}
  \end{thm}

  \begin{thm}\label{thm:markov-prob}
    Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora $ \forall m,n\in\N $
    \begin{enumerate}[label=(\roman*)]
    \item\label{pt:markov-prob-gen} $ \PP\left(X_{n} = j\right) = {(\lambda P^{n})}_{j} $;
    \item $ \PP\left(X_{n} = j | X_{0} = i \right) = \PP\left( X_{m+n} = j | X_{m} = i \right) = {(P^{n})}_{ij} $.
    \end{enumerate}
  \end{thm}
  \begin{proof}
    \begin{enumerate}[label=(\roman*)]
    \item
      Usando la~\eqref{eq:markov-prodotto} abbiamo
      \begin{align*}
        \PP\left(X_{n} = j\right) & = \PP\left( \bigsqcup_{i_{0} \cdots i_{n-1}} \left\{ X_{0} = i_{0}, \ldots, X_{n-1} = i_{n-1}, X_{n} = j \right\} \right) \\
                                  & = \sum_{i_{0} \cdots i_{n-1}} \lambda_{i_{0}} P_{i_{0}i_{1}} \cdots P_{i_{n-1}j} = \sum_{i_{0}} \lambda_{i_{0}} {(P^{n})}_{i_{0}j} = {\left(\lambda P^{n}\right)}_{j};
      \end{align*}
    \item \textcolor{red}{Segue dalla proprietà di Markov.}\qedhere
    \end{enumerate}
  \end{proof}
\end{thm}

\subsection{Accessibilità degli stati}
\begin{definition}\
  \begin{itemize}
  \item Diciamo che \emph{esiste un cammino} da $ i $ a $ j $ ($ i\to j $), se $ \exists n\in\N$ tale che $ {(P^{n})}_{ij}>0 $;
  \item Diciamo che due stati $ i $ e $ j $ sono \emph{comunicanti} ($ i \leftrightarrow j $) se esistono $ m,n\in\N $ tali che $ {(P^{m})}_{ij} > 0 $ e $ {(P^{n})}_{ji} > 0 $. Notiamo che la condizione di essere comunicanti definisce una relazione di equivalenza tra gli stati;
  \item Diciamo che un sottoinsieme $ C \subseteq I $ è chiuso se $ \forall i\in C, \forall j \in I \setminus C $, $ i \nrightarrow j $ ($ i $ non comunica con $ j $);
  \item Chiamiamo \emph{stato assorbente} un singoletto chiuso (contenuto in $ I $);
  \item Un sottoinsieme $ B\subseteq I $ si dice \emph{irriducibile} se $ \forall i,j\in B\ i \leftrightarrow j $.
    Se invece tutto $ I $ è irriducibile, diremo che anche la matrice stocastica $ P $ associata alla catena di Markov è irriducibile. Più esplicitamente
    \[ \forall i,j\in I\ \exists n\in\N : {(P^{n})}_{ij} > 0. \]
    Osserviamo infine che le classi di equivalenza della relazione $ \leftrightarrow $ sono gli insiemi irriducibili massimali.
    Questo naturalmente implica che $ P $ sia anche irriducibile. Si può inoltre verificare che se $ P $ è primitiva, allora $ \forall i,j\in I\ {(P^{m})}_{ij}>0 $ definitivamente in $ m $.
  \item Uno stato $ i\in I $ si dice \emph{aperiodico} se definitivamente in $ n $ vale $ {(P^{n})}_{ii} > 0 $. Se $ i $ non è aperiodico, definiamo il \emph{periodo} di $ i $ come $ d_{i} \coloneqq \operatorname{MCD}\left\{n\in\N : {(P^{n})}_{ii} > 0 \right\} $;
  \end{itemize}
\end{definition}

\begin{proposition}
  Sia $ P $ una matrice stocastica irriducibile di dimensione $ n $. Allora \[ \tilde{P} = \frac{1}{2}(P+\Id_{n}) \] è anche primitiva.
\end{proposition}
\begin{proof}
  Dato che ogni matrice commuta con l'identità possiamo scrivere
  \[ {(P+\Id_{n})}^{m} = \sum_{k=0}^{m} \binom{m}{k} P^{k}\Id_{n}^{{m-k}} \geq \sum_{k=0}^{m} P^{k} \]
  e quindi $ \left({(P+\Id_{n})}^{m}\right)_{ij} \geq \sum_{k=0}^{m} {(P^{k})}_{ij} > 0 $ se scegliamo $ m $ maggiore o uguale al massimo tra gli esponenti che rendono $ P $ irriducibile.
\end{proof}

\begin{proposition}
  Uno stato $ i\in I $ è aperiodico se e solo se $ d_{i} = 1 $.
  \textcolor{red}{Prima è stato definito solo per stati periodici.}
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

\begin{proposition}
  Sia $ P $ matrice stocastica irriducibile, e supponiamo esista $ i\in I $\footnote{È qui essenziale l'ipotesi iniziale che la cardinalità di $ I $ sia finita.}  aperiodico. Allora $ P $ è primitiva.
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

\begin{example}
  Consideriamo $ P = \begin{psmallmatrix} 0 & 1 \\ 1 & 0 \end{psmallmatrix} $. Si può verificare che questa è irriducibile ma non primitiva.
  Considerando invece $ \tilde{P} = \frac{1}{2}(P+\Id_{2}) = \begin{psmallmatrix} 1/2 & 1/2 \\ 1/2 & 1/2 \end{psmallmatrix} $ otteniamo una matrice stocastica primitiva (che corrisponde ad aver aggiunto la possibilità di rimanere in ciascuno stato a una data estrazione).
\end{example}

\subsection{Ricorrenza degli stati}
\begin{definition}
  Per ogni $ i\in I $ definiamo le variabili aleatorie $ V_{i}\colon \Omega\to\N\cup\{+\infty\} $ nel seguente modo:
  \[ V_{i} \coloneqq \sum_{n=0}^{+\infty} \chi_{\{X_{n} = i\}}. \]
  Le $ V_{i} $ contano dunque quante volte lo stato $ i $ viene visitato nella ``evoluzione temporale''.
  Introduciamo inoltre il numero di visite allo stato $ i $ prima del tempo $ n $
  \[ V_{i}(n) \coloneqq \sum_{k=0}^{n-1} \chi_{\{X_{k} = i\}}. \]
\end{definition}
\begin{definition}
  Uno stato $ i\in I $ si dice
  \begin{itemize}
  \item \emph{ricorrente} se $ \PP\left(V_{i} = +\infty \right) = 1 $ cioè se lo stato $ i $ viene visitato infinite volte quasi certamente;
  \item \emph{transiente} se $ \PP\left(V_{i} = +\infty \right) = 0 $.
  \end{itemize}
\end{definition}

\begin{definition}[tempo di $ r $-esimo passaggio]
  Definiamo ulteriori variabili aleatorie $ \Omega\to\N\cup \{+\infty\} $:
  \[ T_{i}^{(r)} \coloneqq \inf\{ n\geq T_{i}^{(r-1)}+1 : X_{n} = i \}. \]
  Il caso $ r = 1 $, cioè il tempo di primo passaggio, è invece definito come
  \[ T_{i}^{(1)} \equiv T_{i} \coloneqq \inf\{n\geq 1 : X_{n} = i \} \]
  ricordando che $ \inf\emptyset = +\infty $.
\end{definition}

\begin{definition}[Lunghezza della $ r $-esima escursione]
  \begin{equation}\label{eq:markov-escursione}
    S_{i}^{(r)} \coloneqq
    \begin{cases}
      T_{i}^{(r)} - T_{i}^{(r-1)} & \text{se } T_{i}^{(r-1)} < +\infty \\
      0                           & \text{altrimenti}
    \end{cases}
  \end{equation}
\end{definition}

\begin{lemma}
  Sia $ A $ un evento generato da $ \left\{ X_{m} : m \leq T_{i}^{(r-1)} \right\} $. Allora
  \[ \PP\left( S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty, A \right) = \PP\left( S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty \right). \]
\end{lemma}

\begin{lemma}
  Vale
  \[ \PP\left(S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty \right) = \PP\left( T_{i} = k \middle| X_{0} = i \right). \]
\end{lemma}

\begin{lemma}
  Posto $ f_{i} \coloneqq \PP(T_{i} < +\infty | X_{0} = i ) $, si ha
  \[ \PP\left( V_{i} > r \middle| X_{0} = i \right) = {(f_{i})}^{r}. \]
\end{lemma}

\begin{thm}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora
  \begin{enumerate}[label=(\roman*)]
  \item $ f_{i} = 1\quad \iff\quad i $ è ricorrente $ \quad \iff\quad \sum_{n\geq 0} {(P^{n})}_{ii} = +\infty $;
  \item $ f_{i} < 1\quad \iff\quad i $ è transiente $ \quad \iff\quad \sum_{n\geq 0} {(P^{n})}_{ii} < +\infty $.
  \end{enumerate}
\end{thm}

\subsection{Convergenza}
Ci occupiamo qui di enunciare alcuni risultati che riguardano la convergenza della successione
\[ \lambda^{(n)} \coloneqq \lambda P^{n}, \]
che rappresenta, per il punto~\ref{pt:markov-prob-gen} del Teorema~\ref{thm:markov-prob}, la probabilità degli stati al tempo $ n $-esimo.
Se tale successione ammette limite $ \pi $, necessariamente dev'essere
\[ \pi P = \left( \lim_{n\to+\infty} \lambda P^{n} \right) P = \lim_{n\to+\infty} \lambda P^{n+1} = \pi \]
cioè la distribuzione $ \pi $ è \emph{invariante}.

\begin{thm}[convergenza all'equilibrio]\label{thm:markov-convergenza}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $, con $ P $ irriducibile e ogni $ i\in I $ aperiodico. Allora $ \exists! \pi $ distribuzione invariante. Inoltre
  \begin{tasks}(2)
    \task\label{pt:markov-convergenza-prob} $ \lim\limits_{n\to+\infty} \PP(X_{n} = j) = \pi_{j} \quad \forall j\in I $;
    \task\label{pt:markov-convergenza-mat} $ \lim\limits_{n\to+\infty} {(P^{n})}_{ij} = \pi_{j} \quad \forall i,j\in I $.
  \end{tasks}
\end{thm}

\begin{example}
  Facciamo un controesempio al Teorema~\ref{thm:markov-convergenza} nel caso in cui $ P $ non sia primitiva. Prendendo $ P = \begin{psmallmatrix} 0 & 1 \\ 1 & 0 \end{psmallmatrix} $ si ha
  \[
    \lambda^{(n)} =
    \begin{cases}
      \begin{psmallmatrix}
        \lambda_{1} & \lambda_{2}
      \end{psmallmatrix}
      & \text{per }n \text{ pari}\\
      \begin{psmallmatrix}
        \lambda_{2} & \lambda_{1}
      \end{psmallmatrix}
      & \text{per }n \text{ dispari}
    \end{cases}
  \]
  e quindi non può convergere.
\end{example}

\begin{thm}[ergodico per catene irriducibili]\label{thm:markov-ergodico}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $, con $ P $ irriducibile. Allora
  \begin{enumerate}[label=(\roman*)]
  \item Posto $ m_{i} \coloneqq \E_{i}[T_{i}] $ vale
    \[ \PP\left(\frac{V_{i}(n)}{n} \to \frac{1}{m_{i}} \right) = 1; \]
  \item $ \forall f\colon I\to\R $ limitata vale
    \[ \PP\left(\frac{1}{n} \sum_{k=0}^{n-1} f(X_{k}) \to \sum_{i\in I} \frac{f(i)}{m_{i}} \right) = 1. \]
  \end{enumerate}
\end{thm}

\subsubsection{Dimostrazione del Teorema di convergenza all'equilibrio}
Iniziamo a dimostrare alcuni risultati che ci serviranno nella dimostrazione del Teorema~\ref{thm:markov-convergenza}.
\begin{proposition}
  Se per ogni $ i,j \in I $ (finito) esiste  il $ \lim\limits_{n\to+\infty} {(P^{n})}_{ij} = \pi_{j} $ allora $ \pi = {(\pi_{j})}_{j\in I} $ è una distribuzione invariante.
\end{proposition}
\begin{proof}
  Verifichiamo che $ \pi $ è una distribuzione. È ovviamente a entrate non negative e inoltre vale
  \[ \sum_{j\in I} \pi_{j} = \sum_{j\in I} \lim_{n\to+\infty}{(P^{n})}_{ij} = \lim_{n\to+\infty} \sum_{j\in I} {(P^{n})}_{ij} = 1. \]
  Verifichiamo ora che $ \pi $ è invariante
  \begin{align*}
    {(\pi P)}_{j} & = \sum_{i\in I} \pi_{i} P_{ij} = \sum_{i\in I} \left(\lim_{n\to+\infty} {(P^{n})}_{ki} \right) P_{ij} = \lim_{n\to+\infty} \sum_{i\in I} {(P^{n})}_{ki} P_{ij} \\
                  & = \lim_{n\to+\infty} {(P^{n+1})}_{kj} = \pi_{j}. \qedhere
  \end{align*}
\end{proof}

Definiamo ora una distanza tra vettori di probabilità nel modo seguente
\begin{equation}\label{eq:dist-misure}
  d(\mu,\nu) \coloneqq \frac{1}{2} \sum_{i\in I} \abs{\mu_{i} - \nu_{i}}.
\end{equation}
\begin{lemma}
  Sia $ Q $ matrice stocastica e siano $ \mu, \nu $ vettori di probabilità su $ I $. Allora vale
  \begin{enumerate}[label=(\roman*)]
  \item\label{pt:lemma-d} $ d(\mu Q, \nu Q) \leq d(\mu, \nu) $;
  \item\label{pt:lemma-d-alpha} Se $ \exists\alpha\in (0,1] : \forall i,j\ Q_{ij} > \alpha $ allora $ d(\mu Q, \nu Q) \leq (1-\alpha) d(\mu,\nu) $.
  \end{enumerate}
\end{lemma}

\begin{proof}
  Poniamo
  \[ I^{+} \coloneqq \{i\in I: \mu_{i} > \nu_{i}\} \qquad \text{ e }\qquad I_{Q}^{+} \coloneqq \left\{i\in I: {(\mu Q)}_{i} > {(\nu Q)}_{i} \right\}. \]
  e ovviamente $ I^{-} \coloneqq I \setminus I^{+} $ e $ I_{Q}^{-} \coloneqq I\setminus I_{Q}^{+} $. Riscriviamo la distanza sopra introdotta come
  \begin{align*}
    d(\mu,\nu)      & = \frac{1}{2} \sum_{i\in I} \abs{\mu_{i} - \nu_{i}} = \frac{1}{2} \sum_{i\in I^{+}} (\mu_{i} - \nu_{i}) + \frac{1}{2} \sum_{i\in I^{-}} (\nu_{i} - \mu_{i}) \\
                    & = \frac{1}{2} \sum_{i\in I^{+}} (\mu_{i} - \nu_{i}) + \frac{1}{2} \left(1- \sum_{i\in I^{+}} \nu_{i}\right) - \frac{1}{2}\left(1 - \sum_{i\in I^{+}} \mu_{i} \right) = \sum_{i\in I^{+}} (\mu_{i} - \nu_{i}).
  \end{align*}
  Notiamo incidentalmente che da questa relazione si ottiene $ d(\mu,\nu) \leq 1 $ per ogni $ \mu $ e $ \nu $. Consideriamo ora
  \begin{align*}
    d(\mu Q, \nu Q) & = \sum_{i\in I_{Q}^{+}} \left[ {(\mu Q)}_{i} - {(\nu Q)}_{i} \right] = \sum_{i\in I_{Q}^{+}} \sum_{j\in I} (\mu_{j} - \nu_{j}) Q_{ji} \\
                    & \leq \sum_{i\in I_{Q}^{+}} \sum_{j\in I^{+}} (\mu_{j} - \nu_{j}) Q_{ji} \leq (1-\alpha) \sum_{j\in I^{+}} (\mu_{j} - \nu_{j}) = (1-\alpha) d(\mu, \nu)
  \end{align*}
  dato che $ I_{Q}^{+} \neq \emptyset $, il che dimostra la tesi~\ref{pt:lemma-d-alpha}. Per dimostrare la tesi~\ref{pt:lemma-d} osserviamo invece che
  \[
    d(\mu Q, \nu Q) \leq \sum_{i\in I_{Q}^{+}} \sum_{j\in I^{+}} (\mu_{j} - \nu_{j}) Q_{ji} \leq \sum_{i\in I} \sum_{j\in I^{+}} (\mu_{i} - \nu_{i}) Q_{ji}
    = \sum_{j\in I^{+}} (\mu_{j} - \nu_{j}) = d(\mu, \nu).\qedhere
  \]
\end{proof}
\begin{proof}[Dimostrazione del Teorema~\ref{thm:markov-convergenza}]
  Sia $ \lambda^{(n)} \coloneqq \lambda P^{n} $. Poiché $ P $ è primitiva (è irriducibile ed esiste uno stato aperiodico), $ \exists s\in\N, \exists \alpha\in (0,1] $ tale che $ {(P^{s})}_{ij} \geq \alpha \ \forall i,j \in I $. Affermo che la successione $ {(\lambda^{(n)})}_{n\in\N} $ è di Cauchy rispetto alla distanza $ d $ definita nella~\eqref{eq:dist-misure}. Considerando infatti $ h,k \geq s $, che scriviamo come
  \[ h = ms +r \qquad k = ms + q \]
  per opportuni $ m, r, q\in\N $, abbiamo
  \[
    d(\lambda^{(h)}, \lambda^{(k)}) = d(\lambda P^{h}, \lambda P^{k}) = d(\lambda P^{r} P^{ms}, \lambda P^{q} P^{ms})
    \leq {(1-\alpha)}^{m} d(\lambda P^{r}, \lambda P^{q}) \leq {(1-\alpha)}^{m}.
  \]
  Quindi, poiché $ \alpha > 0 $, dato un $ \varepsilon > 0 $ possiamo sempre trovare un $ m $ tale che $ d(\lambda^{(h)}, \lambda^{(k)}) < \varepsilon $ quando $ h,k \geq m $.

  Lo spazio dei vettori di probabilità è chiuso (è un ottante della buccia della palla di raggio unitario secondo la norma $ \norm{\cdot}_{1} $) in un completo ($ \R^{\card(I)} $)\footnote{Sappiamo che $ \R^{N} $ per ogni $ N\in\N $ è completo rispetto alla distanza indotta dalla norma euclidea, ma in dimensione finita tutte le norme sono equivalenti e dunque abbiamo completezza anche rispetto alla distanza $ d $ (che deriva appunto dalla norma $ \norm{\cdot}_{1} $).}, dunque è anch'esso completo.
  Pertanto esiste un $ \pi $ vettore di probabilità invariante tale che $ \lambda^{(n)} \to \pi $.
  Per l'unicità basta usare l'invarianza
  \[ d(\pi_{1}, \pi_{2}) = d(\pi_{1} P^{s}, \pi_{2} P^{s}) \leq (1-\alpha) d(\pi_{1}, \pi_{2}) \]
  dove $ s\in\N $ è tale che le entrate di $ P $ siano tutte non negative. Questo è assurdo perché altrimenti sarebbe $ \alpha d(\pi_{1}, \pi_{2}) \leq 0 $.
  Abbiamo così dimostrato (facendo riferimento anche al punto~\ref{pt:markov-prob-gen} del Teorema~\ref{thm:markov-prob}) la tesi~\ref{pt:markov-convergenza-prob}. Per la tesi~\ref{pt:markov-convergenza-mat} basta osservare che il vettore $ \pi $ trovato non dipende dalla particolare distribuzione iniziale $ \lambda $ e quindi basterà prendere $ \lambda = \delta_{i} $.
\end{proof}

\subsubsection{Dimostrazione del Teorema ergodico}
Al fine di dimostrare il Teorema~\ref{thm:markov-ergodico}, definiamo $ \gamma_{i}^{k} $ e dimostriamone le proprietà.
\begin{definition}
  \[ \gamma_{i}^{k} \coloneqq \E_{k}\left[ V_{i}(T_{k}) \right] = \E_{k}\left[ \sum_{n=0}^{T_{k}-1} \chi_{\{X_{n} = i \}}\right] \]
  Intuitivamente, $ \gamma_{i}^{k} $ ci dice quante volte ci aspettiamo di passare per $ i $ partendo da $ k $ prima di tornare in $ k $.
\end{definition}

\begin{proposition}
  Sia $ P $ irriducibile. Allora valgono le seguenti proprietà
  \begin{enumerate}[label=(\roman*)]
  \item $ \gamma_{k}^{k} = 1 $;
  \item\label{itm:gamma-k-i-inv} $ \gamma^{k} = {(\gamma_{i}^{k})}_{i\in I} $ è invariante;
  \item\label{itm:gamma-k-i-bound} $ 0 < \gamma_{i}^{k} < +\infty\ \forall i,k$;
  \item\label{itm:gamma-k-i-coincidenza} Se $ \lambda $ è invariante e $ \lambda_{k} = 1 $ allora $ \lambda = \gamma^{k} $;
  \item\label{itm:gamma-k-i-m} $ m_{i} \coloneqq \E_{i}\left[T_{i}\right] < +\infty $;
  \item $ \exists! $ distribuzione invariante $ \pi_{i} = 1/m_{i} $.
  \end{enumerate}
\end{proposition}
\begin{proof}
  Dimostriamo i vari punti.
  \begin{enumerate}[label=(\roman*)]
  \item Ovvio (numero di volte che visito $ k $ partendo da $ k $ prima di tornare in $ k $).

  \item Supponiamo per ora $ j\neq k $. Allora
    \begin{align*}
      \gamma_{j}^{k} &= \E_{k}\left[ \sum_{n=0}^{T_{k}-1} \chi_{\{X_{n} = j\}}\right]
                       \overset{(a)}{=} \E_{k}\left[ \sum_{n=1}^{T_{k}} \chi_{\{X_{n} = j\}}\right]
                       = \E_{k}\left[\sum_{n=1}^{+\infty} \chi_{\{X_{n} = j, n \leq T_{k}\}}\right]
                       \overset{(b)}{=} \sum_{n=1}^{+\infty} \E_{k}\left[\chi_{\{X_{n} = j, n \leq T_{k}\}}\right] \\
                     &\overset{(c)}{=} \sum_{n=1}^{+\infty} \PP_{k}(X_{n} = j, n \leq T_k)
                       = \sum_{n=1}^{+\infty} \PP_{k}\left(\bigcup_{i\in I} (X_{n-1} = i), X_{n} = j, n\leq T_k\right) \\
                     &\overset{(d)}{=} \sum_{n=1}^{+\infty} \sum_{i\in I} \PP_{k}\left(X_{n-1} = i, X_{n} = j, n\leq T_k\right)
                       \overset{(e)}{=} \sum_{i\in I} \sum_{n=1}^{+\infty} \PP_{k}\left(X_{n-1} = i, X_{n} = j, n\leq T_k\right) \\
                     &\overset{(f)}{=} \sum_{i\in I} \sum_{n=1}^{+\infty} \PP_{k}(X_{n-1}=i, n\leq T_k )\PP\left(X_{n} = j |X_{n-1}=i, n\leq T_k\right) \\
                     &\overset{(g)}{=} \sum_{i\in I} \sum_{n=1}^{+\infty} \PP_{k}(X_{n-1}=i, n\leq T_k )\PP\left(X_{n} = j |X_{n-1}=i, X_{n-1}\neq k\right) \\
                     &\overset{(h)}{=} \sum_{i\in I} \sum_{n=1}^{+\infty} \PP_{k}(X_{n-1}=i, n\leq T_k )\PP\left(X_{n} = j |X_{n-1}=i\right) \\
                     &= \sum_{i\in I} \sum_{n=1}^{+\infty} \E_{k}\left[\chi_{\{X_{n-1} = i, n \leq T_{k} \}}\right] P_{ij}
                       = \sum_{i\in I} \E_{k}\left[\sum_{n=1}^{+\infty} \chi_{\{X_{n-1} = i, n \leq T_{k} \}}\right] P_{ij} \\
                     & = \sum_{i\in I} \E_{k}\left[\sum_{n=1}^{T_k} \chi_{\{X_{n-1} = i\}}\right] P_{ij}
                       = \sum_{i\in I} \E_{k}\left[\sum_{n=0}^{T_k-1} \chi_{\{X_{n-1} = i\}}\right] P_{ij} \\
                     &= \sum_{i\in I} \gamma_{i}^{k} P_{ij} = {\left(\gamma^{k} P\right)}_{j}.
    \end{align*}
    Giustifichiamo le uguaglianze evidenziate dalle lettere latine:
    \begin{enumerate}[label=(\emph{\alph*})]
    \item $ j\neq k $;
    \item la somma è in realtà finita;
    \item definizione di valore di aspettazione di una caratteristica;
    \item gli eventi nell'unione sono disgiunti;
    \item $ I $ ha cardinalità finita;
    \item teorema della ``probabilità totale'';
    \item proprietà di Markov;
    \item se $ i = k $ il primo termine ha probabilità nulla.
    \end{enumerate}
    Se invece $ j = k $ allora, per quanto mostrato,
    \begin{align*}
      \gamma_{k}^{k} &= \sum_{i\in I} \gamma_{i}^{k} - \sum_{i\in I\setminus\{k\}} \gamma_{i}^{k}
                       = \sum_{i\in I} \gamma_{i}^{k} \sum_{l \in I} P_{il} - \sum_{i\in I\setminus\{k\}} \left(\gamma^{k} P\right)_{i}
                       = \sum_{l\in I} \sum_{i\in I} \gamma_{i}^{k} P_{il} - \sum_{i\in I\setminus\{k\}} \left(\gamma^{k} P\right)_{i} \\
                     &= \sum_{l\in I}\left(\gamma^{k} P\right)_{l} - \sum_{i\in I\setminus\{k\}}\left(\gamma^{k} P\right)_{i}
                       = \sum_{i\in I}\left(\gamma^{k} P\right)_{i} - \sum_{i\in I\setminus\{k\}}\left(\gamma^{k} P\right)_{i}
                       = \left(\gamma^{k} P\right)_{k}.
    \end{align*}

  \item Essendo $ P $ irriducibile, esiste un $ r \in \N : \forall i, j\in I, \ {(P^{r})}_{ij} > 0 $. Allora da un lato
    \[
      \gamma_{i}^{k} = {(\gamma^{k} P^{r})}_{i} = \sum_{j\in I} \gamma_{j}^{k} {(P^{r})}_{ji} \geq \gamma_{k}^{k} {(P^{r})}_{ki} = {(P^{r})}_{ki} > 0,
    \]
    mentre dall'altro per ogni $ j\in I $
    \[
      1 = \gamma_{k}^{k} = {(\gamma^{k} P^{r})}_{k} = \sum_{j\in I} \gamma_{j}^{k} {(P^{r})}_{jk} \geq \gamma_{j}^{k} {(P^{r})}_{jk} \quad \Rightarrow \quad \gamma_{j}^{k} \leq \frac{1}{{(P^{r})}_{jk}} < +\infty.
    \]

  \item Se $ j = k $ abbiamo già che $ \lambda_k = \gamma_{k}^{k} $. Per $ j \neq k $, usando la riscrittura del punto \ref{itm:gamma-k-i-inv}, dobbiamo mostrare che $ \forall j \in I $ vale
    \[
      \lambda_j = \sum_{n=1}^{+\infty} \PP_{k}(X_{n} = j, n\leq T_{k}) = \lim_{N\to+\infty} \sum_{n=0}^{N} \ \sum_{i_0\cdots i_{n-1} \in I\setminus\{k\}} P_{ki_{n-1}} \cdots P_{i_1 i_0} P_{i_0 j}.
    \]
    dove si intende che per $ n=0 $ l'unico termine della somma è $ P_{kj} $. \\
    Si ha
    \begin{align*}
      \lambda_{j} &= \sum_{i_0\in I} \lambda_{i_0} P_{i_0 j}
                    = \lambda_{k} P_{kj} + \sum_{i_0\in I\setminus\{k\}} \lambda_{i_0} P_{i_0 j} \\
                  &= P_{kj} + \sum_{i_0\in I\setminus\{k\}} \left(\sum_{i_1\in I} \lambda_{i_1} P_{i_1 i_0}\right) P_{i_0 j}
                    = P_{kj} + \sum_{i_0\in I\setminus\{k\}} \left(\lambda_{k} P_{k i_0} + \sum_{i_1\in I\setminus\{k\}} \lambda_{i_1} P_{i_1 i_0}\right) P_{i_0 j} \\
                  &= P_{kj} + \sum_{i_0\in I\setminus\{k\}} P_{k i_0} P_{i_0 j} + \sum_{i_0, i_1\in I\setminus\{k\}} \lambda_{i_1} P_{i_1 i_0} P_{i_0 j} \\
                  &= P_{kj} + \sum_{i_0\in I\setminus\{k\}} P_{k i_0} P_{i_0 j} + \sum_{i_0, i_1\in I\setminus\{k\}} \left(\sum_{i_2 \in I} \lambda_{i_2} P_{i_2 i_1}\right) P_{i_1 i_0} P_{i_0 j} \\
                  &= P_{kj} + \sum_{i_0\in I\setminus\{k\}} P_{k i_0} P_{i_0 j} + \sum_{i_0, i_1\in I\setminus\{k\}} \left(\lambda_{k} P_{k i_1} + \sum_{i_2 \in I\setminus\{k\}} \lambda_{i_2} P_{i_2 i_1}\right) P_{i_1 i_0} P_{i_0 j} \\
                  &= P_{kj} + \sum_{i_0\in I\setminus\{k\}} P_{k i_0} P_{i_0 j} + \sum_{i_0, i_1\in I\setminus\{k\}} P_{k i_1} P_{i_1 i_0}P_{i_0 j} + \sum_{i_0, i_1, i_2\in I\setminus\{k\}} \lambda_{i_2} P_{i_2 i_1} P_{i_1 i_0} P_{i_0 j}.
    \end{align*}
    Procedendo iterativamente in questo modo si ottiene
    \begin{multline*}
      \lambda_j = \overset{\mathwitch}{\cdots} =  P_{kj} + \sum_{i_0\in I\setminus\{k\}} P_{k i_0} P_{i_0 j} + \cdots + \sum_{i_0 \cdots i_{n-1}\in I\setminus\{k\}} P_{k i_{n-1}} \cdots P_{i_0 j} \\
      + \sum_{i_0 \cdots i_{n}\in I\setminus\{k\}} \lambda_{i_n} P_{i_{n} i_{n-1}} \cdots P_{i_0 j}.
    \end{multline*}
    Per concludere dobbiamo mostrare che l'ultimo termine tende a 0 per $ n \to +\infty $ \footnote{Di questo fatto non diamo una dimostrazione molto formale}: moralmente nel limite cercato l'ultimo termine rappresenta la probabilità di partire da uno stato diverso da $ k $ e di non tornare mai in $ k $; ma $ P $ è irriducibile $ \Rightarrow $ ricorrente, da cui concludiamo che tale probabilità deve tendere necessariamente a $ 0 $ all'aumentare del tempo.

  \item Per quanto mostrato al punto~\ref{itm:gamma-k-i-bound}, si ha
    \[
      \E_k[T_k] = \E_k\left[\sum_{n=0}^{T_{k}-1} 1\right] = \E_k\left[\sum_{n=0}^{T_{k}-1} \sum_{i\in I} \chi_{\{X_{n} = i\}}\right] = \sum_{i \in I} \E_k\left[\sum_{n=0}^{T_{k}-1} \chi_{\{X_{n} = i\}}\right] = \sum_{i \in I} \gamma_{i}^{k} < +\infty.
    \]

  \item Per l'esistenza, grazie al punto \ref{itm:gamma-k-i-inv}, sappiamo che il vettore $ \gamma^{k} $ normalizzato è un vettore di probabilità invariante. \\
    Per l'unicità abbiamo sia $ \lambda $ è un vettore di probabilità invariante; allora $ \exists k \in I : \lambda_{k} \neq 0 $. Per il \ref{itm:gamma-k-i-coincidenza} il vettore di probabilità $ \lambda/\lambda_{k} $ coincide con $ \gamma_{k} $ da cui $ \forall i \in I, \ \lambda_i = \lambda_{k} \gamma_{i}^{k} $. Osserviamo che per il punto \ref{itm:gamma-k-i-bound}, $ \gamma_i^{k} > 0 $ per ogni $ i \in I $, quindi anche $ \lambda_i > 0 $ per ogni $ i \in I $. Ma allora per quanto mostrato nella dimostrazione del punto \ref{itm:gamma-k-i-m}, $ \forall k \in I $ si ha
    \[
      1 = \sum_{i \in I} \lambda_{i} = \sum_{i \in I} \lambda_{k} \gamma_{i}^{k} = \lambda_{k} \sum_{i \in I} \gamma_{i}^{k} = \lambda_{k} m_{k}
    \]
    da cui segue la tesi.\qedhere
  \end{enumerate}
\end{proof}


\begin{thm}[Legge forte dei grandi numeri]\label{thm:grandi-numeri}
  Sia $ (\Omega, \mathcal{F}, \PP) $ uno spazio di probabilità e siano $ Y_{1}, \ldots, Y_{n}\colon \Omega \to \R $ variabili aleatorie indipendenti e identicamente distribuite. Sia inoltre $ \mu \coloneqq \E[Y_{i}] $. Allora
  \[ \PP\left(\frac{Y_{1} + \cdots + Y_{n}}{n} \to \mu \right) = 1. \]
\end{thm}

\begin{proof}[Dimostrazione del Teorema~\ref{thm:markov-ergodico}]
  Dato che $ P $ è irriducibile e $ I $ è finito, $ \PP(T_{i} < +\infty) = 1\ \forall i\in I $. Infatti, detto $ m $ un intero tale che $ {(P^{m})}_{ij} > 0 $
  \begin{align*}
    1 & = \PP_{j}(T_{j} < +\infty) = \PP_{j}(X_{n} = j \text{ infinite volte}) = \PP_{j}(X_{n} = j \text{ infinite volte con } n \geq m+1 ) \\
      & = \sum_{k\in I} \PP_{j}(X_{m} = k) \PP_{j}\left(x_{n} = j \text{ infinite volte con } n \geq m+1 \middle| X_{m} = k \right) \\
      &= \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(X_{n} = j \text{ infinite volte}) = \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(T_{j} < +\infty).
  \end{align*}
  Ora se fosse $ \PP_{k}(T_{j} < +\infty) < 1 $ sarebbe
  \[ 1 = \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(T_{j} < +\infty) < \sum_{k\in I} {(P^{m})}_{jk} = 1 \]
  perché $ P^{m} $ è stocastica, il che è assurdo. Dunque $ \PP_{k}(T_{j} < +\infty) = 1\ \forall k,j\in I $. È infine
  \[ \PP(T_{i} < +\infty) = \sum_{k\in I} \PP(X_{0} = k) \PP_{k}(T_{i} < +\infty) = 1. \]
  Questo risultato ci dice che, indipendentemente dal valore di $ X_{0} $, nell'evoluzione temporale lo stato $ i $ verrà visitato quasi certamente. Nel fare i limiti per $ n\to +\infty $, quindi, possiamo senza perdere di generalità supporre che sia $ X_{0} = i $, perché così facendo si stanno togliendo finiti punti. Possiamo dunque sostituire
  \[ \PP \rightarrow \PP_{i} \quad \text{ e } \quad \lambda \rightarrow \delta_{i}. \]
  Dimostriamo le due tesi:
  \begin{enumerate}[label=(\roman*)]
  \item Le variabili aleatorie $ S_{i}^{(r)} $ (definite nella~\eqref{eq:markov-escursione}) sono indipendenti e identicamente distribuite con $ E_{i}\left[S_{i}^{(r)}\right] = m_{i} $.
    Vale la stima
    \[ \sum_{k=1}^{V_{i}(n)-1} S_{i}^{(k)} \leq n \leq \sum_{k=1}^{V_{i}(n)} S_{i}^{(k)} \]
    che afferma che l'istante corrente è compreso tra il tempo necessario a passare per $ i $ $ {(V_{i}(n) - 1)} $ volte e il tempo necessario a passarvi $ V_{i}(n) $ volte. Infatti $ V_{i}(n) $ è il numero di volte in cui si passa per $ i $ prima dell'istante $ n $. Dividendo per $ V_{i}(n) $ si ha
    \[ \frac{\sum_{k=1}^{V_{i}(n)-1} S_{i}^{(k)}}{V_{i}(n)} \leq \frac{n}{V_{i}(n)} \leq \frac{\sum_{k=1}^{V_{i}(n)} S_{i}^{(k)}}{V_{i}(n)}. \]
    Ora poiché $ \PP\left( V_{i}(n) \to +\infty \right) = 1 $ (per ricorrenza), possiamo fare il limite in $ V_{i}(n) $ invece che in $ n $. Usando il Teorema~\eqref{thm:grandi-numeri} abbiamo
    \[ m_{i} \leq \lim_{n\to+\infty} \frac{n}{V_{i}(n)} \leq m_{i} \]
    da cui la tesi (tutte le stime sopra fatte valgono quasi certamente).
  \item Sia $ f\colon I\to\R $ limitata. Allora
    \begin{multline*}
      \frac{1}{n} \sum_{k=0}^{n-1} f(X_{k}) - \sum_{i\in I} \frac{f(i)}{m_{i}}  = \frac{1}{n} \sum_{k=0}^{n-1} \sum_{i\in I} f(i) \chi_{\{X_{k} = i\}} - \sum_{i\in I} \frac{f(i)}{m_{i}} \\
                                                                                = \sum_{i\in I} f(i) \left[ \frac{1}{n} \sum_{k=0}^{n-1} \chi_{\{X_{n} = i\}} - \frac{1}{m_{i}} \right] = \sum_{i\in I} \left[ \frac{V_{i}(n)}{n} - \frac{1}{m_{i}} \right] \to 0
    \end{multline*}
    quasi certamente.\qedhere
  \end{enumerate}
\end{proof}

\subsection{Inversione temporale}
Lo scopo è di definire una catena di Markov che sia il \emph{time-reversal} di una catena di Markov di finiti passi. Diamo implicitamente la definizione tramite il seguente
\begin{thm}[catena inversa]
  Sia $ P $ irriducibile e $ \pi $ un vettore di probabilità invariante per $ P $. Sia inoltre $ {(X_{n})}_{n=0}^{N} $ Markov $ (\pi, P) $. Se $ Y_n \coloneqq X_{N-n} $ allora $ {(Y_n)}_{n=0}^{N} $ è Markov $ (\pi \hat{P}) $ dove $ \hat{P} $ è data $ \forall i, j $ da\footnote{\label{fn:non-matrice-vettore}\emph{Non} è un prodotto matrice vettore.}
  \begin{equation}\label{eq:inversione-matrice}
    \pi_{j} \hat{P}_{ji} = \pi_{i} P_{ij}.
  \end{equation}
  Inoltre $ \hat{P} $ è irriducibile e $ \pi $ è invariante anche per $ \hat{P} $.
\end{thm}
\begin{proof}
  Prima di tutto $ \hat{P} $ è stocastica:
  \[
    \sum_{i\in I}\hat{P}_{ji} = \sum_{i\in I} \frac{\pi_{i} P_{ij}}{\pi_{j}} = \frac{1}{\pi_{j}} \left(\pi P\right)_{j} = \frac{\pi_{j}}{\pi_{j}} = 1.
  \]
  Per mostrare che $ (Y_n) $ è Markov mostriamo che vale l'equazione~\eqref{eq:markov-prodotto}; infatti usando la~\eqref{eq:inversione-matrice}
  \begin{align*}
    \PP(Y_0 = i_0, \ldots, Y_{n} = i_{n}) &= \PP(X_{N-n} = i_n, \ldots, X_{N} = i_{0}) \\
                                          & = \pi_{i_n} P_{i_n i_{n-1}} \cdots P_{i_1 i_0} = \hat{P}_{i_{n-1} i_n} \pi_{i_{n-1}} P_{i_{n-1} i_{n-2}} \cdots P_{i_1 i_0} \\
                                          & = \cdots = \hat{P}_{i_{n-1} i_n} \cdots \hat{P}_{i_1 i_2} \pi_{i_1} P_{i_1 i_0} = \hat{P}_{i_{n-1} i_n} \cdots \hat{P}_{i_0 i_1} \pi_{i_0} \\
                                          & = \pi_{i_0} \hat{P}_{i_0 i_1} \cdots \hat{P}_{i_{n-1} i_n}.
  \end{align*}
  Mostriamo ora che $ \pi $ è invariante:
  \[
    \left(\pi \hat{P}\right)_{i} = \sum_{j\in I} \pi_{j} \hat{P}_{ji} = \sum_{j\in I} \pi_{i} P_{ij} = \pi_{i} \sum_{j\in I} P_{ij} = \pi_{i}.
  \]
  Infine mostriamo che $ \hat{P} $ è irriducibile: essendo $ P $ irriducibile abbiamo $ \forall i, j, \ \exists m\in\N : {(P^{m})}_{ij} > 0 $, da cui $ \exists i_0 = i, i_1, \ldots, i_m=j $ tali che $ P_{i_0 i_1} \cdots P_{i_{n-1} i_n} > 0 $. Ma allora ``invertendo il cammino'' si ha
  \[
    \left(\hat{P}^{m}\right)_{ji} = \hat{P}_{i_{n} i_{n-1}} \cdots \hat{P}_{i_1 i_0} = \left(\frac{\pi_{i_{n-1}}}{\pi_{i_n}} P_{i_{n-1} i_n}\right) \cdots \left(\frac{\pi_{i_0}}{\pi_{i_1}} P_{i_0 i_1}\right) = \frac{\pi_{i_{n}}}{\pi_{i_0}} \left(P_{i_0 i_1} \cdots P_{i_{n-1} i_n}\right) > 0. \qedhere
  \]
\end{proof}

\begin{definition}[catena reversibile]
  $ {(X_n)}_{n=0}^{N} $ Markov $ (\pi, P) $ è reversibile se ammette un'inversione temporale e risulta $ P = \hat{P} $.
\end{definition}

\begin{definition}[bilancio dettagliato]
  Una matrice stocastica $ P $ e un vettore di probabilità $ \lambda $ si dicono in bilancio dettagliato se vale\textsuperscript{\ref{fn:non-matrice-vettore}}
  \begin{equation}
    \lambda_{i} P_{ij} = \lambda_{j} P_{ji}
  \end{equation}
\end{definition}
Osserviamo che se $ \lambda $ è in bilancio dettagliato con $ P $ allora $ \lambda $ è distribuzione invariante per $ P $:
\[
  (\lambda P)_i = \sum_{j\in I}\lambda_j P_{ji} = \sum_{j\in I} \lambda_{i} P_{ij} = \lambda_{i} \sum_{j\in I} P_{ij} = \lambda_{i}.
\]
Tale proprietà suggerisce la seguente
\begin{proposition}
  Sia $ {(X_n)}_{n=0}^{N} $ Markov $ (\pi, P) $ \textcolor{red}{con $ P $ irriducibile}. Allora $ (X_n) $ è reversibile se e solo se $ \lambda $ e $ P $ sono in bilancio dettagliato.
\end{proposition}

\begin{definition}[corrente di probabilità]
  Sia $ P $ irriducibile e $ \lambda $ un vettore di probabilità. Chiamiamo corrente di probabilità $ i \to j $ \[ J(i\to j) \coloneqq \lambda_{i}P_{ij} - \lambda_{j}P_{ji}. \]
\end{definition}

\subsection{Generazione di numeri casuali}
\subsubsection{Introduzione}
\textcolor{red}{Mancante}

\subsubsection{Algoritmo di Matropolis-Hastings}
\textcolor{red}{Mancante}

\subsubsection{Algorimo ``un blocco alla volta''}
\textcolor{red}{Mancante}
