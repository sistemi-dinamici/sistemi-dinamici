\section{Sistemi di funzioni iterate}
\textcolor{red}{Introduzione}

\begin{example}[triangolo di Sierpiński] \label{ex:sierpinski}
  Consideriamo le seguenti mappe $ w_i \colon \R^2 \to \R^2 $
  \[
    w_1 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} \quad
    w_2 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} + \begin{pmatrix} 1/4 \\ \sqrt{3}/4 \end{pmatrix} \quad
    w_1 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} + \begin{pmatrix} 1/2 \\ 0 \end{pmatrix}
  \]
  e il triangolo equilatero di lato unitario $ T \coloneqq \{(x, y) \in \R^2 : x \geq 0, \ y \leq \sqrt{3}x , \ y \leq -\sqrt{3}(x-1)\} $. Con riferimento alla Figure \ref{fig:sierpinski}, l'unione delle 3 immagini di $ T $ tramite le $ w_1 $, $ w_2 $ e $ w_3 $ è la forma più scura in figura. \textcolor{red}{Su ogni triangolo itero questo passo e ottengo il triangolo di Sierpiński}
\end{example}

\begin{example}[insieme di Cantor]
  \textcolor{red}{Mancante}
\end{example}

\begin{definition}[dimensione di box-counting]
  Sia $ F \subseteq \R^n $ \textcolor{red}{compatto} e, fissato $ \delta > 0 $ reale, $ N_\delta(F) $ il minimo numero di palle di raggio $ \delta $ necessarie per coprire $ F $\footnote{Esiste finito per compattezza.}. Definiamo allora
  \begin{itemize}
  \item dimensione di box-counting \emph{inferiore} \[ \underline{\dim} F \coloneqq \liminf_{\delta \to 0} \frac{\log N_\delta(F)}{-\log{\delta}}; \]
  \item dimensione di box-counting \emph{superiore} \[ \overline{\dim} F \coloneqq \limsup_{\delta \to 0} \frac{\log N_\delta(F)}{-\log{\delta}}. \]
  \end{itemize}
  Se la dimensione inferiore e superiore coincidono, definiamo la dimensione di box-counting di $ F $ come $ \dim F \coloneqq \underline{\dim} F = \overline{\dim} F $.
\end{definition}

\begin{definition}[distanza di Hausdorff]
  Sia $ (X, d) $ uno spazio metrico e $ A, B \subseteq X $. Definiamo la distanza di Hausdorff tra $ A $ e $ B $ come
  \[
    h(A, B) \coloneqq \max\{d(A, B), d(B, A)\}
  \]
  dove
  \[
    d(A, B) \coloneqq \sup_{x \in A} d(x, B) = \sup_{x \in A} \inf_{y \in B} d(x, y)
  \]
  e similmente per $ d(B, A) $.
\end{definition}

La distanza di Hausdorff è un modo per quantificare quanto sono distanti due sottoinsiemi di uno spazio metrico, secondo la metrica data. La definizione appena data, tuttavia, non è ben posta per sottoinsiemi generici di $ X $, in quanto non è nemmeno garantito che tale $ h $ sia finita. Se però $ A $ e $ B $ sono compatti non vuoti, la funzione $ y \mapsto d(x, y) $ è continua su un compatto e quindi $ \inf_{y \in B} d(x, y) = \min_{y \in B} d(x, y) $; similmente la funzione $ x \mapsto d(x, B) $ è continua su un compatto, così $ \sup_{x \in A} d(x, B) = \max_{x \in A} d(x, B) $. Discorso analogo vale per $ d(B, A) $. Se ci restringiamo a $ \mathcal{K}(X) $, lo spazio dei compatti di $ X $ non vuoti, la distanza di Hausdorff risulta quindi finita. In realtà, la distanza di Hausdorff è veramente una distanza su $ \mathcal{K}(X) $ come enunciato nel seguente
\begin{thm}
  Sia $ (X, d) $ uno spazio metrico, $ \mathcal{K}(X) = \{K \subseteq X : K \neq \emptyset, \ K \text{ compatto}\} $ e $ h $ la distanza di Hausdorff. Allora
  \begin{enumerate}[label=(\roman*)]
  \item lo spazio $ (\mathcal{K}(X), h) $ è uno spazio metrico;
  \item se $ (X, d) $ è completo, anche $ (\mathcal{K}(X), h) $ è completo;
  \item se $ (X, d) $ è compatto, anche $ (\mathcal{K}(X), h) $ è compatto.
  \end{enumerate}
\end{thm}

Sia ora $ (X, d) $ uno spazio metrico compatto. Consideriamo le funzioni $ v_1, \ldots, v_n \colon X \to X $ e le rispettive funzioni $ w_1, \ldots, w_n \colon \mathcal{K}(X) \to \mathcal{K}(X) $ date da $ w_i(K) \coloneqq \{v_i(x) : x \in K\} $ che agiscono sui sottoinsiemi compatti di $ X $. Essendo le immagini delle $ w_i $ degli insiemi è ben definita la mappa
\begin{align*}
  w \colon \mathcal{K}(X) & \to \mathcal{K}(X) \\
  K & \mapsto \bigcup_{i=1}^{n} w_i(K)
\end{align*}
Se le $ w_i $ sono continue secondo la topologia indotta dalla distanza di Hausdorff, si può verificare che anche $ w $ è continua e pertanto la terna $ (\mathcal{K}(X), h, w) $ è un sistema dinamico topologico.

\begin{proposition}
  Supponiamo che le $ w_i $ siano contrazioni su $ (\mathcal{K}(X), h) $ con costante di Lipschitz $ s_i \in [0, 1) $. Allora anche $ w $ è una contrazione su $ (\mathcal{K}(X), h) $ con costante di Lipschitz $ s \coloneqq \max s_i $.
\end{proposition}

Tale proposizione risulta particolarmente utile nel caso in cui $ (X, d) $ sia anche uno spazio metrico completo (oltre che compatto). Infatti in tale caso anche $ (\mathcal{K}(X), h) $ è completo e quindi il teorema di Banach-Caccioppoli o delle contrazioni assicura l'esistenza di un unico punto fisso per $ w $, ovvero di un unico insieme $ K_w \in \mathcal{K}(X) $ tale $ w(K_w) = K_w $ detto \emph{attrattore globale} del sistema. Come noto della dimostrazione del teorema delle contrazioni, il punto fisso si trova come punto limite delle iterazioni di $ w $ a partire da un qualunque elemento di $ \mathcal{K}(X) $: pertanto tutte le orbite nello spazio delle fasi tramite $ w $ convergeranno allo stesso $ K_w $ secondo la distanza di Hausdorff.

Il triangolo di Sierpiński è un esempio di attrattore globale per l'unione delle mappe definire nell'Esempio \ref{ex:sierpinski}. Infatti non è necessario partire da triangolo per ottenere il triangolo di Sierpiński, come si può vedere nella Figura \ref{fig:sierpinski-acaso}.

\textcolor{red}{Figura mancante}
