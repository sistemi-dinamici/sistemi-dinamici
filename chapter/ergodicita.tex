\section{Teoria ergodica}
\begin{definition}[frequenze di visita]
  Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Dato $ A \in \mathcal{A} $, $ x \in X $ e $ n \in \N $ definiamo la frequenza media delle visite ad $ A $ dell'orbita di $ x $ da $ 0 $ a $ n $ come
  \[
    \nu(x, A, n) \coloneqq \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j}(x)).
  \]
  Definiamo inoltre
  \begin{align*}
    \overline{\nu}(x, A) & \coloneqq \limsup_{n \to +\infty} \nu(x, A, n) \\
    \underline{\nu}(x, A) & \coloneqq \liminf_{n \to +\infty} \nu(x, A, n)
  \end{align*}
  Se $ \overline{\nu} = \underline{\nu} $ allora definiamo la frequenza media delle visite ad $ A $ dell'orbita di $ x $:
  \begin{equation}
    \nu(x, A) \coloneqq \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j}(x)).
  \end{equation}
\end{definition}
Quest'ultima è una buona definizione in virtù del seguente
\begin{thm}[Birkhoff]\label{thm:Birkhoff}
  Sia $ (X,\mathcal{A},\mu,f) $ un sistema dinamico misurabile. Allora $ \forall A\in\mathcal{A} $ e per $ \mu $-q.o. $ x\in X $
  \[ \exists \lim_{n \to +\infty} \nu(x,A,n) = \nu(x,A) \, . \]
  Inoltre, $ \forall \varphi \in L^1(X,\mathcal{A},\mu) $ e per $ \mu $-q.o. $ x\in X $
  \[ \exists \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \varphi\circ f^j(x) \eqqcolon \tilde{\varphi}(x) \, . \]
  La funzione $ \tilde{\varphi} $ viene detta \emph{media temporale} dell'osservabile $ \varphi $.
\end{thm}

\begin{definition}[sistema ergodico]
  Un sistema dinamico misurabile $ (X, \mathcal{A}, \mu, f) $ si dice ergodico se $ \forall A \in \mathcal{A} $ e per $ \mu $-q.o. $ x \in X $ vale
  \[
    \nu(x, A) = \mu(A)
  \]
  cioè se la frequenza statistica delle visite coincide con la probabilità a priori.
\end{definition}
In Tabella \ref{tab:ergodica-vs-probabilia}, si riporta un confronto tra il linguaggio probabilistico e quello usato in teoria ergodica.

\begin{table}
  \centering
  \begin{tabularx}{\textwidth}{cXX}
    & \textsc{Teoria ergodica} & \textsc{Probabilità} \\ \toprule
    $ X $ & spazio delle fasi & spazio dei campioni \\
    $ \mathcal{A} $ & $ \sigma $-algebra dei misurabili & collezione degli eventi \\
    $ \mu $ & misura $ \mu(A) $ & probabilità $ \PP{(x \in A)} $ \\
    $ \varphi \colon X \to \R $ & osservabile & variabile aleatoria \\
    $ \varphi_n \coloneqq (\varphi \circ f^{n})_{n \in \N} $ & valore di un'osservabile lungo un'orbita & processo stocastico con distribuzione  $ \PP{(\varphi_1 \in A_1, \ldots, \varphi_k \in A_k)} =  \mu\left(\bigcap_{j = 1}^{k} \{x \in X : \varphi(f^{j}(x)) \in A_j\}\right) $ \\
    & $ f $-invarianza di $ \mu $ & processo stazionario \\ \bottomrule
  \end{tabularx}
  \caption{teoria ergodica e probabilità.}
  \label{tab:ergodica-vs-probabilia}
\end{table}

\begin{lemma} \label{lem:insieme-quasi-invariante}
  Sia $ A \in \mathcal{A} $ un insieme quasi invariante, cioè tale che $ f(A) \subseteq A \cup N $ con $ \mu(N) = 0 $. Allora esiste $ A' \in \mathcal{A} $ che è $ f $-invariante ed è quasi uguale a $ A $, cioè $ \mu(A \Delta A') = 0 $.
\end{lemma}
\begin{proof}
  Basta prendere $ N' \coloneqq \bigcup_{j \geq 0} f^{-j}(N) $ e $ A' \coloneqq A \setminus N' $. Infatti se $ x \in A' $ allora $ x \in A $ ma $ x \notin N' $ cioè $ \forall j \geq 0, \ x \notin f^{-j}(N) $; quindi $ f(x) \in A \cup N $ e $ \forall j \geq 0, \ f(x) \notin f^{-j}(N) $ da cui in particolare $ f(x) \notin f^0(N) = N $ così $ f(x) \in A \setminus N' $ ovvero $ A' $ è $ f $-invariante. Inoltre per $ f $-invarianza della misura $ \mu(N') \leq \sum_{j \geq 0} \mu(f^{-j}(N)) = 0 $ così $ \mu(A\Delta A') = \mu(A\Delta(A\setminus N')) = \mu(A \cap N') \leq \mu(N') = 0 $.
\end{proof}

\begin{thm}
  Fissato un $ p \geq 1 $, le seguenti proprietà sono equivalenti:
  \begin{enumerate}[label=(\roman*)]
  \item $ (X, \mathcal{A}, \mu, f) $ è ergodico.
  \item $ (X, \mathcal{A}, \mu, f) $ è \emph{metricamente indecomponibile}, cioè $ \forall A \in \mathcal{A} : f(A) \subseteq A, \ \mu(A) > 0 \Rightarrow \mu(A) = 1 $, ovvero lo spazio delle fasi non può essere separato in due insiemi disgiunti, $ f $-invarianti ed entrambi di misura non nulla.
  \item $ (X, \mathcal{A}, \mu, f) $ ha solo integrali primi del moto banali cioè $ \forall \varphi \in L^p(X, \mathcal{A}, \mu; \R) : \varphi \circ f = \varphi $ $ \mu $-q.o. si ha che $ \varphi $ è costante $ \mu $-q.o. in $ X $.
  \item $ \forall \varphi \in L^p(X, \mathcal{A}, \mu; \R) $ la media temporale di $ \varphi $ (che esiste per il Teorema \ref{thm:Birkhoff}) è $ \mu $-q.o. uguale alla media spaziale, cioè
    \begin{equation} \label{eqn:ergodico-mediaosservabile}
      \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi \circ f^j)(x) = \int_{X} \varphi(y) \dif{\mu}(y)
    \end{equation}
    per $ \mu $-q.o. $ x \in X $.
  \item $ \forall A, B \in \mathcal{A} $
    \begin{equation}\label{eq:mescolamento_in_media}
      \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \mu(A) \mu(B)
    \end{equation}
    cioè il sistema dinamico è \emph{mescolante in media}.
  \end{enumerate}
\end{thm}
\begin{proof}
  Mostriamo le varie implicazioni.
  \begin{description}
  \item[$ (i) \Rightarrow (ii) $] Sia $ A\in\mathcal{A} : f(A) \subseteq A $ con $\mu(A) > 0 $. Poiché $ A $ è $ f $-invariante, $ \forall x\in A\ \chi_A(f^j(x)) = 1 $ e dunque $ \nu(x,A) = 1 $ da cui, per l'ergodicità, $ \mu(A) = 1 $.

  \item[$ (ii) \Rightarrow (iii) $] L'idea è che, poiché $ \varphi $ è costante sulle orbite, i suoi insiemi di livello sono $ f $-invarianti; se $ \varphi $ è costante allora un insieme di livello è tutto lo spazio e gli altri sono vuoti; se invece non fosse costante si potrebbe decomporre lo spazio in più insiemi $ f $-invarianti, in contrasto con l'ipotesi. \\
    Sia $ \varphi $ un integrale primo in $ L^p(X, \mathcal{A}, \mu; \R) $. Supponiamo ora che valga l'ipotesi più forte $ \varphi = \varphi \circ f $ ovunque. Fissato $ \gamma \in \R $, sia $ A_\gamma \coloneqq \{x \in X : \varphi(x) \leq \gamma\} $ il sottolivello che è per ipotesi misurabile. Esso è invariante poiché, preso $ y \in f(A_y) $, si ha che $ \exists x\in A_\gamma : y=f(x) $ e quindi $ \varphi(y) = \varphi(f(x)) = \varphi (x) \leq \gamma $, cioè $ y\in A_\gamma $. Perciò l'ipotesi \emph{(ii)} ci dice che $ \mu(A_\gamma) \in \{0, 1\} $. \\
    Consideriamo la funzione
    \begin{align*}
      \psi \colon \R & \to [0, +\infty) \\
      \gamma & \mapsto \mu(A_\gamma)
    \end{align*}
    che è monotona non decrescente ($ \gamma_1 < \gamma_2 \Rightarrow A_{\gamma_1} \subseteq A_{\gamma_2} \Rightarrow \mu(A_{\gamma_1}) \leq \mu(A_{\gamma_2}) $). Osserviamo che
    \[
      \lim_{\gamma \to -\infty} \mu(A_\gamma) = 0 \qquad \lim_{\gamma \to +\infty} \mu(A_\gamma) = 1
    \]
    da cui deduciamo che esiste $ \overline{\gamma} \coloneqq \inf{\{\gamma \in \R : \mu(A_\gamma) = 1\}} \in \R $. Ma essendo gli $ A_\gamma $ inscatolati
    \[
      A_{\overline{\gamma}} = \bigcap_{n \geq 1} A_{\overline{\gamma} + \frac{1}{n}} \quad \Rightarrow \quad A_{\overline{\gamma} + \frac{1}{n}} \downarrow A_{\overline{\gamma}}
    \]
    da cui, per l'Esercizio \ref{es:limitemisuredasopra}
    \[
      \mu(A_{\overline{\gamma}}) = \lim_{n \to +\infty} \mu\left(A_{\overline{\gamma} + \frac{1}{n}}\right) = 1.
    \]
    Concludiamo quindi che $ \varphi(x) = \overline{\gamma} $ per $ \mu $-q.o. $ x \in X $ essendo $ \mu(A_\gamma) = 0 $ per ogni $ \gamma < \overline{\gamma} $ e $ A_{\overline{\gamma}} $ di misura 1. \\
    Supponiamo ora che $ \varphi = \varphi \circ f $ $ \mu $-q.o. Detto allora $ B_\gamma \coloneqq f^{-1}(A_\gamma) = \{x \in X : \varphi(f(x)) \leq \gamma\} $ si ha $ \mu(A_\gamma \Delta B_\gamma) = 0 $ e pertanto $ \exists N \in \mathcal{A} $ con $ \mu(N) = 0 $ tale che $ A_\gamma = B_\gamma \Delta N $. Così $ B_\gamma = f^{-1}(A_\gamma) = f^{-1}(B_\gamma) \Delta f^{-1}(N) $. Quindi posto $ N' \coloneqq f^{-1}(N) $ per invarianza della misura si ha $ \mu(N') = 0 $ e inoltre
    \[
      f(B_\gamma \Delta N') = f(f^{-1}(B_\gamma)) \subseteq B_\gamma = (B_\gamma \Delta N') \Delta N'
    \]
    cioè $ B_\gamma \Delta N' $ è invariante a meno di un insieme di misura nulla. Per il Lemma \ref{lem:insieme-quasi-invariante} sappiamo che esiste $ B'_\gamma \in \mathcal{A} $ tale che $ f(B_\gamma) \subseteq B_\gamma $ e $ \mu(B_\gamma \Delta B'_\gamma) = 0 $. Per l'ipotesi \emph{(ii)} allora $ \mu(B'_\gamma) \in \{0, 1\} $ così\footnote{
      Se $ F, M \in \mathcal{A} $ e $ \mu(M) = 0 $ allora \[\mu(F \Delta M) = \mu((F \setminus M) \cup (M \setminus F)) = \mu(F \setminus M) + \mu(M \setminus F) = \mu(F) - \mu(F \cap M) + \mu(M \setminus F) = \mu(F)\] in quanto $ F \cap M $ e $ M \setminus F $ sono sottoinsiemi misurabili di un insieme di misura nulla.
    }
    \[
      \mu(A_\gamma) = \mu(B_\gamma \Delta N') = \mu(B_\gamma) = \mu((B_\gamma \Delta B'_\gamma) \Delta B'_\gamma) = \mu(B'_\gamma) \in \{0, 1\}.
    \]
    A questo punto si conclude come in precedenza.

  \item[$ (iii) \Rightarrow (iv) $] Osserviamo che la media temporale $ \tilde{\varphi} $ di un'osservabile $ \varphi $ è un integrale primo del moto in quanto
    \begin{align*}
      (\tilde{\varphi} \circ f)(x) & = \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j+1})(x) = \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 1}^{n} (\varphi \circ f^{j})(x) \\
                                   & = \lim_{n \to +\infty} \left(\frac{n+1}{n} \frac{1}{n+1} \sum_{j = 0}^{n} (\varphi \circ f^{j})(x) - \frac{\varphi(x)}{n} \right) \\
                                   & = 1 \cdot \tilde{\varphi}(x) - 0 = \tilde{\varphi}(x).
    \end{align*}
    e quindi per ipotesi $ \exists c \in \R : \tilde{\varphi}(x) = c $ per $\mu$-q.o $ x\in X $. Vogliamo adesso mostrare che $c = \int_{X}\varphi\dif{\mu}$. \\
    Supponiamo per ora $ \varphi \geq 0 $. Per $ f $-invarianza della misura si ha
    \begin{align*}
      \int_X \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j})(x) \dif{\mu}(x)
      & = \frac{1}{n} \sum_{j = 0}^{n-1} \int_X \varphi(f^{j}(x)) \dif{\mu}(x)
        = \frac{1}{n} \sum_{j = 0}^{n-1} \int_{f^j(X)} \varphi(y) \dif{f^j_\sharp\mu}(y) \\
      & = \frac{1}{n} \sum_{j = 0}^{n-1} \int_X \varphi(y) \dif{\mu}(y)
    \end{align*}
    dove abbiamo usato il fatto che $ X \subseteq f^{-1}(f(X)) \subseteq X $, così $ f^{-1}(f(X)) = X $ e quindi $ \mu(f(X)) = \mu(f^{-1}(f(X))) = \mu(X) $ da cui $ X $ e $ f(X) $ sono uguali a meno di insiemi di misura nulla. Concludiamo così che $ \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \in L^1 $. Possiamo quindi applicare il lemma di Fatou per ottenere
    \begin{align*}
      c & = \int_{X} \tilde{\varphi} \dif{\mu} = \int_{X} \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} = \int_{X} \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} \\
        & \leq \liminf_{n \to +\infty} \int_{X} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} = \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \int_{X} (\varphi \circ f^{j}) \dif{\mu} \\
        & = \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \int_{X} \varphi \dif{\mu} = \int_{X} \varphi \dif{\mu}.
    \end{align*}
    dove nella prima uguaglianza si è usato il fatto che $ \tilde{\varphi} $ è quasi ovunque constante e $ \mu(X) = 1 $. \\
    Per la disuguaglianza opposta consideriamo la successione delle troncate $ \varphi_N = \min\{\varphi, N\} $ con $ N \in \N $, e definiamo:
    \[\widetilde{\varphi_N}(x) = \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\]
    Come prima si ottiene che $\widetilde{\varphi_N}$ è un integrale primo del moto, e quindi per ipotesi $ \forall N\in \N\ \exists c_N \in \R : \forall\mu\text{-q.o.}\ x\in X,\ \widetilde{\varphi_N}(x) = c_N $.
    Si ha adesso però che:
    \[\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x) \leq \frac{1}{n}\sum_{j=0}^{n-1} N = N \]
    e quindi per convergenza dominata si può concludere anche che:
    \begin{align*}
      c_N &= \int_{X}\widetilde{\varphi_N}(x)\dif{\mu} = \int_{X}\lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\dif{\mu}
            = \lim\limits_{n\to +\infty}\int_{X}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\dif{\mu}\\
          &= \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}\int_{X}(\varphi_N\circ f^j)(x)\dif{\mu}
            = \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}\int_{X}\varphi_N\dif{\mu} = \int_{X} \varphi_N \dif{\mu}.
    \end{align*}
    Abbiamo adesso che $\varphi_N \uparrow \varphi$, da cui per convergenza monotona:
    \[c_N = \int_{X} \varphi_N \dif{\mu} \xrightarrow{N\to\infty} \int_{X} \varphi \dif{\mu}. \]
    Abbiamo infine che $\varphi_N\leq\varphi$ implica $\widetilde{\varphi_N}\leq\tilde{\varphi}$, da cui
    \[ \int_{X}\varphi\dif{\mu} = \lim\limits_{n\to\infty}c_N = \lim\limits_{n\to\infty} \int_{X}\widetilde{\varphi_N}\dif{\mu}\leq\lim\limits_{n\to\infty} \int_{X}\tilde{\varphi}\dif{\mu} = \int_{X}\tilde{\varphi}\dif{\mu} = c \]
    che fornisce la disuguaglianza opposta tra $c$ e $\int_{X}\tilde{\varphi}\dif{\mu}$.


  \item[$ (iv) \Rightarrow (i) $] Basta scegliere $ \varphi = \chi_A $ con $ A \in \mathcal{A} $ e usare la formula \eqref{eqn:ergodico-mediaosservabile}.

  \item[$ (iv) \Rightarrow (v) $] Siano $ A, B \in \mathcal{A} $. Osservando che $ \chi_A \circ f^j = \chi_{f^{-j}(A)} $ abbiamo che
    \[
      \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \frac{1}{n} \sum_{j=0}^{n-1} \int_X \chi_{f^{-j}(A)} \cdot \chi_B \dif{\mu} = \int_{B} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu}.
    \]
    Ora $ \chi_A \circ f^j \leq 1 $ e $ 1 $ è integrabile essendo lo spazio di misura finita. Quindi per convergenza dominata e usando la formula \eqref{eqn:ergodico-mediaosservabile} con $ \varphi = \chi_A $ otteniamo
    \[
      \lim_{n \to +\infty} \int_{B} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu} =  \int_{B} \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu} = \int_B \left(\int_X \chi_A \dif{\mu}\right) \dif{\mu}
    \]
    da cui segue la tesi
    \[
      \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \mu(A) \int_B \dif{\mu} = \mu(A) \mu(B).
    \]
  \item[$ (v) \Rightarrow (ii) $] Dato $ A \in \mathcal{A} $ tale che $ f(A) \subseteq A $ e $ \mu(A) > 0 $ sia $ B \coloneqq X \setminus A $. Per la $ f $-invarianza di $ A $ si ha $ f^j(A) \subseteq A $ e quindi $ A \subseteq f^{-j}(f^j(A)) \subseteq f^{-j}(A) $; per la $ f $-invarianza della misura si ha invece
    \[
      \mu(A) = \mu(f^{-j}(A)) = \mu(f^{-j}(A) \cap A) + \mu(f^{-j}(A) \cap B) = \mu(A) + \mu(f^{-j}(A) \cap B)
    \]
    da cui $ \mu(f^{-j}(A) \cap B) = 0 $. Ma allora per ipotesi
    \[
      \mu(A)(1-\mu(A)) = \mu(A) \mu(B) = \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = 0
    \]
    e quindi $ \mu(A)=1 $. \qedhere
  \end{description}
\end{proof}

\begin{proposition}\label{prop:rotazioni_erg}
  La rotazione $ R_\alpha \colon \T^1 \to \T^1 $ è ergodica se e solo se $ \alpha \in \R \setminus \Q $.
\end{proposition}
\begin{proof}
  Usiamo la caratterizzazione con gli integrali primi con $ p=2 $. Prendiamo $ \varphi \colon \T^1 \to \R $ in $ L^2 $ e lo sviluppiamo in serie di Fourier:
  \[
    \varphi(x) = \sum_{n \in \Z} \hat{\varphi}(n) e^{2\pi i n x}.
  \]
  Componendola con la rotazione
  \[
    (\varphi \circ R_\alpha)(x) = \varphi(x + \alpha) = \sum_{n \in \Z} \hat{\varphi}(n) e^{2\pi i n \alpha} e^{2\pi i n x}.
  \]
  Affinché $ \varphi $ sia un integrale primo deve valere $ \hat{\varphi}(n) \left(e^{2\pi i n \alpha} - 1 \right) = 0 $ per ogni $ n \in \Z $.
  Ora se $ \alpha \in \R \setminus \Q, \ e^{2\pi i n \alpha} \neq 1 $ per ogni $ n \neq 0 $ così $ \varphi(x) = \hat{\varphi}(0) $, cioè $ \varphi $ è costante q.o. Per mostrare l'implicazione inversa supponiamo per assurdo che $ \alpha \in \Q $ e sia della forma $ p/q $; allora $ e^{2\pi i n \alpha} - 1 = 0 $ per ogni $ n $ della forma $ kq $ con $ k \in \Z $ da cui possiamo trovare un integrale primo non costante contro l'ipotesi che $ R_\alpha $ fosse ergodico.
\end{proof}

\begin{example}[successione di Kolakoski]
  \textcolor{red}{mancante}
\end{example}

\begin{exercise}
  Mostrare che le dilatazioni sul toro sono ergodiche.
\end{exercise}
\begin{solution}
  Sia $ E_m\colon\T^1\to\T^1 $, $ E_m(x) = mx\pmod{1} $ per $ m\in\Z,\ \abs{m} \geq 2 $. Prendiamo $ \varphi\colon\T^1\to\R $ tale che $ \varphi\circ f = \varphi $; espandiamo $ \varphi $ in serie di Fourier e imponiamo che sia un integrale del moto
  \[ \varphi(x) = \sum_{n\in\Z} \hat\varphi(n) e^{2\pi i n x} = \varphi(E_m(x)) = \sum_{n\in\Z} \hat\varphi(n) e^{2\pi i n m x} \quad \forall x\in\T^1. \]
  Per l'ortogonalità della base di Fourier l'uguaglianza deve valere termine a termine; dunque si ha che $ \hat{\varphi}(n) = 0 $ per gli $ n $ non multipli di $ m $. Per tutti gli $ n $ multipli di $ m $ deve invece valere
  \[ \hat\varphi(nm) = \hat{\varphi}(n). \]
  Ora se $ n \neq 0 $ si hanno infiniti coefficienti di Fourier uguali fra loro; questi non possono essere non nulli poiché, essendo $ \varphi \in L^2 $, devono tendere a zero per $ \abs{n} \to +\infty $. Quindi l'unico coefficiente che può essere non nullo è $ \hat\varphi(0) $, ossia $ \varphi $ è una costante.
\end{solution}

\begin{exercise}
  La trasformazione $ T_\alpha \colon \T^2 \to \T^2 $ data da $ T_\alpha(x, y) \coloneqq (x+\alpha, x+y) $ è ergodica se e solo se $ \alpha \in \R \setminus \Q $.
\end{exercise}

\begin{thm}[di ricorrenza di Poincaré]
  Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Allora $ \forall A \in \mathcal{A} $, per $ \mu $-q.o. $ x \in A $, $ x $ è ricorrente in $ A $.
\end{thm}
\begin{proof}
  Sia $ A_r \coloneqq \{x \in A : x \text{ è ricorrente in } A\} \subseteq A $. Riscriviamo $ A_r $ in un modo più comodo. Definiamo $ B_n \coloneqq \{x \in A : \forall m \geq n, \ f^{m}(x) \notin A\} $ come l'insieme degli $ x \in A $ che non visitano più $ A $ dopo $ n $ passi; possiamo scrivere tale insieme come
  \[
    B_n = A \setminus \bigcup_{j \geq n} f^{-j}(A).
  \]
  Così $ \bigcup_{n\geq1} B_n = \{x \in A : \exists n \geq 1 : \forall m \geq n, \ f^{m}(x) \notin A\} $ è l'insieme degli $ x \in A $ che non tornano mai in $ A $ ovvero degli $ x \in A $ che tornano in $ A $ entro un tempo finito $ n $, ma che dopo tale tempo non tornano più. Pertanto l'insieme dei punti ricorrenti è
  \[
    A_r = A \setminus \bigcup_{n \geq 1} B_n
  \]
  Essendo $ A_r $ differenza e unione numerabile di insiemi misurabili, anche $ A_r $ misurabile. Osservando che $ A \subseteq \bigcup_{j \geq 0} f^{-j}(A) $ essendo $ f^{0}(A) = A $, otteniamo
  \[
    B_n \subseteq \bigcup_{j \geq 0} f^{-j}(A) \setminus  \bigcup_{j \geq n} f^{-j}(A).
  \]
  Definendo allora $ \overline{A} \coloneqq \bigcup_{j \geq 0} f^{-j}(A) $ abbiamo che
  \[
    \mu(B_n) \leq \mu(\overline{A}) - \mu\left(\textstyle{\bigcup_{j \geq n}} f^{-j}(A)\right) = \mu(\overline{A}) - \mu\left(f^{-n}\left(\textstyle{\bigcup_{j \geq 0}} f^{-j}(A)\right)\right) = \mu(\overline{A}) - \mu(f^{-n}(\overline{A})).
  \]
  Ma $ \mu $ è $ f $-invariante quindi
  \[
    \mu(B_n) \leq \mu(\overline{A}) -\mu(f^{-n}(\overline{A})) = \mu(\overline{A}) - \mu(\overline{A}) = 0.
  \]
  da cui $ \mu\left(\bigcup_{n \geq 1} B_n\right) \leq \sum_{n \geq 1} \mu(B_n) = 0 $. Così concludiamo che
  \[
    \mu(A_r) = \mu(A) - \mu\left(\textstyle{\bigcup_{n \geq 1}} B_n\right) = \mu(A)
  \]
  cioè $ \mu $-q.o. $ x \in A $ è ricorrente in $ A $.
\end{proof}

\begin{proof}[Dimostrazione del Teorema di Birkhoff (\ref{thm:Birkhoff})]
  Trattiamo solo la parte con le funzioni caratteristiche. Sappiamo che esistono
  \[
    \underline{\nu}(x, A) \coloneqq \liminf_{n \to +\infty} \nu(x, A, n) \qquad \overline{\nu}(x, A) \coloneqq \limsup_{n \to +\infty} \nu(x, A, n)
  \]
  dove $ \nu(x, A, n) \coloneqq \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j}(x)) = \frac{1}{n} T(x, A, n) $. Sicuramente vale $ \underline{\nu}(x, A) \leq \overline{\nu}(x, A) $ quindi è sufficiente mostrare la disuguaglianza opposta. \\
  Fissato $ \epsilon > 0 $ sia
  \[
    \overline{\tau}(x, A, \epsilon) \coloneqq \min{\{n \in \N : \nu(x, A, n) \geq \overline{\nu}(x, A) - \epsilon\}}.
  \]
  Supponiamo per ora che tale quantità sia limitata uniformemente in $ x $, cioè $ \exists M_\epsilon > 0 : \forall x \in X, \ \overline{\tau}(x, A, \epsilon) \leq M_\epsilon $. L'idea è di dividere l'orbita da 0 a $ n-1 $ (abbiamo in mente $ n \gg M_\epsilon $ in quanto manderemo poi $ n \to +\infty $) in sotto-segmenti di lunghezza $ \overline{\tau} $ su cui abbiamo un controllo essendo $ \overline{\tau} $ limitato. Dato $ n > M_\epsilon $ consideriamo quindi il segmento di orbita $ (f^{j}(x))_{j = 0}^{n-1} $ di lunghezza $ n $. Definiamo la successione ricorsiva
  \[
    \begin{cases}
      x_0 = x & \tau_0 = \overline{\tau}(x_0, A, \epsilon) \\
      x_{k+1} = f^{\overline{\tau}(x_k, A, \epsilon)}(x_k) = f^{\tau_k}(x_0) & \tau_k = \sum_{j = 0}^{k} \overline{\tau}(x_j, A, \epsilon)
    \end{cases}
  \]
  definita da $ k = 0 $ fino a $ k = K $ tale che $ \tau_{K} < n - 1 $ ma $ \tau_{K+1} \geq n - 1 $. \\
  Ora per definizione $ \nu(x, A, \overline{\tau}(x, A , \epsilon)) \geq \overline{\nu}(x, A) - \epsilon $, mentre fissato $ N \in \N $ si ha
  \[
    \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j+N}(x)) = \frac{1}{n} \sum_{j = N}^{n+N-1} \chi_A(f^{j}(x)) = \frac{n+N}{n} \frac{1}{n+N} \sum_{j = 0}^{n+N-1} \chi_A(f^{j}(x)) - \frac{1}{n} \sum_{j = 0}^{N-1} \chi_A(f^{j}(x))
  \]
  da cui passando al $ \limsup $ abbiamo $ \overline{\nu}(f^N(x), A) = \overline{\nu}(x, A) $. Quindi per ogni $ k \in \{0, \ldots, K\} $ il numero di visite in ogni sotto-segmento si stima come
  \begin{align*}
    T(x_k, A, \overline{\tau}(x_k, A, \epsilon)) & = \overline{\tau}(x_k, A, \epsilon) \cdot \nu(x_k, A, \overline{\tau}(x, A , \epsilon)) \\
                                                 & \geq \overline{\tau}(x_k, A, \epsilon) \cdot (\overline{\nu}(x_k, A) - \epsilon) \\
                                                 & = \overline{\tau}(x_k, A, \epsilon) \cdot (\overline{\nu}(x, A) - \epsilon).
  \end{align*}
  Il numero totale di visite lungo il segmento di orbita di lunghezza $ n $ si stima considerando i sotto-segmenti su cui abbiamo un controllo come
  \begin{align*}
    T(x, A, n) & = \left[\sum_{k = 0}^{K} T(x_k, A, \overline{\tau}(x_k, A, \epsilon))\right] + T(x_{K}, A, n - \tau_{K}) \\
               & \geq \left[\sum_{k = 0}^{K}  \overline{\tau}(x_k, A, \epsilon)\right] (\overline{\nu}(x, A) - \epsilon) = \tau_{K} \cdot (\overline{\nu}(x, A) - \epsilon) \\
               & \geq (n - 1 - M_\epsilon)(\overline{\nu}(x, A) - \epsilon)
  \end{align*}
  dove abbiamo maggiorato il termine $ T(x_{K}, A, n - \tau_{K}) $ con $ 0 $ in quanto è il pezzo di orbita su cui non abbiamo controllo e abbiamo osservato che per costruzione la somma dei tempi $ \tau_K \geq n - M_{\epsilon} $. Ora per $ f $-invarianza della misura
  \[
    \int_{X} T(x, A, n) \dif{\mu}(x) = \sum_{j = 0}^{n-1} \int_{X} \chi_A(f^{j}(x)) \dif{\mu}(x) = \sum_{j = 0}^{n-1} \int_{X} \chi_A(x) \dif{\mu}(x) = n \cdot \mu(A)
  \]
  così per monotonia
  \begin{gather*}
    n \cdot \mu(A) = \int_{X} T(x, A, n) \dif\mu(x) \geq (n - 1 - M_\epsilon) \left(\int_{X}(\overline{\nu}(x, A) - \epsilon) \dif{\mu}(x)\right) \\
    \mu(A) \geq \frac{n - 1 - M_\epsilon}{n} \left(\int_{X}\overline{\nu}(x, A) \dif{\mu}(x) - \epsilon\right)
  \end{gather*}
  Passando prima al limite in $ n $ e poi all'estremo superiore su $ \epsilon $ otteniamo infine
  \[
    \mu(A) \geq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
  \]
  Ripetendo lo stesso ragionamento con $ \underline{\nu} $, cioè supponendo che la quantità
  \[
    \underline{\tau}(x, A, \epsilon) \coloneqq \min{\{n \in \N : \nu(x, A, n) \leq \overline{\nu}(x, A) + \epsilon\}}
  \]
  sia uniformemente limitata e dividendo il segmento di orbita da $ 0 $ a $ (n-1) $ in sotto-segmenti, giungiamo alla disuguaglianza opposta
  \[
    \mu(A) \leq \int_{X}\underline{\nu}(x, A) \dif{\mu}(x).
  \]
  Combinando le due disuguaglianze abbiamo quindi
  \[
    \int_{X}\underline{\nu}(x, A) \dif{\mu}(x) \geq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
  \]
  Ma d'altra parte essendo $ \underline{\nu}(x, A) \leq \overline{\nu}(x, A) $ per ogni $ x \in X $ anche
  \[
    \int_{X}\underline{\nu}(x, A) \dif{\mu}(x) \leq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
  \]
  Così
  \[
    \int_{X} \overline{\nu} \dif{\mu} = \int_{X} \underline{\nu} \dif{\mu} \quad \Rightarrow \quad \int_{X} \left(\overline{\nu} - \underline{\nu}\right) \dif{\mu} = 0.
  \]
  Ma dato che l'integrando è positivo concludiamo che $ \mu $-q.o. si ha $ \overline{\nu}(x, A) = \underline{\nu}(x, A) $. \\
  \textcolor{red}{Dobbiamo ora rimuovere l'ipotesi della limitatezza.}
\end{proof}

\section{Unica ergodicità}
\begin{proposition}
  Sia $ (X, \mathcal{A}, \mu_1, f) $ un sistema ergodico e $ \mu_2 $ una misura di probabilità su $ (X, \mathcal{A}) $ $ f $-invariante. Allora i seguenti fatti sono equivalenti:
  \begin{enumerate}[label=(\roman*)]
  \item $ \mu_1 \neq \mu_2 $;
  \item $ \mu_2 $ non è assolutamente continua rispetto a $ \mu_1 $, cioè $ \exists A \in \mathcal{A} $ tale che $ \mu_1(A) = 0 $ ma $ \mu_2(A) > 0 $;
  \item $ \exists A \in \mathcal{A} $ $ f $-invariante tale che $ \mu_1(A) = 0 $ e $ \mu_2(A) > 0 $.
  \end{enumerate}
\end{proposition}

Tale risultato stabilisce che un sistema dinamico misurabile può essere ergodico rispetto a due misure ``che non si parlano''. Ci sono tuttavia dei sistemi dinamici che ammettono una sola misura invariante. In tale caso si dà la seguente
\begin{definition}[sistema unicamente ergodico]
  $ (X, \mathcal{A}, \mu, f) $ si dice unicamente ergodico se esiste un'unica misura di probabilità su $ (X, \mathcal{A}) $ che sia $ f $-invariante.
\end{definition}

Tale definizione ha senso perché un sistema unicamente ergodico è anche ergodico. Se infatti per assurdo $ (X, \mathcal{A}, \mu, f) $ non fosse ergodico esisterebbe $ A \in \mathcal{A} $ $ f $-invariante tale che $ \mu(A) > 0 $ e $ \mu(X \setminus A) > 0 $. Possiamo allora definire le misure
\[
  \nu_1(E) \coloneqq \frac{\mu(A \cap E)}{\mu(A)}
  \qquad
  \nu_2(E) \coloneqq \frac{\mu((X \setminus A) \cap E)}{\mu(X \setminus A)}
\]
che sono misure di probabilità diverse (basta prendere $ E = A $) e $ f $-invarianti contro l'ipotesi. Per l'invarianza basta osservare che per l'invarianza di $ \mu $ si ha
\[
  \nu_1(f^{-1}(E)) = \frac{\mu(A \cap f^{-1}(E))}{\mu(A)} = \frac{\mu(f^{-1}(A) \cap f^{-1}(E))}{\mu(A)} = \frac{\mu(f^{-1}(A \cap E))}{\mu(A)} = \frac{\mu(A \cap E)}{\mu(A)} = \nu_1(E).
\]
La seconda uguaglianza è giustificata dal fatto che $ A \cap B $ e $ f^{-1}(A) \cap B $ sono uguali a meno di insiemi di misura nulla per ogni $ B \in \mathcal{A} $. Infatti per $ f $-invarianza di $ A $ si ha $ A \subseteq f^{-1}(A) $ e quindi $ A \Delta f^{-1}(A) = f^{-1}(A) \setminus A $, così per $ f $-invarianza della misura $ \mu(A \Delta f^{-1}(A)) = \mu(f^{-1}(A)) - \mu(A) = \mu(A) - \mu(A) = 0 $; ma allora $ \mu\left((A \cap B) \Delta (f^{-1}(A) \cap B)\right) = \mu\left((A \Delta f^{-1}(A)) \cap B\right) = 0 $. \\
Per la $ f $-invarianza di $ \nu_2 $ basta osservare che essa può essere riscritta in funzione delle sole $ \mu $ e $ \nu_1 $ che sono invarianti:
\[
  \nu_2(E) = \frac{\mu((X \setminus A) \cap E)}{\mu(X \setminus A)}
  = \frac{\mu(E \setminus (A \cap E))}{\mu(X\setminus A)}
  = \frac{\mu(E) - \mu(A \cap E)}{1 - \mu(A)}
  = \frac{\mu(E) - \nu_1(E) \mu(A)}{1 - \mu(A)}.
\]


Se oltre alla struttura misurabile, abbiamo anche una struttura topologica, allora l'unica ergodicità permette di ottenere una convergenza più forte delle medie temporali degli osservabili, rispetto a quella puntuale quasi ovunque garantita dall'ergodicità.

\begin{thm} \label{thm:unic-ergodico-convergenza-uniforme}
  Se $ (X, \mathcal{B}, \mu, f) $, con $ X $ spazio topologico e $ \mathcal{B} $ la $ \sigma $-algebra dei boreliani, è un sistema unicamente ergodico e $ \varphi \colon X \to \R $ è continua, allora si ha convergenza uniforme della media temporale dell'osservabile:
  \[
    \frac{1}{n} \sum_{j=0}^{n-1} (\varphi \circ f^j)(x) \, \touf \, \int_X \varphi \dif{\mu}.
  \]
\end{thm}

Osserviamo che tale enunciato richiede in particolare che le osservabili siano continue e non permette quindi di rimuovere il $ \mu $-q.o. nella definizione di sistema ergodico, essendo le funzioni caratteristiche non continue. Possiamo tuttavia cavarcela per approssimazione dall'alto e dal basso: dato $ A \in \mathcal{B} $, siano $ (\varphi_k)_{k\in\N} $ e $ (\psi_k)_{k\in \N} $ due successioni di funzioni continue tali che $ \varphi_k \uparrow \chi_A $ e $ \psi_k \downarrow \chi_A $ puntualmente. Grazie al Teorema \ref{thm:unic-ergodico-convergenza-uniforme} sappiamo che $ \forall x \in X $ (e uniformemente in $ x $) vale da un lato
\begin{align*}
  \int_X \varphi_k \dif{\mu} = \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi_k \circ f^j)(x) &= \liminf_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi_k \circ f^j)(x) \\ &\leq \liminf_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) = \underline{\nu}(x, A)
\end{align*}
e dall'altro
\begin{align*}
  \int_X \psi_k \dif{\mu} = \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\psi_k \circ f^j)(x) &= \limsup_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\psi_k \circ f^j)(x) \\ &\geq \limsup_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) = \overline{\nu}(x, A).
\end{align*}
Ora per convergenza monotona sulle $ (\varphi_k) $
\[
  \mu(A) = \int_X \chi_A \dif{\mu} = \lim_{k\to+\infty} \int_X \varphi_k \dif{\mu} \leq \underline{\nu}(x, A)
\]
e similmente sulle $ (\psi_k) $
\[
  \mu(A) = \int_X \chi_A \dif{\mu} = \lim_{k\to+\infty} \int_X \psi_k \dif{\mu} \geq \overline{\nu}(x, A).
\]
Così infine si ha che $ \forall x \in X $, $ \mu(A) \leq \underline{\nu}(x, A) \leq \overline{\nu}(x, A) \leq \mu(A) $ ovvero $ \nu(x, A) $ esiste ovunque e vale
\[
  \nu(x, A, n) = \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) \, \touf \, \mu(A).
\]

\begin{exercise}\label{ex:potenze_di_due_cancro}
  Sia $ x_j = \text{cifra più significativa di } 2^j $. Calcolare la frequenza di ciascuna cifra nella successione $ (x_j)_{j\in\N} $. \\
  \textcolor{red}{\emph{Hint}: le rotazioni irrazionali sul toro sono unicamente ergodiche e l'unica misura invariante è Lebesgue. Sarebbe bello mettere la dimostrazione di questa cosa: c'è sulle dispense di Grotto ma scompone la misura in Fourier.}
\end{exercise}
\begin{solution}
  Chiamiamo $ c_n $ la cifra più significativa di $ 2^n $, cioè il numero in $ \{1, \cdots, 9\} $ tale che
  \[ c_n \cdot 10^s \leq 2^n < (c_n+1) \cdot 10^s \]
  dove $ s = \lfloor \log_{10} 2^n \rfloor $. Prendendo ora il logaritmo si ha $ \log_{10}c_n + s \leq n\log_{10}2 < \log_{10}(c_n+1) + s $ e quindi
  \[ \log_{10}c_n \leq \{n\log_{10}2\} < \log_{10}(c_n+1) \]
  Considerando ora le rotazioni $ R_{\log_{10}(2)} \colon \T^1\to\T^1 $ possiamo riscrivere $ \{ n\log_{10}2 \} = R^n_{\log_{10}(2)}(0) $;
  se suddividiamo il toro come $ \T^1 = \bigsqcup_{k=1}^9 I_k $ con $ I_k = \left[\log_{10}k,\log_{10}(k+1)\right) $ la condizione che la cifra più significativa di $ 2^n $ sia $ c_n $ si traduce in
  \[ R^n_{\log_{10}(2)}(0) \in I_c \, . \]
  La sequenza delle potenze di 2 cercata è dunque la dinamica simbolica dell'orbita di 0 tramite la rotazione di $ \log_{10}2 $ secondo la suddetta partizione.

  Il sistema dinamico $ (\T^1, \mathcal{M}, \lambda, R_{\log_{10}(2)}) $ è unicamente ergodico in quanto $ \log_{10}2 $ è irrazionale; dunque le frequenze di visita convergono puntualmente e quindi la frequenza di visita dell'orbita di 0 agli intervalli $ I_k $ è uguale alla misura di Lebesgue degli intervalli stessi:
  \[ \nu(0,I_c) = \log_{10}\left(1+\frac{1}{c}\right) \, . \]
\end{solution}
