\section{Sistemi hamiltoniani}
\subsection{Introduzione}
Referenze e approfondimenti:
\begin{itemize}
\item A. Fasano, S. Marmi - \emph{``Analytical Mechanics: An Introduction''};
\item V. I. Arnol'd - \emph{``Mathematical Methods of Classical Mechanics''};
\item R. Abraham, J. E. Marsden - \emph{``Foundations of Mechanics''}.
\end{itemize}

Sia $ \gl(n, \R) $ il gruppo delle matrici $ n \times n $ a coefficienti in $ \R $ e $ {\GL(n, \R) \coloneqq \{A \in \gl(n, \R) : \det{A} \neq 0\}} $ il sottogruppo di $ \gl(n, \R) $ delle matrici invertibili. Di seguito indicheremo con $ \Gamma $ una particolare matrice antisimmetrica di $ \GL(2n, \R) $ a blocchi
\begin{equation}
  \Gamma \coloneqq
  \begin{pmatrix}
    0 & \Id_{n} \\
    -\Id_{n} & 0 \\
  \end{pmatrix}
\end{equation}
dove $ \Id_n $ è la matrice identità $ n \times n $. Tale matrice gode della proprietà $ \Gamma^t = -\Gamma = \Gamma^{-1} $ ed è facile verificare che $ \det{\Gamma} = 1 $. \\

\begin{exercise}
  Mostrare che ogni matrice antisimmetrica ha rango pari.
\end{exercise}

Sia $ \mathcal{O} \subseteq \R^{2n} \times \R $ un aperto semplicemente connesso detto \emph{spazio delle fasi generalizzato}. Di seguito un elemento di $ \mathcal{O} $ verrà indicato come una coppia $ (\vec{x}, t) $ dove le coordinate $ \vec{x} $ appartengono allo \emph{spazio delle fasi} $ \Pi_{\R^{2n}}(\mathcal{O}) $ e $ t \in \Pi_\R(\mathcal{O}) $ è detta variabile \emph{tempo} (con $ \Pi $ si intende la proiezione su $ \R^{2n} $ e $ \R $ rispettivamente). Spesso si scriverà $ \vec{x} = (\vec{q}, \vec{p}) $ dove $ \vec{q} $ e $ \vec{p} $ sono vettori di a $ n $ componenti detti \emph{posizione} e \emph{momento cinetico}. Le variabili $ \vec{q} $ e $ \vec{p} $ sono dette \emph{canonicamente coniugate}. Con lieve abuso di notazione un elemento di $ \mathcal{O} $ si scriverà spesso come una terna $ (\vec{q}, \vec{p}, t) $. \\

Sull'aperto $ \mathcal{O} $ si considera una funzione scalare $ \ham \colon \mathcal{O} \to \R $, detta \emph{funzione hamiltoniana}, di classe $ \mathcal{C}^{\infty} $. Tale ipotesi su $ \ham $ non sarà sempre necessaria, nel senso che i risultati che andremo ad enunciare valgono per anche per hamiltoniane meno regolari: questa è tuttavia una questione di cui non ci vogliamo occupare e pertanto supporremo che $ \ham $ sia di classe $ \mathcal{C}^\infty $.

Una funzione hamiltoniana genera un insieme di equazioni differenziali dette \emph{equazioni di Hamilton}
\begin{equation} \label{eqn:ham}
  \begin{cases}
    \dot{\vec{x}} = \Gamma \, \nabla_\vec{x} \ham(\vec{x}, t) \\
    \vec{x}(0) = \vec{x}_0
  \end{cases}
\end{equation}
dove con $ \nabla_\vec{x} \ham $ si intende il gradiente di $ \ham $ a tempo fissato, cioè vista come funzione della sola $ \vec{x} $, e $ \vec{x}_0 \in \Pi_{\R^{2n}}(\mathcal{O}) $. Il ``vettore'' $ \Gamma \, \nabla_\vec{x} \ham $ è detto \emph{gradiente simplettico} di $ \ham $.

Essendo $ \ham $ di classe $ \mathcal{C}^\infty $, il Teorema di Cauchy-Lipschitz-Picard-Lindelöf assicura esistenza e unicità locale della soluzione al problema di Cauchy \eqref{eqn:ham} e dipendenza continua dai valori iniziali. In termini delle variabili $ (\vec{q}, \vec{p}) $ il problema di Cauchy assume la forma più usuale
\begin{equation}
  \begin{dcases}
    \dot{q}_i = \dpd{\ham}{p_i} \\
    \dot{p}_i = -\dpd{\ham}{q_i} \\
    q_i(0) = q_{i}^0, \, p_i(0) = p_i^0 \\
  \end{dcases}
  \quad i = 1, \ldots, n
\end{equation}

Grazie al teorema di esistenza e unicità è ben definito il \emph{flusso hamiltoniano}
\begin{align}
  \Phi_\ham^t \colon \Pi_{\R^{2n}}(\mathcal{O}) & \to \R^{2n} \\
  \vec{x}_0 & \mapsto \vec{x}(t; \vec{x}_0) \nonumber
\end{align}
dove $ \vec{x}(t; \vec{x}_0) $ è la soluzione del problema \eqref{eqn:ham}, cioè una funzione da un intervallo che contiene $ t=0 $ a valori in $ \R^{2n} $ tale che $ \vec{x}(0; \vec{x}_0) = \vec{x}_0 $ e che per ogni $ t $ soddisfa $ \od{}{t}\vec{x}(t; \vec{x}_0) = \dot{\vec{x}}(t; \vec{x}_0) = \Gamma \, \nabla_\vec{x} \ham(\vec{x}(t; \vec{x}_0), t) $. Dalla definizione segue che
\[
  \od{}{t}\Phi^t_\ham(\vec{x}_0) = \dot{\vec{x}}(t; \vec{x}_0) = \Gamma \, \nabla_\vec{x} \ham(\Phi_\ham^t(\vec{x}_0), t).
\]

Nel caso \emph{autonomo}, cioè quando l'hamiltoniana non dipende dal tempo, sempre grazie al teorema di esistenza e unicità, si ha che il flusso hamiltoniano al tempo $ t+s $ partendo al punto $ \vec{x}_0 $ è la composizione del flusso al tempo $ s $ partendo da $ \vec{x}_0 $ e del flusso al tempo $ t $ partendo da $ \vec{x}_1 = \vec{x}(s; \vec{x}_0) $. Nel caso non autonomo questo non è più vero perché \textcolor{red}{ci sono problemi con l'origine dei tempi}.

\begin{example}[oscillatore armonico]
  In dimensione $ n = 1 $ si consideri l'hamiltoniana indipendente dal tempo
  \[
    \ham(q, p) = \frac{p^2}{2m} + \frac{1}{2} m \omega^2 q^2
  \]
  che è l'energia di un oscillatore armonico di massa $ m $ e frequenza $ \omega $, dove la posizione è indicata dalla $ q $. Le equazioni di Hamilton sono
  \[
    \dot{q} = \pd{\ham}{p} = \frac{p}{m} \qquad \dot{p} = -\pd{\ham}{q} = -m\omega^2 q
  \]
  da cui otteniamo l'equazione del moto dell'oscillatore armonico $ \ddot{q} + \omega^2 q = 0 $. Date le condizioni iniziali $ q(0) = q_0 $ e $ p(0) = p_0 $ si trova la soluzione $ q(t) = q_0 \cos{(\omega t)} + \frac{p_0}{m\omega} \sin{(\omega t)} $ definita per ogni tempo $ t \in \R $. Il flusso hamiltoniano è
  \[
    \Phi^t_\ham
    \begin{pmatrix}
      q_0 \\
      p_0
    \end{pmatrix}
    =
    \begin{pmatrix}
      q_0 \cos{(\omega t)} + \frac{p_0}{m\omega} \sin{(\omega t)} \\
      -m \omega q_0 \sin{(\omega t)} + p_0\cos{(\omega t)}
    \end{pmatrix}.
  \]
\end{example}

\begin{example}[oscillatore lineare forzato]
  Un oscillatore lineare forzato è un sistema unidimensionale che soddisfa l'equazione del moto
  \[
    m\ddot{q} + f(q) = g(t).
  \]
  Una possibile hamiltoniana da cui deriva tale equazione è
  \[
    \ham(q, p, t) = \frac{p^2}{2m} + F(q) - q g(t) \qquad \text{ con } F(q) = \int_0^q f(z) \dif{z}.
  \]
\end{example}

\begin{thm}
  Se l'hamiltoniana $ \ham $ è indipendente dal tempo allora per ogni $ t $ si ha $ \ham \circ \Phi_\ham^t = \ham $, cioè $ \ham $ è un integrale primo del moto.
\end{thm}
\begin{proof}
  Si ha per la \emph{chain rule}
  \begin{align*}
    \dod{}{t} \left(\ham(\Phi_\ham^t(\vec{x}_0), t)\right) & = (\nabla_\vec{x} \ham) (\Phi^t_\ham(\vec{x}_0), t) \cdot \left(\dod{}{t}\Phi^t_\ham(\vec{x}_0)\right) + \dpd{\ham}{t}(\Phi_\ham^t(\vec{x}_0), t) \\
                                                           & = \left(\nabla_\vec{x} \ham \cdot \Gamma \, \nabla_\vec{x} \ham\right)(\Phi^t_\ham(\vec{x}_0), t) + \dpd{\ham}{t}(\Phi_\ham^t(\vec{x}_0), t) = 0
  \end{align*}
  in quanto per ipotesi $ \pd{\ham}{t} = 0 $ e $ g(w, v) = w \cdot \Gamma v $ è una forma bilineare antisimmetrica e pertanto nulla se valutata sullo stesso vettore.
\end{proof}

\subsection{Parentesi di Poisson}
\begin{definition}[parentesi di Poisson]
  Siano $ F, G \colon \mathcal{O} \to \R $ due osservabili di classe $ \mathcal{C}^\infty $. Si definisce la parentesi di Poisson tra $ F $ e $ G $ come
  \begin{equation}
    \left\{F, G\right\} \coloneqq (\nabla_\vec{x} F)^T \,\Gamma\, \nabla_\vec{x} G = \sum_{i=1}^{n} \left(\dpd{F}{q_i} \dpd{G}{p_i} - \dpd{F}{p_i}\dpd{G}{q_i}\right).
  \end{equation}
\end{definition}

\begin{proposition}
  Le parentesi di Poisson godono delle seguenti proprietà:
  \begin{enumerate}[label=(\roman*)]
  \item \emph{antisimmetria}: $ \{F, G\} = - \{G, F\} $;
  \item \emph{linearità}: $ \{\alpha_1 F_1 + \alpha_2 F_2, G\} = \alpha_1 \{F_1, G\} + \alpha_2\{F_2, G\} $;
  \item \emph{regola di Leibniz}: $ \{F_1 \, F_2, G\} = F_2 \, \{F_1, G\} + F_1 \, \{F_2, G\} $;
  \item \emph{identità di Jacobi}: $ \{F, \{G, H\}\} + \{G, \{H, F\}\} + \{H, \{F, G\}\} = 0 $.
  \end{enumerate}
\end{proposition}

\begin{definition}[algebra di Lie]
  Un'algebra di Lie è una coppia $ (A, \star) $ dove $ A $ è uno spazio vettoriale su $ \K = \R \text{ o } \C $ e $ \star \colon A \times A \to A $ è un prodotto che soddisfa le seguenti proprietà:
  \begin{enumerate}[label=(\roman*)]
  \item \emph{anticommutativo}: $ \forall a, b \in A : a \star b = - (b \star a) $;
  \item \emph{distributivo}: $ \forall a, b, c \in A : a \star (b + c) = a \star b + a \star c $;
  \item \emph{associativo con gli scalari}: $ \forall a, b \in A, \ \forall \lambda \in \K : \lambda (a \star b) = (\lambda a) \star b $;
  \item \emph{identità di Jacobi}: $ \forall a, b, c \in A : a \star (b \star c) + b \star (c \star a) + c \star (a \star b) = 0 $.
  \end{enumerate}
\end{definition}

\begin{exercise}
  Mostrare che le seguenti strutture sono un'algebra di Lie.
  \begin{enumerate}
  \item $ \R^3 $ con il prodotto vettoriale $ (\vec{x} \times \vec{y})_i = \epsilon_{ijk} x_j y_k $ dove $ \epsilon_{ijk} $ è il tensore di Levi-Civita.
  \item Le osservabili $ \mathcal{C}^\infty(\mathcal{O}, \R) $ con le parentesi di Poisson.
  \item $ \gl(n, \R) $ con il commutatore $ [A, B] = AB - BA $.
  \end{enumerate}
\end{exercise}

In termini delle parentesi di Poisson, le equazioni di Hamilton assumono una forma più concisa
\begin{equation}
  \begin{cases}
    \dot{\vec{x}} = \{\vec{x}, \ham\} \\
    \vec{x}(0) = \vec{x}_0
  \end{cases}
\end{equation}
o in funzione delle $ (\vec{q}, \vec{p}) $
\begin{equation}
  \begin{cases}
    \dot{q}_i = \{q_i, \ham\} \\
    \dot{p}_i = \{p_i, \ham\} \\
    q_i(0) = q_i^0, \, p_i(0) = p_i^0
  \end{cases}
\end{equation}
È immediato verificare che valgono le così dette \emph{parentesi di Poisson fondamentali}
\begin{equation}
  \{q_i, q_j\} = \{p_i, p_j\} = 0 \qquad \{q_i, p_j\} = \delta_{ij}
\end{equation}

Le parentesi di Poisson permettono anche di scrivere in modo sintetico la derivata di un'osservabile lungo il flusso hamiltoniano. È infatti facile verificare che
\begin{equation}
  \od{}{t} \left(F(\Phi_\ham^t, t)\right) = \{F, \ham\}(\Phi_\ham^t, t) + \pd{F}{t}(\Phi_\ham^t, t).
\end{equation}
Tale relazione permette di dedurre i seguenti fatti:
\begin{itemize}
\item se un'osservabile non dipende in modo esplicito dal tempo e ha parentesi di Poisson nulla con l'hamiltoniana, allora è un integrale primo del moto;

\item \emph{teorema di Poisson}: usando l'identità di Jacobi, se $ F $ e $ G $ sono due osservabili che hanno parentesi di Poisson nulla con l'hamiltoniana anche la loro parentesi di Poisson $ \{F, G\} $ ha parentesi nulla con l'hamiltoniana. In particolare, se $ F $ e $ G $ non dipendono in modo esplicito dal tempo, anche $ \{F, G\} $ non dipende dal tempo ed è quindi un integrale del moto.
\end{itemize}

\subsection{Sistemi hamiltoniani lineari}

\begin{definition}[sistema hamiltoniano lineare]
  Un sistema hamiltoniano si dice lineare se l'hamiltoniana è una forma quadratica
  \begin{equation}
    \ham(\vec{x}, t) = \frac{1}{2} \vec{x}^T S(t) \vec{x}
  \end{equation}
  dove $ S \colon [a, b] \to \gl(2n, \R) $ è una matrice dipendente dal tempo di classe $ \mathcal{C}^\infty $, simmetrica (cioè $ \forall t \in [a, b], \ S^T(t) = S(t) $) e $ 0 \in [a, b] \subseteq \R $. Un'hamiltoniana di questo tipo definisce delle equazioni di Hamilton lineari
  \begin{equation} \label{eqn:ham-lineare}
    \begin{cases}
      \dot{\vec{x}} = \Gamma S(t) \vec{x} \\
      \vec{x}(0) = \vec{x}_0 \\
    \end{cases}
  \end{equation}
\end{definition}

Nel caso di $ S $ costante, cioè $ S(t) = S $, il sistema \eqref{eqn:ham-lineare} diventa autonomo $ \dot{\vec{x}} = \Gamma S \vec{x} $ e la soluzione si può scrivere in modo esplicito usando l'esponenziale di una matrice
\begin{equation}
  \vec{x}(t; \vec{x}_0) = \Phi^t_\ham(\vec{x}_0) = e^{t \Gamma S} \vec{x}_0 = e^{tA} \vec{x}_0
\end{equation}
dove abbiamo definito $ A \coloneqq \Gamma S $ e ricordiamo che $ e^{tA} \coloneqq \sum_{n=0}^{+\infty} \frac{t^n}{n!}A^n $.

\begin{definition}[matrice hamiltoniana]
  $ A \in \mathrm{gl}(2n, \R) $ è detta matrice hamiltoniana o simplettica infinitesima se
  \begin{equation}
    A^T \Gamma + \Gamma A = 0.
  \end{equation}
  L'insieme $ \sp(n, \R) \coloneqq \{A \in \gl(2n, \R) : A \text{ è hamiltoniana}\} $ è il sottospazio vettoriale di $ \gl(2n, \R) $ delle matrici hamiltoniane\footnote{Le matrici hamiltoniane non formano invece un sottogruppo di $ \gl(2n, \R) $ con la moltiplicazione.}.
\end{definition}

\begin{thm} \label{thm:mat-sp}
  Sia $ A \in \gl(2n, \R) $. Le seguenti proprietà sono equivalenti:
  \begin{enumerate}[label=(\roman*)]
  \item $ A \in \sp(n, \R) $;
  \item $ \Gamma A \Gamma = A^T $;
  \item $ \exists R \in \gl(2n, \R) : R = R^T $ tale che $ A = \Gamma R $;
  \item\label{pt:GAsimm} $ (\Gamma A)^T = \Gamma A $.
  \end{enumerate}
  Inoltre $ \forall A, B \in \sp(n, \R) $ e $ \forall \lambda \in \R $ si ha $ A^T, \lambda A, A \pm B, [A, B] \in \sp(n, \R) $ e pertanto $ \sp(n, \R) $ è una sottoalgebra di Lie di $ \gl(2n, \R) $ con il commutatore.
\end{thm}
\begin{proof}
  \begin{description}
  \item[$ (i) \iff (ii) $] Ovvio.
  \item[$ (i) \iff (iv) $] Sia $ A \in \gl(2n, \R) $ tale che $ A^T \Gamma + \Gamma A = 0 $. Allora $ \Gamma A = - A^T \Gamma = - (\Gamma^T A)^T = - ((-\Gamma) A)^T = (\Gamma A)^T $.
  \item[$ (iii) \iff (iv) $] Sia $ R $ simmetrica e $ A = \Gamma R $. Allora $ (\Gamma A)^T = (\Gamma^2 R)^T = (-\Id R)^T = -R^T = -R = (\Gamma)^2 R = \Gamma (\Gamma R) = (\Gamma A) $. Viceversa per ipotesi $ R \coloneqq -\Gamma A $ è matrice simmetrica quindi $ A = - \Gamma^{-1} R^T = \Gamma R $.
  \end{description}
  \noindent Siano ora $ A = \Gamma R $ e $ B = \Gamma S $, con $ R $ e $ S $ matrici simmetriche, due matrici in $ \sp(n, \R) $. Allora
  \begin{itemize}
  \item $ A^T = R^T \Gamma^T = - R \Gamma = \Gamma (\Gamma R \Gamma) $ e $ (\Gamma R \Gamma)^T = \Gamma^T R^T \Gamma^T = (-\Gamma) R (-\Gamma) = \Gamma R \Gamma $;
  \item $ \lambda A = \lambda \Gamma R = \Gamma (\lambda R) $ e $ \lambda R $ è matrice simmetrica;
  \item $ A \pm B = \Gamma R \pm \Gamma S = \Gamma (R \pm S) $ e $ R \pm S $ è matrice simmetrica;
  \item $ [A, B] = \Gamma R \Gamma S - \Gamma S \Gamma R = \Gamma (R \Gamma S - S \Gamma R) $ e $ (R \Gamma S - S \Gamma R)^T = S^T \Gamma^T R^T - R^T \Gamma^T S^T = - S \Gamma R + R \Gamma S $.
  \end{itemize}
  Quindi $ \sp(n, \R) $ è effettivamente una sottoalgebra di Lie.
\end{proof}

\begin{exercise}
  Mostrare che se $ n = 1 $ allora $ A \in \sp(1, \R) \iff \tr{A} = 0 $. In generale mostrare che scrivendo $ A \in \gl(2n, \R) $ come matrice a blocchi $ A = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix} $ con $ a, b, c, d \in \gl(n, \R) $ allora
  \[
    A \in \sp(n, \R) \iff
    \begin{cases}
      a^T + d = 0 \\
      b^T = b, \ c^T = c
    \end{cases}
  \]
  Dedurre da questo $ \dim_\R {\sp(n, \R)} $.
\end{exercise}
\begin{solution}
  Se $ n = 1 $ allora $ A $ è una matrice quadrata $ 2 \times 2 $ a coefficienti $ a, b, c, d \in \R $. Si ha
  \[
    \Gamma A =
    \begin{pmatrix}
      0 & 1 \\
      -1 & 0
    \end{pmatrix}
    \begin{pmatrix}
      a & b \\
      c & d
    \end{pmatrix}
    =
    \begin{pmatrix}
      c & d \\
      -a & -b
    \end{pmatrix}
  \]
  Imponendo che $ \Gamma A $ sia simmetrica si ottiene $ d = -a \Rightarrow \tr{A} = a + d = 0 $. Nel caso generale $ a, b, c, d $ sono matrici $ n \times n $ pertanto
  \[
    (\Gamma A)^T =
    \begin{pmatrix}
      c^T & -a^T \\
      d^T & -b^T
    \end{pmatrix}
  \]
  da cui affinché $ \Gamma A $ sia simmetrica deve essere $ c^T = c $, $ b^T = b $ e $ -a^T = d \Rightarrow a^T + d = 0 $. \\
  Lo spazio $ \gl(n, \R) $ ha dimensione $ n^2 $ mentre il sottospazio di $ \gl(n, \R) $ delle matrici simmetriche ha dimensione $ n(n+1)/2 $. Pertanto $ \dim_\R \sp(n, \R) = n^2 + n(n+1)/2 + n(n+1)/2 = 2n^2 + n $.
\end{solution}

\subsection{Matrici simplettiche}
\begin{definition}[matrice simplettica]
  Sia $ \Omega \in \gl(2n, \R) $ e $ \mu \in \R $ con $ \mu \neq 0 $. Si dice che $ \Omega $ è $ \mu $-simplettica se soddisfa
  \begin{equation} \label{eqn:mat-Sp}
    \Omega^T \Gamma \Omega = \mu \Gamma.
  \end{equation}
  Per $ \mu = 1 $ si parla semplicemente di matrice simplettica e $ \Sp(n, \R) \coloneqq \{\Omega \in \gl(2n, \R) : \Omega \text{ è simplettica}\} $ è il sottospazio vettoriale di $ \gl(2n, \R) $ delle matrici simplettiche.
\end{definition}

\begin{thm}
  Sia $ \Omega \in \gl(2n, \R) $ e $ \mu \in \R $ con $ \mu \neq 0 $. Se $ \Omega $ è $ \mu $-simplettica allora $ \det{\Omega} \neq 0 $ e $ \Omega^{-1} = - \frac{1}{\mu} \Gamma \Omega^T \Gamma $. Inoltre se $ \Omega_1, \Omega_2 $ sono $ \mu_1 $ e $ \mu_2 $-simplettiche allora $ \Omega_1^T $, $ \Omega_1^{-1} $ e $ \Omega_1 \Omega_2 $ sono rispettivamente $ \mu_1 $, $ \frac{1}{\mu_1} $ e $ \mu_1 \mu_2 $-simplettiche. Pertanto $ \Sp(n, \R) $ è un gruppo.
\end{thm}
\begin{proof}
  Se $ \Omega $ è $ \mu $-simplettica, prendendo il determinante dell'equazione \eqref{eqn:mat-Sp} si ottiene $ (\det{\Omega})^2 = \mu \neq 0 $ da cui $ \Omega $ è invertibile. Ci basta allora verificare che $ \Omega^{-1} \Omega = -\frac{1}{\mu} \Gamma \Omega^T \Gamma \Omega = - \frac{1}{\mu} \mu \Gamma^2 = \Id $. Per quanto riguarda le proprietà di gruppo si ha:
  \begin{itemize}
  \item $ (\Omega_1 \Omega_2)^T \Gamma (\Omega_1 \Omega_2) = \Omega_2^T (\Omega_1 \Gamma \Omega_1) \Omega_2 = \mu_1 \Omega_2^T \Gamma \Omega_2 = \mu_1 \mu_2 \Gamma $;
  \item dalla relazione tra $ \Omega $ e la sua inversa abbiamo $ \Omega \Omega^{-1} = - \frac{1}{\mu} \Omega \Gamma \Omega^T \Gamma = \Id $ da cui $ \Omega \Gamma \Omega^T = \mu \Gamma $ da cui $ \Omega^T $ è $ \mu $-simplettica;
  \item $ (\Omega^{-1})^T \Gamma \Omega^{-1} = (\Omega^T)^{-1} \Gamma \Omega^{-1} = - (\Omega \Gamma \Omega^T)^{-1} = - (\mu \Gamma)^{-1} = \frac{1}{\mu} \Gamma $. \qedhere
  \end{itemize}
\end{proof}

\begin{exercise}
  Mostrare che se $ n = 1 $ allora $ \Omega \in \Sp(1, \R) \iff \det{\Omega} = 1 $. In generale mostrare che scrivendo $ \Omega \in \gl(2n, \R) $ come matrice a blocchi $ \Omega = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix} $ con $ a, b, c, d \in \gl(n, \R) $ allora
  \[
    \Omega \text{ è $ \mu $-simplettica } \iff
    \begin{cases}
      a^T d - c^T b = \mu \Id \\
      (a^T c)^T = a^T c \\
      (b^T d)^T = b^T d
    \end{cases}
  \]
  e per l'inversa è data da $ \Omega^{-1} = \dfrac{1}{\mu} \begin{pmatrix} d^T & -b^T \\ -c^T & a^T \end{pmatrix} $.
\end{exercise}
\begin{solution}
  Se $ n = 1 $ allora $ \Omega $ è una matrice quadrata $ 2 \times 2 $ a coefficienti $ a, b, c, d \in \R $. Si ha
  \[
    \Omega^T \Gamma \Omega =
    \begin{pmatrix}
      a & c \\
      b & d
    \end{pmatrix}
    \begin{pmatrix}
      0 & 1 \\
      -1 & 0
    \end{pmatrix}
    \begin{pmatrix}
      a & b \\
      c & d
    \end{pmatrix}
    =
    \begin{pmatrix}
      0 & ad - bc \\
      bc - ad & 0
    \end{pmatrix}
    =
    \Gamma =
    \begin{pmatrix}
      0 & 1 \\
      -1 & 0
    \end{pmatrix}
  \]
  da cui ricaviamo che $ \det{\Omega} = ad - bc = 1 $. Nel caso generale $ a, b, c, d $ sono matrici $ n \times n $ pertanto
  \[
    \Omega^T \Gamma \Omega =
    \begin{pmatrix}
      a^T c - c^T a & a^T d - c^T b \\
      b^T c - d^T a & b^T d - d^T b
    \end{pmatrix}
    =
    \mu \Gamma
    =
    \begin{pmatrix}
      0        & \mu \Id \\
      -\mu \Id & 0
    \end{pmatrix}
  \]
  per cui dobbiamo imporre $ a^T c = c^T a = (a^T c)^T $, $ b^T d = d^T b = (b^T d)^T $ e infine $ a^T d - c^T b = \mu \Id $. Per quanto riguarda l'inversa
  \[
    \Omega^{-1} = - \frac{1}{\mu} \Gamma \Omega^T \Gamma =
    - \frac{1}{\mu}
    \begin{pmatrix}
      0    & \Id \\
      -\Id & 0
    \end{pmatrix}
    \begin{pmatrix}
      a^T & b^T \\
      c^T & d^T
    \end{pmatrix}
    \begin{pmatrix}
      0    & \Id \\
      -\Id & 0
    \end{pmatrix}
    =
    \dfrac{1}{\mu}
    \begin{pmatrix}
      d^T  & -b^T \\
      -c^T & a^T
    \end{pmatrix}
  \]
\end{solution}
Il seguente risultato stabilisce il legame tra matrici hamiltoniane e matrici simplettiche.

\begin{thm}
  Mostrare che $ A \in \sp(n, \R) \Rightarrow \forall t \in \R, \ e^{tA} \in \Sp(n, \R) $.
\end{thm}
\begin{proof}
  Dalla definizione di esponenziale di una matrice si può mostrare che $ (e^{tA})^T = e^{t A^T} $ e che $ e^{tA} $ è invertibile con inversa $ (e^{tA})^{-1} = e^{-tA} $. Pertanto la condizione di simpletticità è equivalente a mostrare che
  \[
    e^{tA^T} \Gamma = \Gamma e^{-tA}.
  \]
  cioè, usando la definizione
  \[ \sum_{n=0}^{+\infty} \frac{t^n (A^T)^n \Gamma}{n!} = \sum_{n=0}^{+\infty} \frac{(-1)^n t^n \Gamma A^n}{n!}. \]
  Tale uguaglianza è verificata termine a termine in quanto
  \[ (A^T)^n = (\Gamma A \Gamma)^n = (-1)^{n-1} \Gamma A^n \Gamma \]
  da cui
  \[ (A^T)^n \Gamma = (-1)^n \Gamma A^n. \qedhere \]
\end{proof}

\subsection{Spazi vettoriali simplettici}

\begin{definition}[prodotto simplettico]
  Sia $ V $ uno spazio vettoriale su $ \R $ con $ \dim_\R V = 2n $. Una funzione $ \omega \colon V \times V \to \R $ si dice prodotto simplettico se è
  \begin{enumerate}[label=(\roman*)]
  \item \emph{antisimmetrico}: $ \forall \vec{x}, \vec{y} \in V, \ \omega(\vec{x}, \vec{y}) = - \omega(\vec{y}, \vec{x}) $;
  \item \emph{bilineare}: $ \forall \vec{x}_1, \vec{x}_2, \vec{y} \in V, \forall \alpha_1, \alpha_2 \in \R, \ \omega(\alpha_1 \vec{x}_1 + \alpha_2 \vec{x}_2, \vec{y}) = \alpha_1 \omega(\vec{x}_1, \vec{y}) + \alpha_2 \omega(\vec{x}_2, \vec{y}) $;
  \item \emph{non degenere}: $ \forall \vec{y} \in V, \ \omega(\vec{x}, \vec{y}) = 0 \Rightarrow \vec{x} = 0 $.
  \end{enumerate}
  Uno spazio vettoriale dotato di prodotto simplettico $ (V, \omega) $ è detto spazio vettoriale simplettico.
\end{definition}

Sia $ V $ uno spazio vettoriale simplettico e $ \{\vec{e}_1, \ldots, \vec{e}_{2n}\} $ una base. Possiamo definire la matrice $ W \in \gl(2n, \R) $ data da $ W_{ij} = \omega(\vec{e}_i, \vec{e}_j) $. Tale matrici risulta antisimmetrica $ W^{T} = -W $ e invertibile $ \det{W} \neq 0 $. Dati $ \vec{x} = \sum_{i=1}^{2n} x_i \vec{e}_i $ e $ \vec{y} = \sum_{j=1}^{2n} y_j \vec{e}_j $ per bilinearità il prodotto simplettico si scrive come
\begin{equation} \label{eqn:prod-simplettoc-W}
  \omega(\vec{x}, \vec{y}) = \sum_{i, j=1}^{2n} x_i y_j \omega(\vec{e}_i, \vec{e}_j) = \sum_{i, j=1}^{2n} x_i y_j W_{ij} = \vec{x}^T W \vec{y}
\end{equation}
Viceversa data una matrice $ W \in \gl(2n, \R) $ invertibile e antisimmetrica, questa induce su $ V $ un prodotto simplettico $ \omega_W $ dato dalla formula \eqref{eqn:prod-simplettoc-W}. \\

Se $ W = \Gamma $ allora si parla di \emph{prodotto simplettico standard}. Se $ V = \R^{2n} $, scrivendo $ \vec{x} = (\vec{q}, \vec{p}) $ e $ \vec{y} = (\vec{q}', \vec{p}') $ e la base canonica come $ \{\vec{e}_{q_1}, \ldots, \vec{e}_{q_n}, \vec{e}_{p_1}, \ldots, \vec{e}_{p_n}\} $ si ha
\begin{equation}
  \omega_\Gamma(\vec{x}, \vec{y}) = \vec{x}^T \Gamma \vec{y} = \sum_{i=1}^{n} \left(q_i p'_i - p_i q'_i\right) =
  \sum_{i=1}^{n}
  \det{
    \begin{pmatrix}
      q_i & p_i \\
      q'_i & p'_i
    \end{pmatrix}
  }
\end{equation}
Tale relazione ha la seguente interpretazione geometrica: il prodotto scalare simplettico standard su $ \R^{2n} $ è la somma delle aree orientate delle proiezioni del parallelogramma di lati $ \vec{x} $ e $ \vec{y} $ sugli $ n $ piani $ \pi_i = \operatorname{span}\left(\{\vec{e}_{q_i}, \vec{e}_{p_i}\}\right) $.

Osserviamo infine che per il prodotto simplettico standard valgono delle relazioni simili a quelle delle parentesi di Poisson fondamentali
\begin{equation}
  \omega_\Gamma(\vec{e}_{q_i}, \vec{e}_{q_j}) = \omega_\Gamma(\vec{e}_{p_i}, \vec{e}_{p_j}) = 0 \qquad \omega_\Gamma(\vec{e}_{q_i}, \vec{e}_{p_j}) = \delta_{ij}.
\end{equation}

\begin{definition}[base simplettica]
  Sia $ (V, \omega) $ uno spazio vettoriale simplettico. Una base $ \{\vec{e}_1, \ldots, \vec{e}_{2n}\} $ di $ V $ si dice simplettica se $ W = \Gamma $.
\end{definition}

\begin{thm}
  Ogni spazio vettoriale simplettico $ (V, \omega) $ ammette una base simplettica.
\end{thm}
\begin{proof}
  \textcolor{red}{Esercizio, 2 modi.}
\end{proof}

\begin{definition}[funzione simplettica]
  Siano $ (V_1, \omega_1) $ e $ (V_2, \omega_2) $ due spazi vettoriali simplettici. Una funzione lineare $ S \colon V_1 \to V_2 $ si dice simplettica se conserva il prodotto simplettico
  \[
    \omega_2(S(\vec{v}), S(\vec{u})) = \omega_1(\vec{v}, \vec{u}) \quad \forall \vec{v}, \vec{u} \in V_1.
  \]
\end{definition}

\begin{proposition}
  Su $ (\R^{2n}, \omega_\Gamma) $, un endomorfismo lineare $ S \colon \R^{2n} \to \R^{2n} $ è simplettico se e solo se la sua matrice rappresentativa $ S $ è una matrice simplettica $ S^T \Gamma S = \Gamma $.
\end{proposition}

\subsection{Campi vettoriali hamiltoniani}

\begin{definition}[campo vettoriale hamiltoniano]
  Un campo vettoriale $ \vec{X} \colon \mathcal{O} \to \R^{2n} $ di classe $ \mathcal{C}^\infty $ si dice hamiltoniano se esiste una funzione hamiltoniana $ \ham \colon \mathcal{O} \to \R $ tale che $ \forall (\vec{x}, t) \in \mathcal{O}, \ \vec{X}(\vec{x}, t) = \Gamma \nabla_\vec{x} \ham(\vec{x}, t) $.
\end{definition}

Chiaramente l'hamiltoniana associata a $ \vec{X} $ non è unica in quanto ad essa può essere aggiunta una funzione $ \chi $ con $ \nabla_\vec{x} \chi = 0 $. Essendo $ \mathcal{O} $ connesso, tale relazione equivale a chiedere che $ \chi $ sia indipendente da $ \vec{x} $: $ \chi(\vec{x}, t) = \chi(t) $. L'hamiltoniana diventa unica se chiediamo che al campo vettoriale nullo sia associata l'hamiltoniana nulla. \\

In seguito sarà utile il seguente risultato generale dell'analisi vettoriale.

\begin{lemma}[Poincaré] \label{lem:poincare}
  Sia $ A \subseteq \R^m $ un aperto semplicemente connesso e $ \vec{F} \colon A \to \R^m $ una funzione di classe $ \mathcal{C}^1 $ tale che $ \forall i, j = 1, \ldots, m $
  \[
    \pd{F_i}{x_j} = \pd{F_j}{x_i}.
  \]
  Allora esiste una funzione $ f \colon A \to \R $ di classe $ \mathcal{C}^2 $ tale che $ \vec{F} = - \grad{f} $.
\end{lemma}

\begin{thm} \label{thm:campo-hamiltoniano}
  $ X \colon \mathcal{O} \to \R^{2n} $ è un campo vettoriale hamiltoniano se e solo se
  \begin{equation}
    J_\vec{x} \vec{X} (\vec{x}, t) \coloneqq \left(\dpd{X_i}{x_j}(\vec{x}, t)\right)_{ij}
  \end{equation}
  è una matrice hamiltoniana $ \forall (\vec{x}, t) \in \mathcal{O} $.
\end{thm}
\begin{proof}
  Supponiamo che esista una hamiltoniana $ \ham $ tale che $ \vec{X} = \Gamma \nabla_\vec{x} \ham $. Allora
  \[
    \pd{X_i}{x_j} = \sum_{k=1}^{2n} \Gamma_{ik} \md{\ham}{2}{x_k}{}{x_j}{} = \left( \Gamma \, \mathrm{Hess}(\ham) \right)_{ij}.
  \]
  Essendo $ \ham $ regolare, la sua matrice hessiana è simmetrica e pertanto $ J_\vec{x} \vec{X} $ è hamiltoniana. \\
  Viceversa, detto $ \vec{Y}(\vec{x}, t) \coloneqq \Gamma \vec{X}(\vec{x}, t) $ ed essendo $ J_\vec{x} \vec{X} $ hamiltoniana si ha, grazie al punto \ref{pt:GAsimm} del Teorema \ref{thm:mat-sp},
  \[
    \pd{Y_i}{x_j} = (\Gamma J_\vec{x} \vec{X})_{ij} = (\Gamma J_\vec{x} \vec{X})_{ji} = \pd{Y_j}{x_i}.
  \]
  Essendo $ \mathcal{O} $ semplicemente connesso, per il Lemma \ref{lem:poincare}, esiste $ \ham \colon \mathcal{O} \to \R $ tale che $ \vec{Y}(\vec{x}, t) = - \nabla_\vec{x} \ham(\vec{x}, t) $ da cui $ \vec{X}(\vec{x}, t) = -\Gamma \vec{Y}(\vec{x}, t) = \Gamma \nabla_\vec{x} \ham(\vec{x}, t) $.
\end{proof}

\begin{exercise}
  Si consideri il campo vettoriale definito dal sistema di equazioni differenziali
  \[
    \begin{cases}
      \dot{q} = q^{\beta} p^\alpha \\
      \dot{p} = - q^{\gamma} p^{\alpha+1}
    \end{cases}
  \]
  Dire per quali valori di $ \alpha, \beta, \gamma \in \R $ il campo è hamiltoniano e trovare una hamiltoniana.
\end{exercise}
\begin{solution}
  Sia $ \vec{X}(q, p, t) = (p^\alpha q^\beta, -p^{\alpha+1}q^{\beta}) $ il campo in questione. Si ha
  \[
    J_\vec{x} \vec{X} (q, p, t) =
    \begin{pmatrix}
      \beta q^{\beta-1} p^{\alpha} &  - \gamma q^{\gamma-1} p^{\alpha+1} \\
      \alpha q^{\beta} p^{\alpha-1} & - (\alpha+1) q^{\gamma} p^{\alpha}
    \end{pmatrix}
  \]
  Ora $ J_\vec{x} \vec{X} \in \sp(1, \R) \iff \tr{J_\vec{x} \vec{X}} = 0 $ pertanto deve essere
  \[
    \beta q^{\beta-1} p^{\alpha} - (\alpha+1) q^{\gamma} p^{\alpha} = 0 \quad \Rightarrow \quad \left(\beta q^{\beta-1} - (\alpha+1) q^{\gamma}\right) p^{\alpha} = 0
  \]
  da cui ricaviamo se $ \alpha = -1 $ allora $ \beta = 0 $ e $ \gamma $ è qualsiasi; se invece $ \alpha \neq -1 $ allora $ \beta = \alpha+1 $ e $ \gamma = \beta - 1 = \alpha $. Nel primo caso le equazioni di Hamilton diventano
  \[
    \begin{cases}
      \dot{q} = 1/p \\
      \dot{p} = -q^{\gamma}
    \end{cases}
  \]
  e una possibile hamiltoniana è $ \ham (q, p, t) = q^{\gamma+1}/(\gamma+1) + \log{p} $. Nel secondo caso si ha
  \[
    \begin{cases}
      \dot{q} = q^{\alpha+1} p^\alpha  \\
      \dot{p} = - q^{\alpha} p^{\alpha+1}
    \end{cases}
  \]
  e una possibile hamiltoniana è $ \ham(q, p, t) = \dfrac{q^{\alpha+1}p^{\alpha+1}}{\alpha+1} $.
\end{solution}
